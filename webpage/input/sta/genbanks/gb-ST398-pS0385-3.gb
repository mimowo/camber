LOCUS       AM990995                3158 bp    DNA     circular BCT 21-JUN-2010
DEFINITION  Staphylococcus aureus subsp. aureus ST398 pS0385-3 plasmid, isolate
            SO385.
ACCESSION   AM990995
VERSION     AM990995.1  GI:283471940
DBLINK      Project: 29427
KEYWORDS    .
SOURCE      Staphylococcus aureus subsp. aureus ST398
  ORGANISM  Staphylococcus aureus subsp. aureus ST398
            Bacteria; Firmicutes; Bacillales; Staphylococcus.
REFERENCE   1
  AUTHORS   Schijffelen,M.J., Boel,C.H., van Strijp,J.A. and Fluit,A.C.
  TITLE     Whole genome analysis of a livestock-associated
            methicillin-resistant Staphylococcus aureus ST398 isolate from a
            case of human endocarditis
  JOURNAL   BMC Genomics 11, 376 (2010)
   PUBMED   20546576
  REMARK    Publication Status: Online-Only
REFERENCE   2  (bases 1 to 3158)
  AUTHORS   Schijffelen,M.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-APR-2008) Schijffelen M.J., Medical Microbiologie,
            UMC Utrecht, Heidelberglaan 100, Utrecht, 3584 CX, NETHERLANDS
FEATURES             Location/Qualifiers
     source          1..3158
                     /organism="Staphylococcus aureus subsp. aureus ST398"
                     /mol_type="genomic DNA"
                     /strain="ST398"
                     /isolate="S0385"
                     /db_xref="taxon:523796"
                     /plasmid="pS0385-3"
     gene            complement(775..1824)
                     /locus_tag="PSAPIG030001"
     CDS             complement(775..1824)
                     /locus_tag="PSAPIG030001"
                     /inference="protein motif:PFAM:PF01446"
                     /codon_start=1
                     /transl_table=11
                     /product="replication protein Rep"
                     /protein_id="CAQ51149.1"
                     /db_xref="GI:283471941"
                     /db_xref="GOA:D2NAV1"
                     /db_xref="InterPro:IPR000989"
                     /db_xref="UniProtKB/TrEMBL:D2NAV1"
                     /translation="MIKLRRLNQYWRSFYMQYNTTKCIDENQDNKTLKDMTKSGKQRP
                     WREKKIDNVSYADILEILKIKKAFNVKQCGNVLEFKPTDEGYLKLHKTWFCKSKLCPV
                     CNWRRAMKNSYQAQKVIEEVVKEKPTARWLFLTLSTKNAIDGDHLEQSLKHMSKAFNK
                     LKMYTKVKKNLVGFLRSTEVTVNKNDGSYNQHMHVLLCVENAYFRKKENYITQVEWVD
                     LWQKALQVNYRPVANIKAIKPNQKGDKDIQAAIKETSKYSVKSSDFLTDDDERNQEIV
                     NDLEKGLYRKRMLSYGGLLKQKHKILNLDDAEDGNLINTSDEDKTTDEEEKAHSITAI
                     WNFEKQNYYLKDLKR"
     gene            complement(1984..2850)
                     /locus_tag="PSAPIG030002"
     CDS             complement(1984..2850)
                     /locus_tag="PSAPIG030002"
                     /inference="protein motif:PFAM:PF06445"
                     /codon_start=1
                     /transl_table=11
                     /product="transcriptional regulator"
                     /protein_id="CAQ51150.1"
                     /db_xref="GI:283471942"
                     /db_xref="GOA:D2NAV2"
                     /db_xref="InterPro:IPR009057"
                     /db_xref="InterPro:IPR010499"
                     /db_xref="InterPro:IPR011256"
                     /db_xref="InterPro:IPR012287"
                     /db_xref="InterPro:IPR018060"
                     /db_xref="UniProtKB/TrEMBL:D2NAV2"
                     /translation="MDMILKNLNDSIDYIDQNLTKDLNLYDIAYFVGLPEQHYRNLFI
                     FLTGIGLSEYIKKRKLYFANKDLLNKESVTSVAIKYGYSIDGFTKSFKDWSGYLPSQI
                     YEEQVLISYPKLSFAINVKGGIDMKTRIVDLPKINIVGVKKRVRMQYEGVNNEIEELA
                     NSITNIQKEEMNNLQNIEPKEIVNVSYDADENFIKEEGYLTHMIGVITTENNISDQLD
                     VINIDESKWIVFENEGKFPKVLQDTYAKIYSEWLPETEYKLANVPMFSFTKFNDSDSD
                     NAYSEIWVAVNN"
ORIGIN      
        1 ttcgtcgtta taatctagtt aaatcccgta agggcgcact tatgcgttct aaaaagaacg
       61 cgtaaaaaac cataatcgac cttcgtcaat ttttatctta gttttaagga atcggcgttg
      121 tggcgcagat ataaaaatta atactgcacg aactagaatc gacgttgtag cgtagatata
      181 aaaattaata ctacatagaa atttaactaa gatttaagct actagttttt tgctagtagc
      241 ttttttattt tcttaatttt taaatgcctt tattttgaat tttaaggggg tatttagagc
      301 aaactctaaa tttatatagt tttatatacg tataatcata ttcatttctt aaaacgcaaa
      361 tatgagcaaa ataagggtat atctctctga catttatctt gtatttaaaa agacatatta
      421 cttacttggc taaagtcatt ttaaatacaa tcattcgtat ttttttagac ggccatttta
      481 aaagcgctat aaccctttta tatatttttc taatttgatt ggaactctct aaaatttata
      541 taacttaaaa agttaaattg tttagctcat tttttgttca tgaccagtgg caaatttttt
      601 agacactgcc cagttatatg caaatttaaa aattagcaaa ctggacagaa aaaaataaac
      661 actgcccatt tacatgcaaa cttaaaagtt aacatgaaaa ctggtcaacc atgccgattg
      721 aacgctatag ttcccgcagg ggcaaaaaga cataaaaaaa cgctagcttt taagctaacg
      781 tttcaaatct tttaagtaat aattttgttt ttcaaaattc caaattgcag taattgaatg
      841 tgctttttct tcttcgtctg ttgttttatc ttcgtcactt gtattaatta aattgccatc
      901 ttctgcatcg tctaaattta aaattttatg tttttgttta agcaatccac cataactcaa
      961 catacgtttt cggtataaac ctttttccaa atcattcacg atttcttggt ttctttcatc
     1021 atcatcagtt aaaaaatcag atgacttaac cgaatattta gaggtttctt tgatagctgc
     1081 ttgaatatct ttatcgcctt tttgatttgg tttgatcgct ttaatatttg ctactggtcg
     1141 ataattaact tgtaatgctt tttgccataa atcgacccat tcaacttgcg taatgtagtt
     1201 ttcttttttt ctgaaatacg cattttcaac acacaataaa acgtgcatat gttgattata
     1261 actaccgtca tttttattaa ctgttacttc agttgaacgt aaaaaaccca ccaaattctt
     1321 tttaactttt gtatacatct ttaatttatt aaaagcctta gacatgtgtt ttaaactttg
     1381 ttctaaatgg tcgccgtcta ttgcattttt agtcgataaa gtaagaaata accaacgagc
     1441 agttggcttt tctttaacca cttcttcaat cactttttga gcttgataac tatttttcat
     1501 agcacgcctc caattacaaa ctgggcataa ttttgactta caaaaccatg tcttatgtaa
     1561 tttcaaataa ccttcatcag ttggcttgaa ttctaagacg ttaccacatt gttttacgtt
     1621 aaaagccttt ttaattttta agatttccag tatatctgca taacttacat tatctatctt
     1681 cttttctctc catgggcgtt gtttcccact tttcgtcata tcttttaatg ttttattatc
     1741 ttgattttcg tctatacatt tagtagtatt atattgcata taaaaagacc tccagtattg
     1801 atttaagcgt cgtaacttaa tcatattact ggagcttttt tatgtcaatt ttttctttca
     1861 cgcacaaaaa tacataaaaa cactttcata tcaacgataa accaactttt aatatctttg
     1921 agttatttct atatagtatc aagacaagaa gaaactcgtt tctaaaacct taaatatcta
     1981 aaatcaatta ttaactgcaa cccaaatttc actataagca ttatcactat ctgaatcatt
     2041 aaatttagta aaagaaaaca ttggtacatt agctaattta tattctgttt ctggcaacca
     2101 ttcagaataa attttagcgt atgtgtcttg caaaactttt ggaaatttac cttcattttc
     2161 aaagactatc catttacttt catcaatatt tataacatca agttgatcgc ttatattatt
     2221 ttctgttgtt ataacaccaa tcatatgagt taaataaccc tcttctttaa taaaattctc
     2281 atcagcatca tatgaaacat ttacaatttc tttaggttca atgttttgta aattgttcat
     2341 ttcttctttt tgtatattag taatactatt tgctagttct tcaatttcat tgttaacacc
     2401 ctcgtattgc atacgaacgc gtttttttac gcctactata ttaatttttg gcaaatctac
     2461 aattctagtt ttcatgtcta ttcctccttt aacattaata gcaaatgaaa gtttcggata
     2521 cgaaattaaa acttgttctt catatatttg agatggtaaa tacccactcc aatctttaaa
     2581 agactttgta aaaccatcaa ttgaatatcc atattttata gctacactcg ttactgattc
     2641 tttatttaat aaatctttgt tagcaaaata caattttctt ttttttatat actcagataa
     2701 accaatacca gtaagaaata taaataaatt tctataatgt tgctcgggta aaccaacaaa
     2761 atacgctata tcatataaat ttaaatcttt tgttaaattc tgatctatat aatctataga
     2821 atcatttaaa ttttttaata tcatatccat cacctttcaa tcctatagta atcaataatc
     2881 aaagaaatga ctcgataatt tatatataac aattatcgaa taaaaaataa ttgctttatt
     2941 ccaattgctt tattgacgtt gagcctcgga acccttaaca atcccaaaac ttgtcgaatg
     3001 gtcggcttaa tagctcacgc tatgccgaca ttcgtctgca agtttagtta agggttcttc
     3061 tcaacatcaa taaattttct cggcataaat gcgtgttctt ttcttgtctg accagtagac
     3121 aaagtagacc caactgttat tgctttacag gtctattt
//

