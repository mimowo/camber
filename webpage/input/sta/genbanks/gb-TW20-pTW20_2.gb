LOCUS       FN433598                3011 bp    DNA     circular BCT 20-JAN-2010
DEFINITION  Staphylococcus aureus subsp. aureus TW20, plasmid pTW20_2.
ACCESSION   FN433598
VERSION     FN433598.1  GI:269942329
DBLINK      Project: 36647
KEYWORDS    complete genome.
SOURCE      Staphylococcus aureus subsp. aureus TW20
  ORGANISM  Staphylococcus aureus subsp. aureus TW20
            Bacteria; Firmicutes; Bacillales; Staphylococcus.
REFERENCE   1
  AUTHORS   Holden,M.T., Lindsay,J.A., Corton,C., Quail,M.A., Cockfield,J.D.,
            Pathak,S., Batra,R., Parkhill,J., Bentley,S.D. and Edgeworth,J.D.
  TITLE     Genome sequence of a recently emerged, highly transmissible,
            multi-antibiotic- and antiseptic-resistant variant of
            methicillin-resistant Staphylococcus aureus, sequence type 239 (TW)
  JOURNAL   J. Bacteriol. 192 (3), 888-892 (2010)
   PUBMED   19948800
REFERENCE   2  (bases 1 to 3011)
  AUTHORS   Holden,M.T.G.
  TITLE     Direct Submission
  JOURNAL   Submitted (30-JUL-2009) Holden M.T.G., The Wellcome Trust Sanger
            Institute, Pathogen Sequencing Unit, Wellcome Trust Genome Campus,
            Hinxton, Cambridge, CB10 1SA, UNITED KINGDOM
FEATURES             Location/Qualifiers
     source          1..3011
                     /organism="Staphylococcus aureus subsp. aureus TW20"
                     /mol_type="genomic DNA"
                     /strain="TW20"
                     /sub_species="aureus"
                     /db_xref="taxon:663951"
                     /plasmid="pTW20_2"
                     /country="United Kingdom:London"
     gene            complement(646..1641)
                     /locus_tag="SATW20_p2010"
     CDS             complement(646..1641)
                     /locus_tag="SATW20_p2010"
                     /codon_start=1
                     /transl_table=11
                     /product="replication protein"
                     /protein_id="CBA12171.1"
                     /db_xref="GI:269942330"
                     /db_xref="GOA:D1GV10"
                     /db_xref="InterPro:IPR000989"
                     /db_xref="UniProtKB/TrEMBL:D1GV10"
                     /translation="MQYNTTRSITENQDNKTLKDMTKSGKQRPWREKKIDNVSYADIL
                     EILKIKKAYNVKQCGNVLEFKPTDEGYLKLHKTWFCKSKLCPVCNWRRAMKNSYQAQK
                     VIEEVIKEKPKARWLFLTLSTKNAIDGDTLEQSLKHLTKAFDRLSRYKKVKQNLVGFM
                     RSTEVTVNKNDGSYNQHMHVLLCVENAYFRKKENYITQEEWVNLWQKALQVDYRPVAN
                     IKAIKPNQKGDKDIESAIKETSKYSVKSSDFLTDNDEKNQEIVSDLEKGLYRKRMLSY
                     GGLLKQKHKILNLDDAEDGNLINTSDEDKITDEEAKAHSITAIWNFEKQNYYLRH"
     gene            2093..2674
                     /locus_tag="SATW20_p2020"
     CDS             2093..2674
                     /locus_tag="SATW20_p2020"
                     /codon_start=1
                     /transl_table=11
                     /product="putative exported protein"
                     /protein_id="CBI50746.1"
                     /db_xref="GI:269942331"
                     /db_xref="UniProtKB/TrEMBL:D1GV09"
                     /translation="MNAIQKSVSLRKVLVISLSFLTILSMVLNFNFKEAQAQNKNNIA
                     DKNIETLNEKEIEKELDYIYGKIIVLDKDGTAKDVNLENAKSRYGYVPEGFQKLKNDI
                     ENKKKNSSISPRAVGGNYKNSNDCFYSEVLNSYGELLTGNIIAAVFDDVKAKNIKSAA
                     KKLARIGIKGNLAGIAATMVSKDVQCTLKYGWF"
ORIGIN      
        1 ctgtttaatt aagatttttt atttttcgtt atccaagagg cgcatttaca tacttaaaaa
       61 tgcgtcgagc taaaaccaaa atttacgtgt aataatcgaa tatacttatt ttgcatttta
      121 agggggtatt tagaacgaac tttaaaaata tatagtttta tacctgtatt atcttatttg
      181 tttcttaaaa cgcaaatatg agccaaataa atatattctg attttataat caaaaatttt
      241 ttataatcaa aaattcgatc aaacccagtg tcgatttttc ggacactgcc catttacatg
      301 caaattaaaa attagcataa aatcctttaa aacttccccc atgtttttct aaattcctgc
      361 agggcgcttt ttatcaaaaa tcgagaattt ttaatttttc tctaagaact ccgtaagagc
      421 ataaggagaa tttgctatca ttcaaaaatg ataggtctgg caaagccaga cattccaagg
      481 gttttaagta attttaaagt gatatcataa aaagctgtag tgtcacactt ttgtgacaca
      541 atatattgcg tttgtcacag ttttgtgaca ctacatacag catttgtcac aaaaataaga
      601 cagtactttt ttacaaaaaa tcaataaaaa agacatcagt catcactaat gtcttaaata
      661 ataattttgt ttttcaaaat tccaaattgc cgtaattgaa tgtgcttttg cttcttcatc
      721 tgttatttta tcttcatcac ttgtattaat taaattacca tcttcagcat cgtctaagtt
      781 taaaatttta tgcttttgtt taagcaatcc accataactt aacatacgtt ttcgatataa
      841 acctttttct aaatcactta caatttcttg atttttttcg tcattgtcag ttaaaaaatc
      901 agatgactta accgaatatt ttgatgtctc tttgattgcc gattcaatat ctttatcgcc
      961 tttttgatta ggtttaattg ctttaatatt tgcaacaggt cgataatcaa cttgtaatgc
     1021 tttttgccat aaattaaccc attcttcttg agttatataa ttctcttttt ttctaaaata
     1081 tgcattttca acacataaca aaacgtgcat atgctgatta taactaccat catttttatt
     1141 aacggtaact tctgttgaac gcataaatcc aacaagattt tgtttaacct ttttatatct
     1201 actcaaccta tcaaatgctt tagttagatg cttcaaactt tgttctaaag tatctccatc
     1261 tatcgcattt ttggttgaaa gtgttaaaaa caaccaacgt gcttttggct tttccttaat
     1321 tacttcttca atcacttttt gagcttgata actatttttc atagcacgcc tccaattaca
     1381 aactggacat agctttgatt tacaaaacca cgtcttatgt aatttcaaat aaccttcatc
     1441 ggtcggcttg aattctaaga cgttaccaca ttgttttacg ttataagcct tttttatttt
     1501 taaaatttca agtatatctg catagcttac attatctatt ttcttttctc tccatgggcg
     1561 ttgtttccca cttttcgtca tatcttttaa cgttttatta tcttgatttt cggttatact
     1621 tctagtagta ttatattgca tataaaaaga cccccagtat tatttaagat tcgacacctt
     1681 aataatataa ctggggtttt ttattgtcaa attttccttt cacgcacaaa aacacgttta
     1741 aacgctgtaa tatcaacagt tacagctata tatataaaat cgagttattt ctatatagta
     1801 tcaagacaag aagaaactcg ttttcaactc gtttcaaaaa caatctattt ctttttctct
     1861 ttatagccct ctaaacctaa ataaatcaat gtgaaaacca acgaataaat aaagctttct
     1921 aaaaagtcaa aaggattatt aaataaaatc gaattaacaa aattaaatat taaaaatacg
     1981 gacaataaaa caattaaaaa ataaataaac ttattcatat taaactcctt tgtttttata
     2041 tgttaatatt atattggatt catttaaata acaaaacaag ggggcattta taatgaatgc
     2101 aattcaaaaa agtgtatcac taagaaaagt tttagtaata tctttaagtt ttttaactat
     2161 actttcaatg gtcttgaatt ttaattttaa agaagctcaa gcccaaaaca aaaacaacat
     2221 cgcagataaa aacattgaaa ctttaaacga aaaagaaatc gaaaaagaac tagactacat
     2281 atatggcaaa attattgttt tagataaaga cggaaccgct aaagatgtta atctagaaaa
     2341 tgccaaatct agatatggat atgttcctga aggtttccaa aaactaaaga acgacataga
     2401 gaataaaaag aaaaatagct caatatcacc tagagcagtt ggtggaaact acaaaaatag
     2461 taatgactgt ttctattctg aagttttaaa ctcatatgga gaacttttaa ccggtaatat
     2521 aatcgcagca gtttttgatg atgtaaaagc caaaaacata aagagtgcag caaaaaaatt
     2581 agccagaatt ggcataaaag gaaatttagc tggaattgca gctactatgg tttcaaagga
     2641 tgtacaatgt actctgaaat acggatggtt ctaaatatat aatttattcc aattgcttta
     2701 ttgacgttga gcctcggaac ccttaacaat ccctaaactt gtcgaatcgt cggcttaata
     2761 gctcacgcta tgccgacatt cgtcttcaag tttagttaag ggttcttctc aacatcaata
     2821 aattttctcg gcataaatgc atgtttgatt atgcgtgtaa ctatccctta aataaaacat
     2881 taattgtgca ctgatttact aattattatc ccttgctttc attcacacta taatcaacac
     2941 cttaattcta aacatctgaa gtaatcataa tttttaaacc taataaaatt cgtgactatc
     3001 ttagttaaac t
//

