import os

from utils.coev_input import *
from src.method.coev_methods import calcDetParsMutModel

if __name__ == '__main__':
    mtype = "m"
    exp_dir = "mtb"
    input_dir = "../../input/"+exp_dir + "/"
    output_dir = "../../output/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "mutations_bin_sel_"+mtype+".txt")
    mutations_bin = readMutationsBin(input_fh)
    input_fh.close()
    
    
    zero_score = 0.0
    zero_score_c = 0
    
    non_zero_score = 0.0
    non_zero_score_c = 0
    
    input_fh = open(output_dir + "profile_pairs_ppi_" + mtype + ".txt")
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 2:
            continue
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        score = float(tokens[2])
        ppi_score = float(tokens[3])
        if ppi_score < 100.0:
            zero_score_c += 1
            zero_score += score
        else:
            non_zero_score += score
            non_zero_score_c += 1
    input_fh.close()
    print(zero_score/float(zero_score_c))
    print(non_zero_score/float(non_zero_score_c))
    
    