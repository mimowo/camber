from utils.coev_input import readStrains, readPhyloTree, readMutationsBin
from src.method.coev_methods import calcDetParsMutModel


if __name__ == '__main__':
    mtype = "m"
    exp_dir = "mtb"
    input_dir = "../../input/"+exp_dir + "/"
    output_dir = "../../output/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "phylo_tree.txt")
    phylo_tree = readPhyloTree(input_fh, strains_list=strains_list)
    input_fh.close()

    input_fh = open(input_dir + "mutations_bin_sel_"+mtype+".txt")
    mutations_bin = readMutationsBin(input_fh)
    input_fh.close()
    
    output_fh = open(output_dir + "mutations_bin_sel_stats_"+mtype+".txt", "w")
    for profile_id in sorted(mutations_bin.keys()):
        mutation_bin = mutations_bin[profile_id]
        phylo_tree.setObservedMutProfile(mutation_bin)
        changes = calcDetParsMutModel(phylo_tree)
        count_1 = mutation_bin.count('1')
        count_q = mutation_bin.count('?')
        output_fh.write(str(profile_id) + "\t" + str(changes) + "\t" + str(count_1) + "\t" + str(count_q) + "\n")
    output_fh.close()
    
    
    