from src.structs.mutation_det import MutationDet
from src.structs.phylo_tree import PhyloTreeNode, PhyloTree

def readStrains(input_fh):
    strains = []
    
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        if len(line) > 0 and line[0] == "#":
            continue
        tokens = line.split()
        if len(tokens) >= 1:
            strains.append(tokens[0])

    return strains

def readMutations(input_fh, mtype = "a", strains_ord = None, gene_names = None):
    mutations_det = []
    
    lines = input_fh.readlines()
    line0 = lines[0].strip()
    tokens = line0.split("\t")
    strain_tokens = tokens[5:]
    strains_rev = [0]*(len(tokens) - 5)
    if strains_ord != None:
        for i in range(len(strain_tokens)):
            strains_rev[i] = strains_ord.index(strain_tokens[i])
    else:
        for i in range(len(strain_tokens)):
            strains_rev[i] = i   
    
    for i in range(1, len(lines), 1):
        line = lines[i].strip()
        tokens = line.split()
        if len(tokens) == 0:
            continue
        cc_id = tokens[0]
        filename_id = tokens[1]
        gene_name = tokens[2]
        desc = tokens[3]
        if gene_names != None and cc_id in gene_names:
            gene_name = gene_names[cc_id]
        if mtype == "g" and desc != "gene":
            continue
        elif mtype == "m" and not desc.startswith("snp"):
            continue
        position = int(tokens[4])
        
        full_profile = ['X']*(len(tokens) - 5)
        for j in range(5, len(tokens), 1):
            full_profile[strains_rev[j-5]] = tokens[j]
        mutation = MutationDet(full_profile, desc, gene_name, cc_id, position)
        mutations_det.append(mutation)
        
    return mutations_det
    
def splitStrSubtrees(strains_str):
    strains_strs = []
    curr_str = ""
    count = 0
    for i in range(0, len(strains_str), 1):
        if(strains_str[i]=="("):
            curr_str += strains_str[i]
            count += 1
        elif(strains_str[i]==")"):
            curr_str += strains_str[i]
            count -= 1
        elif(count == 0 and strains_str[i]==","):
            strains_strs.append(curr_str)
            curr_str = ""
        else:
            curr_str += strains_str[i]
    strains_strs.append(curr_str)
    
    return strains_strs

def readPhyloTreeRec(tree, tree_str, strains_list, e_len=None):
    last_index = tree_str.rfind(")")
    if last_index == -1:
        tokens = tree_str.split(":")
        strain_name = tokens[0]
        
        strain_id = strains_list.index(strain_name)
        strain_node = PhyloTreeNode(strain_id)
        if len(tokens) == 2 and e_len == None:
            strain_node.e_len = float(tokens[1])
        elif e_len == None:
            strain_node.e_len = 1.0
        else:
            strain_node.e_len = e_len
        tree.addLeafNode(strain_node)
        return strain_node
        
    else:
        node_id = tree.createNodeID("")
        
        node = PhyloTreeNode(node_id)
        if last_index + 2 < len(tree_str):
            len_index = tree_str.rfind(":")
            if e_len == None and len_index > 0:
                node.e_len = float(tree_str[len_index+1:])
            elif e_len == None:
                node.e_len = 1.0
            else:
                node.e_len = e_len
        
        tree.addInternalNode(node)
        subtrees_str = splitStrSubtrees(tree_str[1:last_index])
        
        for subtree_str in subtrees_str:
            subtree = readPhyloTreeRec(tree, subtree_str, strains_list, e_len)
            node.addChildren(subtree)
            subtree.parent = node
        return node

def readPhyloTree(input_fh, strains_list, e_len=None):
    tree = PhyloTree(len(strains_list))
    tree_str = ""
    lines = input_fh.readlines()
    for line in lines:
        tree_str += line.strip()
    
    root = readPhyloTreeRec(tree, tree_str, strains_list, e_len)
    tree.setRoot(root)
    tree.setLeavesByIDs(range(tree.leaves_count))
    return tree
    
def readMutationsBin(input_fh, msets = False):
    mutations_bin = {}
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) != 2:
            continue
        if msets == False:
            mutations_bin[int(tokens[0])] = tokens[1]
        else:
            ms, qs = set([]), set([])
            for i in range(len(tokens[1])):
                if tokens[1][i] == '1':
                    ms.add(i)
                elif tokens[1][i] == '?':
                    qs.add(i)
            mutations_bin[int(tokens[0])] = (ms, qs)
    return mutations_bin
    
def readCCGeneNamesMapping(input_fh, ref_strain):
    cc_gene_name_map = {}
    
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        cc_id = tokens[0]
        strain_id = tokens[1]
        gene_id = tokens[3]
        if gene_id == "x":
            continue
        if strain_id == ref_strain:
            cc_gene_name_map[cc_id] = gene_id
    return cc_gene_name_map

def readPPInetwork(input_fh):
    ppi_pairs = {}
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 2:
            continue
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        
        if gene1_id.count(".") > 0:
            gene1_id = gene1_id.split(".")[1]
            gene2_id = gene2_id.split(".")[1]
        
        score = float(tokens[2])
        if not (gene2_id, gene1_id) in ppi_pairs and not (gene1_id, gene2_id) in ppi_pairs:
            ppi_pairs[(gene1_id, gene2_id)] = score
        
    return ppi_pairs
    