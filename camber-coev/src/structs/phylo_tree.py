class PhyloTreeNode:
    def __init__(self, node_id):
        self.visited = False
        self.node_id = node_id
        self.children = []
        self.parent = None 
        self.e_len = 1.0
    def addChildren(self, child_node):
        self.children.append(child_node)
    def setParent(self, parent):
        self.parent = parent

    def toString(self):
        n = len(self.children)
        if n==0:
            return str(self.node_id)
        else:
            text = "("
            i = 0
            for child in self.children:
                text += child.toString()
                i += 1
                if i < n:
                    text += ","
            text += ")"
            return text

class PhyloTree:
    def __init__(self, leaves_count):
        self.root_id = 0
        self.root = None
        self.nodes = {}
        self.leaves_count = leaves_count
        self.nodes_count = leaves_count
        self.leaves = set([])
        self.leaf_ids = set([])

    def setObservedMutProfile(self, mut_profile):
        for i in set(range(len(mut_profile))) & set(self.nodes.keys()):
            self.nodes[i].observed_mut_state = mut_profile[i]

    def setCompleteMutProfile(self, mut_profile):
        for i in set(range(len(mut_profile))) & set(self.nodes.keys()):
            self.nodes[i].complete_mut_state = mut_profile[i]
            
    def setLeavesByNodes(self, leaves):
        self.leaves = leaves 
        for leaf in leaves:
            self.leaf_ids.add(leaf.node_id)

    def setLeavesByIDs(self, leaf_ids):
        self.leaf_ids = leaf_ids
        for leaf_id in leaf_ids:
            self.leaves.add(self.nodes[leaf_id])
            
    def setRoot(self, root):
        self.root_id = root.node_id
        self.root = root
        
    def addLeafNode(self, node):
        self.nodes[node.node_id] = node
        
    def addInternalNode(self, node):
        self.nodes[node.node_id] = node
        self.nodes_count += 1
        
    def createNodeID(self, node_name):
        return self.nodes_count
    
    def calcChanges(self):
        root = self.nodes[self.root_id]
        n_zero, n_one = self.calcChangesRec(root)
        return min(n_zero,n_one)
    
    def calcChangesRec(self, node):
        if node.node_id in self.leaf_ids:
            return node.n_zero, node.n_one
        else:
            n_zeros = []
            n_ones = []
            for subnode in node.children:
                n_zero, n_one = self.calcChangesRec(subnode)
                n_zeros.append(n_zero)
                n_ones.append(n_one)
            zero_yes = False
            one_yes = False
            for i in range(0, len(n_zeros), 1):
                if n_zeros[i] > n_ones[i]:
                    zero_yes = True 
                if n_ones[i] > n_zeros[i]:
                    one_yes = True
            if zero_yes:
                n_zero_res = 1
            else:
                n_zero_res = 0
            if one_yes:
                n_one_res = 1
            else:
                n_one_res = 0
            for i in range(0, len(n_zeros), 1):
                n_zero_res += min(n_zeros[i], n_ones[i])
                n_one_res += min(n_zeros[i], n_ones[i])
            
            #print node.node_id, n_zero_res, n_one_res
            node.n_zero = n_zero_res
            node.n_one = n_one_res
            return n_zero_res, n_one_res
            
    def __str__(self):
        txt = "TREE ROOT ID: " + str(self.root_id) + "\n"
        for i in self.nodes:
            node_i = self.nodes[i]
            parent = node_i.parent
            if parent == None: 
                txt += str(i) + "\t" + str("ROOT:")+str("%.5f" % 0.0)+"\t" #+ str(len(node_i.children)) +":"
            else:
                txt += str(i) + "\t" + str(parent.node_id) + ":"+str("%.5f" % node_i.e_len)+"\t" #+ str(len(node_i.children)) +":"
            for node_j in node_i.children:
                txt += str(node_j.node_id) + ","
            txt = txt[:-1] + "\n"
             
        return txt
            
        