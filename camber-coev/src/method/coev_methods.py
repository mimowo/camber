from collections import deque

def calcSubleavesMutCount(phylo_tree):
    queue = deque([])
    for node in phylo_tree.nodes.values():
        node.visited = 0
    for i in phylo_tree.leaf_ids:
        node = phylo_tree.nodes[i]
        queue.append(node.parent)

        if node.observed_mut_state == '1':
            node.pres_subleaves_count = 1
            node.abs_subleaves_count = 0
            node.q_subleaves_count = 0
            node.subleaves_count = 1
        elif node.observed_mut_state == '0':
            node.pres_subleaves_count = 0
            node.abs_subleaves_count = 1
            node.q_subleaves_count = 0
            node.subleaves_count = 1
        else:
            node.pres_subleaves_count = 0
            node.abs_subleaves_count = 0
            node.q_subleaves_count = 1
            node.subleaves_count = 1
            
    while queue:
        node = queue.popleft()
        if node == None:
            continue
        node.visited += 1
        if node.visited < len(node.children):
            continue
        i = node.node_id
        
        children_list = list(node.children)
      
        node.pres_subleaves_count = 0
        node.abs_subleaves_count = 0
        node.q_subleaves_count = 0
        node.subleaves_count = 0

        for j in range(len(children_list)):
            node.pres_subleaves_count += children_list[j].pres_subleaves_count
            node.abs_subleaves_count += children_list[j].abs_subleaves_count
            node.q_subleaves_count += children_list[j].q_subleaves_count
            node.subleaves_count += children_list[j].subleaves_count
        queue.append(node.parent)
    return None    

def parentState(children_states):
    only_q = True
    is_zero = False
    if len(children_states) > 0:
        for ch_state in children_states:
            if ch_state == '0':
                is_zero = True
                only_q = False
            elif ch_state == '1':
                only_q = False    
        if is_zero:
            return '0'
        elif only_q:
            return '?'
        else:
            return '1'
    return '0'

def calcDetParsMutModel(phylo_tree):
    calcSubleavesMutCount(phylo_tree)
    phylo_tree.setCompleteMutProfile(['?']*phylo_tree.nodes_count)
        
    queue = deque([])
        
    for i in phylo_tree.leaf_ids:
        node = phylo_tree.nodes[i]
        node.complete_mut_state = node.observed_mut_state
        node.changes = 0
        queue.append(node)
    
    while queue:
        node_i = queue.popleft()
        if node_i == None:
            continue    
        node_i.visited += 1
        if node_i.visited < len(node_i.children):
            continue
        
        changes_tmp = 0
        only_q = True
        is_zero = False
        if len(node_i.children) > 0:
            for node_j in node_i.children:
                if node_j.complete_mut_state == '0':
                    is_zero = True
                    only_q = False
                    changes_tmp += node_j.changes
                elif node_j.complete_mut_state == '1':
                    only_q = False
                    changes_tmp += 1        
                else:
                    pass
        
            if is_zero:
                node_i.changes = changes_tmp
                node_i.complete_mut_state = '0'
            elif only_q:
                node_i.changes = 0
                node_i.complete_mut_state = '?'
            else:
                node_i.complete_mut_state = '1'
                node_i.changes = 0
            
        queue.append(node_i.parent)
    return phylo_tree.root.changes

def stable(mut1_from='1', mut1_to='0', mut2_from='0', mut2_to='1', theta=0.5):
    
    if (mut1_from, mut1_to, mut2_from, mut2_to)    == ('0', '0', '0', '0'):
        return 0
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('0', '0', '0', '1'):
        return 0
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('0', '0', '1', '0'):
        return 0   
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('0', '0', '1', '1'):
        return 0    
    
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('0', '1', '0', '0'):
        return 0
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('0', '1', '0', '1'):
        return 1
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('0', '1', '1', '0'):
        return -1   
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('0', '1', '1', '1'):
        return theta   
    
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '0', '0', '0'):
        return 0
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '0', '0', '1'):
        return -1
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '0', '1', '0'):
        return 1
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '0', '1', '1'):
        return 0
    
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '1', '0', '0'):
        return 0
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '1', '0', '1'):
        return theta
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '1', '1', '0'):
        return -1   
    elif (mut1_from, mut1_to, mut2_from, mut2_to)  == ('1', '1', '1', '1'):
        return 0
    return 0


def scoreMutPair(profile1, profile2, phylo_tree):

    queue = deque([])
    for node in phylo_tree.nodes.values():
        node.visited = 0

    for i in phylo_tree.leaf_ids:
        node = phylo_tree.nodes[i]
        node.tscore = 0.0
        queue.append(node.parent)
        node.complete_mut_state1 = profile1[i]
        node.complete_mut_state2 = profile2[i]
                
    while queue:
        node = queue.popleft()
        if node == None:
            continue
        node.visited += 1
        if node.visited < len(node.children):
            continue
        i = node.node_id

        node.tscore = 0.0    
        
        children_list = list(node.children)
        
        node_states1 = []
        node_states2 = []
        
        for j in range(len(children_list)):
            node_j = children_list[j]
            node_states1.append(node_j.complete_mut_state1)      
            node_states2.append(node_j.complete_mut_state2)
            
        node.complete_mut_state1 = parentState(node_states1)
        node.complete_mut_state2 = parentState(node_states2)
        node_mut1 = node.complete_mut_state1
        node_mut2 = node.complete_mut_state2

        for j in range(len(children_list)):
            node_j = children_list[j]
            
            node_j_mut1 = node_j.complete_mut_state1
            node_j_mut2 = node_j.complete_mut_state2
            
            node.tscore  += (stable(node_mut1, node_j_mut1, node_mut2, node_j_mut2, 0.5) + node_j.tscore)
        queue.append(node.parent)
            
    return phylo_tree.root.tscore   
