from src.utils.coev_input import *

def convertMutations(mutations_det):
    mut_bin_profiles_map = {}
    mut_bin_profiles_map_rev = {}
    mut_bin_desc_map = {}
    
    profile_id = 0
    
    for mutation_det in mutations_det:
        state_counts = {}
        for i in range(0, len(mutation_det.full_profile), 1):
            state_raw = mutation_det.full_profile[i]
            tokens = state_raw.split(":")
            state = state_raw[0]
            
            if state in ['-', '?']:
                continue
            
            if not state in state_counts:
                state_counts[state] = 0
            state_counts[state] += 1
            
        max_count = 0
        for state in state_counts.keys():    
            if state_counts[state] > max_count:
                max_count = state_counts[state]
        ref_state = '?'
        for state in state_counts.keys():
            if state_counts[state] == max_count:
                ref_state = state
                
        bin_profile = ""
        for i in range(0, len(mutation_det.full_profile), 1):
            state_raw = mutation_det.full_profile[i]
            tokens = state_raw.split(":")
            state = state_raw[0]
            if state in ['?', "-"]:
                bin_profile += '?'
            elif state == ref_state:
                bin_profile += '0'
            else:
                bin_profile += '1'
        if not bin_profile in mut_bin_profiles_map_rev.keys():
            mut_bin_desc_map[profile_id] = set([(mutation_det.gene_id, mutation_det.position)])
            mut_bin_profiles_map[profile_id] = bin_profile
            mut_bin_profiles_map_rev[bin_profile] = profile_id
            profile_id += 1
        else:
            p_id = mut_bin_profiles_map_rev[bin_profile]
            mut_bin_desc_map[p_id].add((mutation_det.gene_id, mutation_det.position))
    return mut_bin_profiles_map, mut_bin_desc_map

if __name__ == '__main__':
    mtype = "m"
    exp_dir = "spe"
    input_dir = "../../input/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "gene_families.txt")
    gene_names = readCCGeneNamesMapping(input_fh, "TIGR4")
    input_fh.close()
    
    input_fh = open(input_dir + "mutations_all.txt")
    mutations_det = readMutations(input_fh, mtype=mtype, strains_ord=strains_list, gene_names=gene_names)
    input_fh.close()
    
    mutations_bin_map, mutations_desc_map = convertMutations(mutations_det)   
    
    output_fh = open(input_dir + "mutations_bin_"+mtype+".txt", "w")
    for profile_id in mutations_bin_map.keys():
        text_line = str(profile_id) + "\t" + mutations_bin_map[profile_id] + "\n"
        output_fh.write(text_line)
    output_fh.close()

    output_fh = open(input_dir + "mutations_bin_desc_"+mtype+".txt", "w")
    for profile_id in mutations_desc_map.keys():
        text_line = str(profile_id) + "\t"
        for (gene_id, position) in mutations_desc_map[profile_id]:
            text_line += gene_id + ":" + str(position) + ";"
        text_line = text_line[:-1] + "\n"
        output_fh.write(text_line)
    output_fh.close()    
    
    