from utils.coev_input import readStrains, readPhyloTree, readMutationsBin
from src.method.coev_methods import calcDetParsMutModel

thr = 4

if __name__ == '__main__':
    mtype = "m"
    exp_dir = "spe"
    input_dir = "../../input/"+exp_dir + "/"
    output_dir = "../../output/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()

    input_fh = open(input_dir + "mutations_bin_sel_"+mtype+"_"+str(thr)+".txt")
    mutations_bin_map = readMutationsBin(input_fh, msets=True)
    input_fh.close()
    
    output_fh = open(input_dir + "profile_pairs_"+mtype+"_"+str(thr)+".txt", "w")
    
    profile_ids = sorted(mutations_bin_map.keys())
    for i in range(len(profile_ids)):
        m1, q1 = mutations_bin_map[profile_ids[i]]
        for j in range(i+1):
            m2, q2 = mutations_bin_map[profile_ids[j]]
            if len(m1 & m2) >= 8:
                output_fh.write(str(profile_ids[i]) + "\t" + str(profile_ids[j]) + "\n")
    output_fh.close()
    
    
    