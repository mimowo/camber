from method.coev_methods import calcDetParsMutModel
from utils.coev_input import readStrains, readPhyloTree, readMutationsBin

thr = 4

def filteroutMutationsBin(phylo_tree, mutations_bin):
    mutations_bin_sel = {}
    
    for profile_id in mutations_bin:
        mutation_bin = mutations_bin[profile_id]
        if mutation_bin.count("?") > 0.02*len(mutation_bin):
          #  print("here", mutation_bin.count("?"), len(mutation_bin), mutation_bin)
            continue
        phylo_tree.setObservedMutProfile(mutation_bin)
        count = calcDetParsMutModel(phylo_tree)
        if count > thr:
            mutations_bin_sel[profile_id] = mutation_bin
    
    return mutations_bin_sel

if __name__ == '__main__':
    mtype = "m"
    exp_dir = "spe"
    input_dir = "../../input/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "phylo_tree.txt")
    phylo_tree = readPhyloTree(input_fh, strains_list=strains_list)
    input_fh.close()

    input_fh = open(input_dir + "mutations_bin_"+mtype+".txt")
    mutations_bin = readMutationsBin(input_fh)
    input_fh.close()
    
    mutations_bin_sel = filteroutMutationsBin(phylo_tree, mutations_bin)
    
    output_fh = open(input_dir + "mutations_bin_sel_"+mtype+"_"+str(thr)+".txt", "w")
    for profile_id in sorted(mutations_bin_sel.keys()):
        text_line = str(profile_id) + "\t" + mutations_bin_sel[profile_id] + "\n"
        output_fh.write(text_line)
    output_fh.close()
    