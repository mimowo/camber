import os

from utils.coev_input import *
from src.method.coev_methods import calcDetParsMutModel

def readScoredProfilePairs(input_fh):
    scored_pairs = {}
    
    for line in input_fh.readlines():
        tokens = line.strip().split()
        
        if len(tokens) < 3:
            continue
        score = float(tokens[2])
        scored_pairs[(int(tokens[0]), int(tokens[1]))] = score
    return scored_pairs

def readProfileGenes(input_fh):
    profile_genes = {}
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 1:
            continue
        profile_id = int(tokens[0])

        if not profile_id in profile_genes:
            profile_genes[profile_id] = set([])
        mut_desc_tokens = tokens[1].split(";")
        for mut_desc_token in mut_desc_tokens:
            gene_id = mut_desc_token.split(":")[0]
            profile_genes[profile_id].add(gene_id)
            
    return profile_genes

thr = 4

if __name__ == '__main__':
    mtype = "m"
    exp_dir = "spe"
    input_dir = "../../input/"+exp_dir + "/"
    output_dir = "../../output/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "mutations_bin_sel_"+mtype+"_" + str(thr) + ".txt")
    mutations_bin = readMutationsBin(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "mutations_bin_desc_"+mtype+".txt")
    profile_genes = readProfileGenes(input_fh) 
    input_fh.close()
    
    input_fh = open(output_dir + "scored_pairs_"+mtype+"_" + str(thr) + ".txt")
    read_scored_profile_pairs = readScoredProfilePairs(input_fh) 
    input_fh.close()
    
    input_fh = open(input_dir + "ppi_network_0.txt")
    ppi_network = readPPInetwork(input_fh)
    input_fh.close()
    
    scored_gene_pairs = {}
    
    for (profile1_id, profile2_id) in read_scored_profile_pairs:
        score = read_scored_profile_pairs[(profile1_id, profile2_id)]
        profile1_genes = list(profile_genes[profile1_id])
        profile2_genes = list(profile_genes[profile2_id])
        for gene1_id in profile1_genes:
            for gene2_id in profile2_genes:
                if gene1_id == gene2_id:
                    continue
                if not gene1_id.startswith("SP") or not gene2_id.startswith("SP"):
                    continue
                if (gene1_id, gene2_id) in scored_gene_pairs and scored_gene_pairs[(gene1_id, gene2_id)] < score:
                    scored_gene_pairs[(gene1_id, gene2_id)] = score
                elif (gene2_id, gene1_id) in scored_gene_pairs and scored_gene_pairs[(gene2_id, gene1_id)] < score:
                    scored_gene_pairs[(gene2_id, gene1_id)] = score
                elif not (gene1_id, gene2_id) in scored_gene_pairs:
                    scored_gene_pairs[(gene1_id, gene2_id)] = score
                    
    scored_gene_pairs_sorted = sorted(scored_gene_pairs, key=lambda gp:scored_gene_pairs[gp])
    output_fh = open(output_dir + "profile_pairs_ppi_" + mtype +"_" + str(thr) + ".txt", "w")
    for (gene1_id, gene2_id) in scored_gene_pairs_sorted:
        ppi_score = 0.0
        if (gene1_id, gene2_id) in ppi_network:
            ppi_score = ppi_network[(gene1_id, gene2_id)]
        if (gene2_id, gene2_id) in ppi_network:
            ppi_score = ppi_network[(gene2_id, gene1_id)]
        score = scored_gene_pairs[(gene1_id, gene2_id)]
                            
        output_fh.write(gene1_id + "\t" + gene2_id + "\t" + str(score) + "\t" + str(ppi_score) + "\n")
    output_fh.close()
    
    