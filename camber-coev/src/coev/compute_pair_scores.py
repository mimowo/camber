from utils.coev_input import readStrains, readPhyloTree, readMutationsBin
from src.method.coev_methods import scoreMutPair
from src.utils.coev_progress import CoevProgress
import subprocess
import os

def readProfilePairs(input_fh):
    pairs = []
    
    for line in input_fh.readlines():
        tokens = line.strip().split()
        
        if len(tokens) == 2:
            pairs.append((int(tokens[0]), int(tokens[1])))
    return pairs

thr = 4

def file_len(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

if __name__ == '__main__':
    mtype = "m"
    exp_dir = "spe"
    input_dir = "../../input/"+exp_dir + "/"
    output_dir = "../../output/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "phylo_tree.txt")
    phylo_tree = readPhyloTree(input_fh, strains_list=strains_list)
    input_fh.close()
    

    input_fh = open(input_dir + "mutations_bin_sel_"+mtype+"_" + str(thr)+".txt")
    mutations_bin_map = readMutationsBin(input_fh, msets=False)
    input_fh.close()
    
    count = 0
    read_size = 0
    total_size = os.path.getsize(input_dir + "profile_pairs_"+mtype+"_"+str(thr)+".txt")
    
    input_fh = open(input_dir + "profile_pairs_"+mtype+"_"+str(thr)+".txt")    
    output_fh = open(output_dir + "scored_pairs_" + mtype +"_"+str(thr)+".txt", "w")
    
    while 1:
        line = input_fh.readline()
        
        read_size += len(line)
        if count % 10000 == 0:
            print(count, read_size, total_size, 100*read_size/total_size, "%")
                    
        count += 1
        
        if not line:
            break
        tokens = line.strip().split()
        if len(tokens) != 2:
            continue
        profile1_id = int(tokens[0])
        profile2_id = int(tokens[1])
        profile1 = mutations_bin_map[profile1_id]
        profile2 = mutations_bin_map[profile2_id]
        
        score = scoreMutPair(profile1, profile2, phylo_tree)
        if score < 4.0:
            continue
        output_fh.write(str(profile1_id) + "\t" + str(profile2_id) + "\t" + str(score) + "\n")
    output_fh.close()
    input_fh.close()
    
    