from utils.coev_input import readStrains, readPhyloTree, readMutationsBin
from src.method.coev_methods import calcDetParsMutModel

def my_split(s, seps):
    res = [s]
    for sep in seps:
        s, res = res, []
        for seq in s:
            res += seq.split(sep)
    return res

if __name__ == '__main__':
    exp_dir = "spe"
    input_dir = "../../input/"+exp_dir + "/"
    output_dir = "../../output/"+exp_dir + "/"
    
    input_fh = open(input_dir + "strains_all.txt")
    strains_list = readStrains(input_fh)
    input_fh.close()    
    
    input_fh = open(input_dir + "phylo_tree.txt")
    tree_txt = input_fh.readline()
    input_fh.close()

    input_fh = open(input_dir + "phylo_tree.txt")
    tree = readPhyloTree(input_fh, strains_list)
    input_fh.close()    
    
    tree.root.toString()

    
    print(tree_txt)
    
    tokens = my_split(tree_txt, [")","(", ":", ","])
    print(tokens)
    
    strains_final = []
    for token in tokens:
        if len(token) > 1:
            strains_final.append(token)
            
    output_fh = open(input_dir + "strains.txt", "w")
    for strain_id in strains_final:
        output_fh.write(strain_id + "\n")
    output_fh.close()
            