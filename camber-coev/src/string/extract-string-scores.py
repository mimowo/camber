#!/usr/bin/python
import subprocess
import os
species_map = {"170187":"TIGR4"}; 
treshold = 0;

input_fn = 'D://data//protein.links.v9.0.txt'
exp_dir = "spe"
input_dir = "../../input/"+exp_dir + "/"
file_handle = open(input_fn)
output_fh = open(input_dir + "proteins.links.scores2." + str(treshold) + ".txt", "w")


def file_len(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

count = 0
read_size = 0
total_size = os.path.getsize(input_fn)
line = file_handle.readline();

while 1:        
    line = file_handle.readline();
    count += 1
    read_size += len(line)
    if count % 100000 == 0:
        print(count, read_size, total_size, 100*read_size/total_size, "%")
    
    if not line:
        break;
    line = line.strip()
    words = line.split();
#    print(line)
    if len(words) < 3:
        continue
    prot1_full_id = words[0]
    prot2_full_id = words[1]
    score = int(words[2])
    prot1_tokens = prot1_full_id.split(".")
    prot2_tokens = prot2_full_id.split(".")
    if len(prot1_tokens) < 2 or len(prot2_tokens)<2:
        continue
    prot1_sp = prot1_tokens[0]
    prot2_sp = prot2_tokens[0]
  #  print(line, "x"+prot1_sp, prot2_sp)
    if prot1_sp in species_map and prot2_sp in species_map:
         output_fh.write(line+"\n")
output_fh.close()

