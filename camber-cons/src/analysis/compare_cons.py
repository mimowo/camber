def readConsStats(input_fh):
    stats = {}
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) < 3:
            continue
        ann_id = tokens[0]
        sc1 = tokens[1]
        sc2 = tokens[2]
        stats[ann_id] = (sc1, sc2)
    return stats

if __name__ == '__main__':
    input_fh = open("conservation_all.txt")
    stats_all = readConsStats(input_fh)
    input_fh.close()
    
    input_fh = open("conservation_mtb.txt")
    stats_mtb = readConsStats(input_fh)
    input_fh.close()
    
    
    output_fh = open("conversation_cmp.txt", "w")
    for key_id in sorted(stats_all.keys() & stats_mtb.keys()):
        (st1, st2) = stats_all[key_id]
        (st3, st4) = stats_mtb[key_id]
        if key_id.startswith("Rv"):
            output_fh.write(key_id + "\t" + st1 + "\t"+ st2 + "\t"+ st3 + "\t"+ st4 + "\tY" + "\n")
        else:
            output_fh.write(key_id + "\t" + st1 + "\t"+ st2 + "\t"+ st3 + "\t"+ st4 + "\tN" + "\n")
    output_fh.close()
        
    
    