import time
import sys
import os
from multiprocessing import Pool
from urllib.request import urlopen


sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
    
patric_db = "http://brcdownloads.vbi.vt.edu/patric2/genomes/"

def selectStrains(lines):
    ret = []
    for line in lines:
        line = str(line).strip()
        path_id_ind1 = line.find('href="') + 6
        path_id_ind2 = line.find("/", path_id_ind1)
        strain_full_id = line[path_id_ind1:path_id_ind2]
        if strain_full_id.count(">") > 0 or strain_full_id.count("<") > 0:
            continue
        if len(strain_full_id) > 5:
            ret.append(strain_full_id)
    return ret

def downloadStrainFiles(full_strain_id):
    print("XXX", full_strain_id)
    seq_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.faa"
    try:
        code = urlopen(seq_fn).code
    except:
        seq_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".RefSeq.faa"
    try:
        code = urlopen(seq_fn).code
    except:
        print("NOT FOUND:", full_strain_id, seq_fn)
        return full_strain_id
        
    out_seq_fn = "/home/misias/cons/aa_seqs/seq-"+full_strain_id+".faa"
    
    if not os.path.exists(out_seq_fn) or os.path.getsize(out_seq_fn)<10:
        try:
            u = urlopen(seq_fn)
            localFile = open(out_seq_fn, 'wb')
            localFile.write(u.read())
            localFile.close()
        except:
            print("ERROR: " + seq_fn)
            print("ERROR: " + out_seq_fn)                

    return full_strain_id

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)

    print("Reading PATRIC database: " + patric_db)
    f = urlopen(patric_db)
    lines = f.readlines()
    f.close()

    strain_det_list = selectStrains(lines)
    
    print(len(strain_det_list))
    #for full_strain_id in strain_det_list:
    #    print(full_strain_id)
        
    home = "/home/misias/"
    ensure_dir(home + "cons/")
    ensure_dir(home + "cons/aa_seqs/")
    
    WORKERS = 10
    
    progress = CAMBerProgress("Download strains")
    progress.setJobsCount(len(strain_det_list))
    
    TASKS = strain_det_list
    
    if WORKERS > 1:
        pool = Pool(processes=WORKERS) 
        for r in pool.imap(downloadStrainFiles, TASKS):
            progress.update(desc=r)
    else:
        for TASK in TASKS:
            downloadStrainFiles(TASK)

    output_fh = open(home + "cons/" + "/strains-all.txt", "w")
    
    for full_strain_id in strain_det_list:
        output_fh.write(full_strain_id + "\n")
    output_fh.close()
    
    