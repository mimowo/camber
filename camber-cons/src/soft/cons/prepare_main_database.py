import sys
from multiprocessing import Pool


sys.path.append("../../../../camber2/src/")

from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import translateSequence
from soft.utils.camber_io_utils import ensure_dir
from soft.utils.camber_closure_utils import *
import os


cons_dir = "/home/misias/cons/"

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    genome_id = "H37Rv"
      
    genome_fn = cons_dir + "/aa_"+genome_id+".fasta"
    genome_fn = os.path.abspath(genome_fn)
    formatdb_exe = parameters["BLAST_FORMATDB_PATH"]
    
    os.system(formatdb_exe+" -p T -i "+genome_fn)
    