import sys
from multiprocessing import Pool


sys.path.append("../../../../camber2/src/")
import os
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *

cons_dir = "/home/misias/cons/"

def convertSequence(genome_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
        
    ensure_dir(cons_dir + "/sel_seq/")
    input_fn = cons_dir + "/aa_seqs/seq-"+genome_id+".faa"
    output_fn = cons_dir + "/sel_seq/seq-"+genome_id+".fasta"
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 100:
        return ""
    if os.path.exists(output_fn) and os.path.getsize(output_fn) > 100:
        return genome_id
    input_fh = open(input_fn)
    output_fh = open(output_fn, "w")

    seq_map = readSequenceFromFile(input_fh)
    saved = False
    if len(seq_map) == 1:
        seq = list(seq_map.values())[0]
        writeFASTAseq(output_fh, seq.upper(), genome_id);
    else:
        for seq_id in seq_map:
            seq = seq_map[seq_id]
            if len(seq) > 0:
                saved = True
                writeFASTAseq(output_fh, seq.upper(), seq_id);

    output_fh.close()
    if not saved:
        os.remove(output_fn)    
        return ""
    return genome_id

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    genomes_list = []
    input_fh = open(cons_dir + "strains-all.txt")    
    for line in input_fh.readlines():
        genomes_list.append(line.strip())
    input_fh.close()
    
    progress = CAMBerProgress("Formatting sequences")
    n = len(genomes_list)
    progress.setJobsCount(n)
    
    #genomes_list = ["16K", "132", "RN4220", "MR1"]
    pool = Pool(processes=10) 
      
    TASKS = genomes_list
    genomes_list_sel = []
    
    for r in pool.imap(convertSequence, TASKS):
        if r != None and len(r) > 0:
            genomes_list_sel.append(r)
        progress.update(desc=r)     

    output_fh = open(cons_dir + "strains-sel.txt", "w")    
    for genome_id in genomes_list_sel:
        output_fh.write(genome_id + "\n")
    output_fh.close()  
    