import sys
from multiprocessing import Pool


sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import translateSequence
from soft.utils.camber_io_utils import ensure_dir
from soft.utils.camber_closure_utils import *
import os


cons_dir = "/home/misias/cons/"

def prepareDatabases(genome_id): 
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    genome_fn = cons_dir + "/sel_seq/seq-"+genome_id+".fasta"
    genome_fn = os.path.abspath(genome_fn)
    formatdb_exe = parameters["BLAST_FORMATDB_PATH"]
    
    os.system(formatdb_exe+" -p T -i "+genome_fn)
    return genome_id


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    WORKERS = int(parameters["WORKERS"])
    
    genomes_list = []
    input_fh = open(cons_dir + "strains-sel.txt")    
    for line in input_fh.readlines():
        genomes_list.append(line.strip())
    input_fh.close()
    
    TASKS = genomes_list
    
    progress = CAMBerProgress("Prepare databases")
    progress.setJobsCount(len(TASKS))
    pool = Pool(processes=WORKERS) 
      
    for r in pool.imap(prepareDatabases, TASKS):
        progress.update(desc=str(r))
    
    
    