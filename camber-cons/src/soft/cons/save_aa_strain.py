import sys
from multiprocessing import Pool


sys.path.append("../../../../camber2/src/")
import os
from threading import Thread
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *

def saveStrainQueries(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    fasta_dir = "/home/misias/cons/"
    
    fasta_output_fn = fasta_dir+"aa_"  + strain_id+".fasta"
    fasta_output_fh = open(fasta_output_fn, "w")
    
    readStrainSequences(strains,[strain_id], verb=False)
    readFinalAnnotationsIter(strain)
    annotation = strain.all_annotation
    
    multigenes = {}
    strain = strains.getGenome(strain_id)
    for gene in list(annotation.genes.values()):
        multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain, gene.fragment_id)
        if not multigene_id in multigenes:
            multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, target_id=gene.fragment_id)
            multigenes[multigene_id] = multigene
        else:
            multigene = multigenes[multigene_id]
        multigene.addGene(gene)
    
    multigenes_sorted = sorted(multigenes.values(), key=lambda multigene:multigene.mg_longest_full_id)
    
    for multigene in multigenes_sorted:
        dna_sequence = multigene.sequence()
        aa_sequence = translateSequence(dna_sequence)[:-1]
        if len(dna_sequence) > 3 and len(aa_sequence) > 10:
            ann_id = multigene.mg_longest_full_id.replace(" ", "_")
            ann_id = ann_id.replace(strain_id, "")
            ann_id = ann_id.strip("_")
            writeFASTAseq(fasta_output_fh, aa_sequence, ann_id)
        else:
            print("ERROR: ", strain_id, multigene.mg_full_id)
            print("ERROR: ", aa_sequence)
            print("ERROR: ", dna_sequence)
    fasta_output_fh.close()
    
    strain.sequence = None
    strain.sequences_map = {}
    return strain_id


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()

    WORKERS = int(parameters["WORKERS"])
    
    genomes_list = ["H37Rv"]
    n = len(genomes_list)
    
    progress = CAMBerProgress("Prepare queries")
    progress.setJobsCount(n)
    pool = Pool(processes=WORKERS)
     
    all_seq_mappings = {}
      
    for r in pool.imap(saveStrainQueries, list(genomes_list)):
        strain_id = r        
        progress.update(desc=strain_id)
        
        