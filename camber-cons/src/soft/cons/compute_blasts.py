import sys
import os
import threading
from multiprocessing import Pool



sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import ensure_dir, writeFASTAseq

cons_dir = "/home/misias/cons/"

def calcBlast(files_pair):
    query_id = files_pair[0]
    genome_id = files_pair[1]

    tr_evalue = float(1e-5)

    blast_exe = parameters["BLAST_PATH"]
    if platform.system().count("Windows") > 0 :
        blast_exe = os.path.abspath(blast_exe) 

    results_dir = cons_dir + "/blast_res_"+query_id+"/"
    ensure_dir(results_dir)
     
    query_fn = cons_dir + "/aa_"+query_id+".fasta"
    genome_fn = cons_dir + "/sel_seq/seq-"+genome_id+".fasta"
    
    src_output_fn = results_dir +"/tmp-"  + str(query_id)+"-"+str(genome_id) + ".txt"
    dst_output_fn = results_dir +"/blast-"  + str(query_id)+"-"+str(genome_id) + ".txt"

    if os.path.exists(dst_output_fn):
        return (query_id, genome_id)
    elif not os.path.exists(dst_output_fn) and os.path.exists(src_output_fn):
        os.rename(src_output_fn, dst_output_fn)
        return (query_id, genome_id)
    
    command = blast_exe + " -p blastp -e "+str(tr_evalue) + " ";
    command = command + " -d " + genome_fn + " -i " + query_fn + " -o " + src_output_fn + " -m 8\n";
    os.system(command)
    os.rename(src_output_fn, dst_output_fn)
    return (query_id, genome_id)


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    genomes_list = []
    input_fh = open(cons_dir + "strains-sel.txt")    
    for line in input_fh.readlines():
        genomes_list.append(line.strip())
    input_fh.close()
    
    WORKERS = int(parameters["WORKERS"])
    n = len(genomes_list)
    
    progress = CAMBerProgress("Compute BLASTSs")
    progress.setJobsCount(n)
    
    query_id = "H37Rv"
    
    TASKS = []
    for genome_id in genomes_list:
        TASKS.append((query_id, genome_id))
      
    pool = Pool(processes=WORKERS) 
    for r in pool.imap(calcBlast, TASKS):
        file1_id = r[0]
        file2_id = r[1]
        progress.update(desc=str(file1_id) + ":"+str(file2_id))

    print("")


    