import sys
import os
import threading
from multiprocessing import Pool


sys.path.append("../../../../camber2/src/")

from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import ensure_dir, writeFASTAseq

cons_dir = "/home/misias/cons/"
pid_thr = 50

def readSeqsMapping(input_fh):
    mapping = {}
    seq_id = ""
    seq = ""
    
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        if len(line) == 0:
            continue
        elif line.startswith(">"):
            if len(seq) > 0 and len(seq_id) > 0:
                mapping[seq_id] = seq
            seq_id = line[1:]
            seq = ""
        else:
            seq += line
    if len(seq) > 0 and len(seq_id) > 0:
        mapping[seq_id] = seq    
    return mapping

def parseBlasts(files_pair):
    query_id = files_pair[0]
    genome_id = files_pair[1]
    
    results_dir = cons_dir + "/blast_res_"+query_id+"/"
    
    blast_input_fn = results_dir + "/blast-" + str(query_id)+"-"+str(genome_id)+".txt"
    
    input_fh = open(cons_dir + "aa_"+query_id + ".fasta")
    seqsMapping1 = readSeqsMapping(input_fh)
    input_fh.close()
    
    input_fh = open(cons_dir + "sel_seq/seq-"+genome_id + ".fasta")
    seqsMapping2 = readSeqsMapping(input_fh)
    input_fh.close()
    
    blast_input_fh = open(blast_input_fn)
    rejected = 0
    acc_edges = set([])
    for line in blast_input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split()
        if len(tokens) < 12:
            continue
        
        seq1_id = tokens[0]
        seq2_id = tokens[1]
        
        try:
            seq1 = seqsMapping1[seq1_id]
            seq2 = seqsMapping2[seq2_id]
        except:
            print("Mapping error: ", genome_id)
            print("Mapping line: ", line)
        seq1_len = len(seq1)
        seq2_len = len(seq2)
                
        evalue = tokens[10]
        aln_len = int(tokens[3])
        identity_b = float(tokens[2])
        identities = int(round(identity_b*aln_len/100))
        mismatches = int(tokens[4])
        residues = identities+mismatches
        pid = float(identities)/float(max(seq1_len, seq2_len))
        id_p = float(pid_thr)/100.0
        if float(evalue) > float(1e-6):
            rejected += 1
            continue
        if not pid*100.0 >= hssp(residues, id_p*100.0 - 19.5):
            rejected += 1
            continue
        acc_edges.add((seq1_id, seq2_id, genome_id))
    blast_input_fh.close()
    
    return (genome_id, acc_edges)


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    query_id = "H37Rv"
    genomes_list = []
    
    input_fh = open(cons_dir + "strains-sel.txt")    
    for line in input_fh.readlines():
        line = line.strip()
        if len(line) > 3:
            genomes_list.append(line)
    input_fh.close()
    
    WORKERS = int(parameters["WORKERS"])
    print(WORKERS)
    
    n = len(genomes_list)
    
    progress = CAMBerProgress("Parse Blasts")
    progress.setJobsCount(n)
    pool = Pool(processes=WORKERS) 
      
    TASKS = []
    for genome_id in genomes_list:
        TASKS.append((query_id, genome_id))

    all_edges = set([])
    
    if WORKERS > 1:
        for r in pool.imap(parseBlasts, TASKS):
            genome_id = r[0]
            new_edges = r[1]
            all_edges.update(new_edges)
            progress.update(desc=str(genome_id) + " "+str(len(all_edges)))
    else:
        for TASK in TASKS:
            r = parseBlasts(TASK)
            genome_id = r[0]
            new_edges = r[1]
            all_edges.update(new_edges)
            progress.update(desc=str(genome_id) + " "+str(len(all_edges)))
        
    print("")
    
    graph_output_fn = cons_dir + "/mult_graph_"+query_id+"_"+str(pid_thr)+".txt"
    graph_output_fh = open(graph_output_fn, "w")
    for (node1_id, node2_id, genome_id) in all_edges:
        graph_output_fh.write(node1_id +"\t" + node2_id +"\t" + genome_id + "\n")
    graph_output_fh.close()
    
    print("")

