import sys
import os
from multiprocessing import Pool



sys.path.append("../../../../camber2/src/")
import threading
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import ensure_dir, writeFASTAseq

cons_dir = "/home/misias/cons/"
pid_thr = 50
query_id = "H37Rv"

def getGenomeClass(genome_id):
    tokens = genome_id.split("_")
    
    if len(tokens) == 1:
        return genome_id

    return tokens[0] + "_" + tokens[1]

def computeConservations(input_fh):
    mult_cons = {}
    classes = {}
    mult_cons_freq = {}
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 3:
            continue
        
        mult_id = tokens[0]
        genome_id = tokens[2]
        genome_class_id = getGenomeClass(genome_id)
        if not genome_class_id in classes:
            classes[genome_class_id] = set([])
        classes[genome_class_id].add(genome_id)
        
        if not mult_id in mult_cons:
            mult_cons[mult_id] = {}
        if not genome_class_id in mult_cons[mult_id]:
            mult_cons[mult_id][genome_class_id] = 0
        mult_cons[mult_id][genome_class_id] += 1
        
    for mult_id in mult_cons:
        mult_cons_freq[mult_id] = {}
        for class_id in mult_cons[mult_id]:
            mult_cons_freq[mult_id][class_id] = float(mult_cons[mult_id][class_id]) / float(len(classes[class_id]))
    return mult_cons_freq


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = cons_dir + "/mult_graph_"+query_id+"_"+str(pid_thr)+".txt"
    input_fh = open(input_fn)
    mult_cons = computeConservations(input_fh)
    input_fh.close()
    
    output_fn = cons_dir + "/conservation_"+query_id+"_"+str(pid_thr)+".txt"
    output_fh = open(output_fn, "w")
    for mult_id in mult_cons:
        mult_classes = mult_cons[mult_id]
        mult_classes_half = 0

        for class_id in mult_classes:
            p = mult_classes[class_id]
            if p > 0.5:
                mult_classes_half += 1   
                
        text_line = mult_id + "\t"
        text_line += str(len(mult_classes)) + "\t"
        text_line += str(mult_classes_half) + "\t"                
                     
        for class_id in mult_classes:
            p = mult_classes[class_id]

            text_line += class_id + ":" + str("%.2f" % p) + ";"
        output_fh.write(text_line + "\n")
    output_fh.close()
    