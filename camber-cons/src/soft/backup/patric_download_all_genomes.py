import time
import sys
from os.path import basename
from urllib.request import urlopen

sys.path.append("../../")
import os
import urllib


from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *



parameters = overwriteParameters(sys.argv)
parameters = readParameters()



ensure_dir(parameters["GENOMES"][:-1]+"_tmp")
ensure_dir(parameters["GENBANKS"][:-1]+"_tmp")
ensure_dir(parameters["FEATURES"][:-1]+"_tmp")

patric_db = "http://brcdownloads.vbi.vt.edu/patric2/genomes/"



f = urlopen(patric_db)
lines = f.readlines()
f.close()

sublines = []
for line in lines:
    str_line = str(line)
    if (str(line)).count("href") > 0:
        sublines.append(str_line)
        

print("NUMBER_OF_STRAINS: ", len(sublines))

strains_patric = set([])

for line in sublines:
    ind = line.find("=")+2
    ind2 = line.find(">")-2
    
    strain_id = line[ind:ind2]
    path_id = line[ind:ind2]
    print(path_id, strain_id)
    
    if strain_id != "":
        strains_patric.add(strain_id)
        seq_fn = patric_db + "/" + path_id + "/" + path_id+ ".fna"
        fea_fn = patric_db + "/" + path_id + "/" + path_id+ ".RefSeq.cds.tab"
        out_seq_fn = parameters["GENOMES"][:-1]+"_tmp"+"/seq-"+strain_id+".fasta"
        out_fea_fn = parameters["FEATURES"][:-1]+"_tmp"+"/cds-"+strain_id+".tab"
        if not os.path.exists(out_seq_fn):
            try:
                u = urlopen(seq_fn)
                localFile = open(out_seq_fn, 'wb')
                localFile.write(u.read())
                localFile.close()
            except:
                print("ERROR: " + out_seq_fn)               
        if not os.path.exists(out_fea_fn):
            try: 
                u = urlopen(fea_fn)
                localFile = open(out_fea_fn, 'wb')
                localFile.write(u.read())
                localFile.close()            
            except:
                print("ERROR: " + out_fea_fn)
         
for strain_id in strains_patric:
    print(strain_id, "-")

