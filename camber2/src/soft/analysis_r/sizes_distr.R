#install.packages(
require(graphics); require(grDevices);

#data_fn = paste(species_id, "_out/r_subset/", drug_name, ".txt", sep = "")
if (Sys.info()[['sysname']] == "Linux") {
	prefix = "/home/misias/camber/"
} else{
	prefix = "D:/Dropbox/Dropbox/camber/"
}

datamap = array();
datamap[["mtb"]]="mtu"
datamap[["ecoli"]]="eco"
datamap[["sta"]]="sau"

for (dataset in c("mtb", "sta", "ecoli")) {
	analysis = "/results/res-30-50-1e-10-YES-Q/analysis/"
	input_fn = "sizes_distr.txt"
	#output_fn = "sizes_distr.pdf"
	dataout = datamap[dataset]
	output_fn = paste(dataout, "_ref.pdf", sep="")

	input_path = paste(prefix, dataset, analysis, input_fn, sep="")
	if (file.exists(input_path)) {
		output_path = paste(prefix, output_fn, sep="")

		data = read.table(input_path, header=FALSE, sep="\t")
		data_mat = t(as.matrix(data[,c(2,3)]))

		pdf(output_path, width=18, height=10)
		par(mfrow=c(1,1),
			mar=c(7,7,1,0), 
			oma=c(0,0,0,0), 
			mgp=c(5,1,0))

		barplot(data_mat, col=c("blue", "red"),
			las=2, beside=TRUE, 
			cex.lab=2.5, cex.axis=2.2, 
			xlab="Number of strains", 
			ylab="Number of connected components")

		axis(1, at=3*1:nrow(data)-1, labels=data[,1],  cex.axis=2.5)

		legend("top", 
			c("Before the refinement procedure", 
			  "After the refinement procedure"
			 ), fill=c("blue", "red"), cex=3.0);
		dev.off()
	}
}
