import sys
import shutil
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

print("Creates input for CAMBerVis")
print("Input folder will be stored at: "+parameters["CAMBERVIS_INPUT_PATH"])


ensure_dir(parameters["CAMBERVIS_INPUT_PATH"])
ensure_dir(parameters["CAMBERVIS_INPUT_PATH"]+"/genomes")

progress = CAMBerProgress("Copying genomes:")
n = len(strains.allGenomes())
progress.setJobsCount(n)

for strain_id in strains.allGenomes():
    progress.update()
    src_fn = parameters["GENOMES"]+'seq-' + strain_id + ".fasta"
    dst_fn = parameters["CAMBERVIS_INPUT_PATH"]+'/genomes/seq-' + strain_id + ".fasta"
    shutil.copy(src_fn, dst_fn)

print("Copying unified annotations.")
src_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(0)+".txt"
dst_fn = parameters["CAMBERVIS_INPUT_PATH"]+"/clustering.txt"
shutil.copy(src_fn, dst_fn)

print("Copying the file with the list of strains.")
src_fn = parameters["STRAINS_INFO_FILE"]
dst_fn = parameters["CAMBERVIS_INPUT_PATH"]+"/strains.txt"
shutil.copy(src_fn, dst_fn)

print("The CAMBerVis input is already generated.")
print("Copy (and rename) the folder 'cambervis-input' into 'examples/' folder in your CAMBerVis installation.")

