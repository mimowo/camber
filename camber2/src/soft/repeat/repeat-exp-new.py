import sys
import os


sys.path.append("../../")

from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene
from soft.structs.multigene import *

#strain1_id = sys.argv[1]
if len(sys.argv) > 1:
    pid_tr = float(sys.argv[1])
else:
    pid_tr = 50.0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
diffTime()
#readStrainsSequences([strain1_id, strain2_id])
#readStrainsSequences(strains.allGenomes())
diffTime("sequences")
readExtAnnotations(strains.allGenomes())
#readExtAnnotations([strain1_id, strain2_id])
diffTime("annotations")

strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
print(strains_ordered)
if parameters["SPECIES"] == "mtb":
    all_strains = list(strains.allGenomes())
    all_strains.remove("KZN_V2475")
    strains_ordered = sorted(all_strains, key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))

pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
results_dir = pangenome_dir +"results/"


def extractGeneName(gene_id):
    tokens_a = gene_id.split(".")
    if len(tokens_a) == 5:
        return tokens_a[1]+"."+tokens_a[2]+"."+tokens_a[3]
    elif len(tokens_a) == 4:
        return tokens_a[0]+"."+tokens_a[1]+"."+tokens_a[2]
    elif len(tokens_a) == 6:
        return tokens_a[2]+"."+tokens_a[3]+"."+tokens_a[4]

    else:
        return ""
        
def commonGenes(strain1_id, strain2_id, exp_type):
    global pid_tr 
    ext_annotation1 = strains.getGenome(strain1_id).ext_annotation
    ext_annotation2 = strains.getGenome(strain2_id).ext_annotation
    
    annotation1 = strains.getGenome(strain1_id).annotation
    annotation2 = strains.getGenome(strain2_id).annotation
    
    core_genes = set([])
    input_fh = open(results_dir+"blast-"+strain1_id+"-"+strain2_id+".txt")
    lines = input_fh.readlines()
    input_fh.close()
    counter = 0
    
    for line in lines:
        counter += 1
        tokens = line.split()
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        #gene1_id_a = tokens[0]
        #gene2_id_a = tokens[1]
        debug = False
        
        gene1_id_a = extractGeneName(gene1_id)
        gene2_id_a = extractGeneName(gene2_id)
        #mg1_id_a = extractMultiGeneName(gene1_id)
        #mg2_id_a = extractMultiGeneName(gene2_id)
        #print gene1_id, gene1_id_a
        if not gene1_id_a in ext_annotation1.genes:
            continue
        if not gene2_id_a in ext_annotation2.genes:
            continue;
        if debug:
            print("ok")
        if exp_type == "org":
            if not gene2_id_a in annotation2.genes:
                continue        
        pid = float(tokens[2])
        aln_len = int(tokens[3])
        if pid < pid_tr:
            continue
        gene1 = ext_annotation1.genes[gene1_id_a]
        gene2 = ext_annotation2.genes[gene2_id_a]
        cover = int(pid*float(aln_len)/100)
   
        if cover*3 >= 0.5*max(gene1.length(), gene2.length()):
            core_genes.add(gene1)
    return core_genes

def multigeneIDs(genes):
    mg_ids = set([])
    for gene in genes:
        mult_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        mg_ids.add(mult_id)
    return mg_ids

def corePanGenomeSizes(exp_type):
    pan_genomes, core_genomes = [], []
    for i in range(0, len(strains_ordered), 1):
        strain_id = strains_ordered[i]
        if i == 0:
            strain_first_id = strains_ordered[i]
            if exp_type == "org":
                core_mgenes = multigeneIDs(list(strains.getGenome(strain_first_id).annotation.genes.values()))
                pan_mgenes = multigeneIDs(list(strains.getGenome(strain_first_id).annotation.genes.values()))
            else:
                core_mgenes = multigeneIDs(list(strains.getGenome(strain_first_id).ext_annotation.genes.values()))
                pan_mgenes = multigeneIDs(list(strains.getGenome(strain_first_id).ext_annotation.genes.values()))
            pan_mgenes_size = len(pan_mgenes)
        else:
            strain_new_id = strains_ordered[i]
            if exp_type == "org":
                new_mgenes = multigeneIDs(list(strains.getGenome(strain_new_id).annotation.genes.values()))
            else:
                new_mgenes = multigeneIDs(list(strains.getGenome(strain_new_id).ext_annotation.genes.values()))
                
            ann_new_mgenes = multigeneIDs(list(strains.getGenome(strain_new_id).annotation.genes.values()))
            comm_genes = commonGenes(strain_first_id, strain_new_id, exp_type)
            comm_mgenes_ids = multigeneIDs(comm_genes)
            
            core_mgenes_tmp = set(core_mgenes)
            for mgene_id in core_mgenes:
                if not mgene_id in comm_mgenes_ids:
                    core_mgenes_tmp.remove(mgene_id)
            core_mgenes = set(core_mgenes_tmp)
            
            for j in range(0, i, 1):
                strain_curr_id = strains_ordered[j]
                comm_curr_genes = commonGenes(strain_new_id, strain_curr_id, exp_type)
                
                for gene in comm_curr_genes:
                    mgene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
                    if mgene_id in new_mgenes:
                        new_mgenes.remove(mgene_id)

            pan_mgenes.update(new_mgenes)     
            pan_mgenes_size += len(new_mgenes)
        pan_genomes.append(len(pan_mgenes))
        core_genomes.append(len(core_mgenes))

    return pan_genomes, core_genomes

pan_genomes_org, core_genomes_org = corePanGenomeSizes("org")
pan_genomes_new, core_genomes_new = corePanGenomeSizes("new")

tls = []
for i in range(0, len(strains_ordered), 1):
    strain_id = strains_ordered[i]
    tl = str(i) + "\t" + strain_id
    tl+= str(pan_genomes_org[i]) + "\t"
    tl+= str(core_genomes_org[i]) + "\t"
    tl+= str(pan_genomes_new[i]) + "\t"
    tl+= str(core_genomes_new[i]) + "\n"
    tls.append(tl)

output_fh = open(parameters["ANALYSIS_RESULTS"]+"/pan_core_genome.txt", "w")
for tl in tls:
    output_fh.write(tl)
output_fh.close()


