import sys
import os
import multiprocessing
   
sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene

def runBlast(params):
    strain1_id, strain2_id = params

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()

    pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
    fasta_dir = pangenome_dir +"fasta/"
    info_dir = pangenome_dir +"info/"
    results_dir = pangenome_dir +"results/"

    tr_evalue = float(1e-1)

    blast_exe = parameters["BLAST_PATH"]
    if platform.system().count("Windows") > 0 :
        blast_exe = os.path.abspath(blast_exe) 

    query_fn = fasta_dir+"all-"  + strain1_id + ".fasta"
    genome_fn = fasta_dir+"all-"  + strain2_id + ".fasta"
    src_output_fn = results_dir + "tmp-"  + strain1_id+"-"+strain2_id + ".txt"
    dst_output_fn = results_dir + "blast-"  + strain1_id+"-"+strain2_id + ".txt"

    if not os.path.exists(dst_output_fn):
        command = blast_exe + " -p blastp -e "+str(tr_evalue)+" -F F ";
        command = command + " -d " + genome_fn + " -i " + query_fn + " -o " + src_output_fn + " -m 8";
        os.system(command)
        os.rename(src_output_fn, dst_output_fn)

    return (strain1_id, strain2_id)

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    #strains = ["042", "S88"]
    print(parameters["BLAST_PATH"])

    pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
    fasta_dir = pangenome_dir +"fasta/"
    info_dir = pangenome_dir +"info/"
    results_dir = pangenome_dir +"results/"
    ensure_dir(pangenome_dir)
    ensure_dir(fasta_dir)
    ensure_dir(info_dir)
    ensure_dir(results_dir)
    tr_evalue = float(1e-1)

    progress = CAMBerProgress("Blastp:")
    WORKERS = int(parameters["WORKERS"])

    TASKS = []
    for strain1_id in strains.allGenomes(): 
        for strain2_id in strains.allGenomes(): 
            if strain1_id == strain2_id:
                continue
            TASKS.append((strain1_id, strain2_id))

    progress.setJobsCount(len(TASKS))

    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(runBlast, TASKS):
            progress.update()
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            runBlast(TASK)
            progress.update()
