import sys
import os
dir_path = os.path.abspath(sys.argv[0])
folder_name = "camber-svn"
camber_dir = dir_path[0:dir_path.find(folder_name)]+folder_name+"/"
camber_src_dir = camber_dir+"src/"
sys.path.append(camber_src_dir)

print(camber_src_dir)
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

ensure_dir("/research/wongls-group/wozniak/scripts2")
ensure_dir(parameters["CLOSURE_RESULTS_BLAST"])

ensure_dir(parameters["CLOSURE_RESULTS_BLAST_PARSED"])

species = parameters["SPECIES"]
prefix = parameters["EXP_PREFIX"]
hssp = parameters["HSSP"]
params = ["HSSP","EXP_PREFIX", "SPECIES", "PID_TYPE"]

for strain_id in strains.allGenomes():
    script_fn = "/research/wongls-group/wozniak/scripts2/b"+species+"-"+hssp+"-"+strain_id+".sh"
    script_fh = open(script_fn, "w")

    command = "python " + "/research/wongls-group/wozniak/camber-svn/src/soft/repeat/do-blasts-self.py " 
    command += strain_id + createCommandParams(params, parameters)+"\n"
    script_fh.write(command)
            
    script_fh.close()
    os.system("qsub "+script_fn);

print("BLAST_END")
