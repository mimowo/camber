import sys
sys.path.append("/research/wongls-group/wozniak/camber-svn/src/")
import os
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene

script_nr = 0

strain_id = sys.argv[1]

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
#strains = ["042", "S88"]
print(parameters["BLAST_PATH"])


pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
results_dir = pangenome_dir +"results/"
scripts_dir = "/research/wongls-group/wozniak/scripts4/"
ensure_dir(pangenome_dir)
ensure_dir(fasta_dir)
ensure_dir(info_dir)
ensure_dir(results_dir)
ensure_dir(scripts_dir)
tr_evalue = float(1e-1)

blast_exe = parameters["BLAST_PATH"]
if platform.system().count("Windows") > 0 :
    blast_exe = os.path.abspath(blast_exe) 

def alignSelf(strain_id):
    query_fn = fasta_dir+"all-"  + strain_id + ".fasta"
    
    genome_fn = fasta_dir+"all-"  + strain_id + ".fasta"
    src_output_fn = results_dir + "tmp-"  + strain_id+"-"+strain_id + ".fasta"
    dst_output_fn = results_dir + "blast-"  + strain_id+"-"+strain_id + ".fasta"
    if not os.path.exists(dst_output_fn):
        
        command = blast_exe + " -p blastp -e "+str(tr_evalue)+" -F F ";
        command = command + " -d " + genome_fn + " -i " + query_fn + " -o " + src_output_fn + " -m 8";
    
        os.system(command)
        os.rename(src_output_fn, dst_output_fn)


alignSelf(strain_id)
