import sys
#sys.path.append("/home/w/wozniak/camber-svn/src/")
sys.path.append("/research/wongls-group/wozniak/camber-svn/src/")
import os
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene

#strain1_id = sys.argv[1]
#strain2_id = sys.argv[2]

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
diffTime()
#readStrainsSequences([strain1_id, strain2_id])
readStrainsSequences(strains.allGenomes())
diffTime("sequences")
readExtAnnotations(strains.allGenomes())
#readExtAnnotations([strain1_id, strain2_id])

pangenome_dir = parameters["CAMBER_RESOURCE_PATH"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
results_dir = pangenome_dir +"results/"

def extractGeneName(gene_id):
    tokens_a = gene_id.split(".")
    if len(tokens_a) == 5:
        return tokens_a[1]+"."+tokens_a[2]+"."+tokens_a[3]
    elif len(tokens_a) == 6:
        return tokens_a[2]+"."+tokens_a[3]+"."+tokens_a[4]
    else:
        return ""
    
def extractMultiGeneName(gene_id):
    tokens_a = gene_id.split(".")
    if len(tokens_a) == 5:
        return tokens_a[2]+"."+tokens_a[3]
    elif len(tokens_a) == 6:
        return tokens_a[3]+"."+tokens_a[4]
    else:
        return ""    
    
def commonGenes(strain1_id, strain2_id):
    annotation1 = strains.getGenome(strain1_id).ext_annotation
    annotation2 = strains.getGenome(strain2_id).ext_annotation
    
    f_annotation1 = strains.getGenome(strain1_id).annotation
    f_annotation2 = strains.getGenome(strain2_id).annotation
    
    core_genes = set([])
    input_fh = open(results_dir+"blast-"+strain1_id+"-"+strain2_id+".fasta")
    lines = input_fh.readlines()
    input_fh.close()
    counter = 0
    
    for line in lines:
        counter += 1
      #  if counter % 10000 == 0:
      #      print counter#, core_genes
        tokens = line.split()
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        #print line
        
        gene1_id_a = extractGeneName(gene1_id)
        gene2_id_a = extractGeneName(gene2_id)
        mg1_id_a = extractMultiGeneName(gene1_id)
        mg2_id_a = extractMultiGeneName(gene2_id)
        #print gene1_id, gene1_id_a
       # if not gene1_id_a in f_annotation1.genes:
      #      continue
    #    if not gene2_id_a in f_annotation2.genes:
    #        continue        
        pid = float(tokens[2])
        aln_len = int(tokens[3])
        #print line
        #print pid, aln_len
        if not gene1_id in core_genes:
            if pid < 50.0:
                continue
            gene1 = annotation1.genes[gene1_id_a]
            gene2 = annotation2.genes[gene2_id_a]
            if aln_len*3 >= 0.5*max(gene1.length(), gene2.length()):
               # core_genes.add(gene1_id_a)
                core_genes.add(mg1_id_a)
    return core_genes

core_genes = None
strain1_id = "W"
for strain2_id in strains.allGenomes():
    if strain1_id != strain2_id:
        comm_genes = commonGenes(strain1_id, strain2_id)
        if core_genes == None:
            core_genes = set(comm_genes)
        else:
            core_genes_tmp = set(core_genes)
            for gene_id in core_genes:
                if not gene_id in  comm_genes:
                    core_genes_tmp.remove(gene_id)
            core_genes = set(core_genes_tmp)
        print(len(core_genes), strain2_id)
                
            
