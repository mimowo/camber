import sys
sys.path.append("/research/wongls-group/wozniak/camber-svn/src/")
import os
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene

strain1_id = sys.argv[1]
strain2_id = sys.argv[2]

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
diffTime()
readStrainsSequences([strain1_id, strain2_id])
#readStrainSequences(strains.allGenomes())
diffTime("sequences")
readExtAnnotations([strain1_id, strain2_id])

pangenome_dir = parameters["CAMBER_RESOURCE_PATH"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
results_dir = pangenome_dir +"results/"

annotation1 = strains.getGenome(strain1_id).ext_annotation
annotation2 = strains.getGenome(strain2_id).ext_annotation

def commonGenes(strain1_id, strain2_id):
    core_genes = set([])
    input_fh = open(results_dir+"blast-"+strain1_id+"-"+strain2_id+".fasta")
    lines = input_fh.readlines()
    input_fh.close()
    counter = 0
    
    for line in lines:
        counter += 1
        if counter % 10000 == 0:
            print(counter)#, core_genes
        tokens = line.split()
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        #print line
        tokens1_a = gene1_id.split(".")
        tokens2_a = gene2_id.split(".")
        gene1_id_a = tokens1_a[1]+"."+tokens1_a[2]+"."+tokens1_a[3]
        gene2_id_a = tokens2_a[1]+"."+tokens2_a[2]+"."+tokens2_a[3]
        pid = float(tokens[2])
        aln_len = int(tokens[3])
        #print line
        #print pid, aln_len
        if not gene1_id in core_genes:
            if pid < 50.0:
                continue
            gene1 = annotation1.genes[gene1_id_a]
            gene2 = annotation2.genes[gene2_id_a]
            if aln_len*3 >= 0.5*min(gene1.length(), gene2.length()):
                core_genes.add(gene1_id)
    return core_genes

comm_genes = commonGenes(strain1_id, strain2_id)
print(len(comm_genes))
#print comm_genes
