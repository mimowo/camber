import sys
sys.path.append("/research/wongls-group/wozniak/camber-svn/src/")
import os
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene

script_nr = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
#strains = ["042", "S88"]
print(parameters["BLAST_PATH"])
readStrainsSequences(strains.allGenomes())
diffTime("sequences")
readExtAnnotations(strains.allGenomes())
diffTime("annotations")

pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
ensure_dir(pangenome_dir)
ensure_dir(fasta_dir)
ensure_dir(info_dir)

def countQueries(strain):
    all_output_fn = info_dir+"all-"  + strain+".txt"
    all_output_fh = open(all_output_fn, "w");
    fasta_input_fn = fasta_dir+"all-"  + strain+".fasta"
    fasta_input_fh = open(fasta_input_fn, "r")
    annotation = strains.getGenome(strain).ext_annotation
    
    counter = 0
    lines = fasta_input_fh.readlines()
    for line in lines:
        if len(line)==0:
            continue
        if line[0]==">":
            counter += 1;
    
    print(strain, counter, len(annotation.genes))
    fasta_input_fh.close()
    all_output_fh.close()


for strain in strains.allGenomes():
    countQueries(strain)
    