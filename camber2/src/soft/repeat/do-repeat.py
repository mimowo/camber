import sys
import os
import time

sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

params = ["HSSP", "SPECIES", "EXP_PREFIX", "WORKERS"]

python_exe = sys.executable

files_command = python_exe + " prepare-files-all.py " + createCommandParams(params, parameters)
db_command = python_exe + " prepare-databases-all.py " + createCommandParams(params, parameters)
blast_command = python_exe + " do-blasts-all.py " + createCommandParams(params, parameters)
repeat1_command = python_exe + " repeat-exp-new.py 50 org " + createCommandParams(params, parameters)
repeat2_command = python_exe + " repeat-exp-new.py 50 new " + createCommandParams(params, parameters)

s_time = time.clock()

os.system(files_command)
os.system(db_command)
os.system(blast_command)
os.system(repeat1_command)
os.system(repeat2_command)

t_time = time.clock()

print("TIME", t_time-s_time)

