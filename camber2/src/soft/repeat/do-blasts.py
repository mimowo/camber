import sys
sys.path.append("/research/wongls-group/wozniak/camber-svn/src/")
import os
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene

script_nr = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
#strains = ["042", "S88"]
print(parameters["BLAST_PATH"])


pangenome_dir = parameters["CAMBER_RESOURCE_PATH"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
results_dir = pangenome_dir +"results/"
scripts_dir = "/research/wongls-group/wozniak/scripts4/"
ensure_dir(pangenome_dir)
ensure_dir(fasta_dir)
ensure_dir(info_dir)
ensure_dir(results_dir)
ensure_dir(scripts_dir)
tr_evalue = float(1e-5)

blast_exe = parameters["BLAST_PATH"]
if platform.system().count("Windows") > 0 :
    blast_exe = os.path.abspath(blast_exe) 

for strain1_id in ["W"]:
    script_fn = scripts_dir+"rep"+strain1_id+".sh"
    script_fh = open(script_fn, "w")
    query_fn = fasta_dir+"all-"  + strain1_id + ".fasta"
    
    for strain2_id in strains.allGenomes():
        if strain1_id == strain2_id:
            continue;
    
        genome_fn = fasta_dir+"all-"  + strain2_id + ".fasta"
        src_output_fn = results_dir + "tmp-"  + strain1_id+"-"+strain2_id + ".fasta"
        dst_output_fn = results_dir + "blast-"  + strain1_id+"-"+strain2_id + ".fasta"
        #print query_file_name
        if not os.path.exists(dst_output_fn):
            command = blast_exe + " -p blastp -e "+str(tr_evalue)+" -F F ";
            command = command + " -d " + genome_fn + " -i " + query_fn + " -o " + src_output_fn + " -m 8\n";
            command = command + "mv "+src_output_fn+" "+dst_output_fn +"\n"
            script_fh.write(command)
            
    script_fh.close()
    os.system("qsub "+script_fn);    
            
