import sys
import os

sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene

script_nr = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
#strains = ["042", "S88"]
print(parameters["BLAST_PATH"])

pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
ensure_dir(pangenome_dir)
ensure_dir(fasta_dir)
ensure_dir(info_dir)

def saveQueries(strain):
    readStrainsSequences([strain], verbose=False)
   # diffTime("sequences")
    readExtAnnotations([strain], verbose=False)
   # diffTime("annotations")

    all_output_fn = info_dir+"all-"  + strain+".txt"
    all_output_fh = open(all_output_fn, "w");
    fasta_output_fn = fasta_dir+"all-"  + strain+".fasta"
    fasta_output_fh = open(fasta_output_fn, "w")
    annotation = strains.getGenome(strain).ext_annotation
    sorted_list = sorted(list(annotation.genes.values()), key=lambda gene:gene.left_bound)
    for gene in sorted_list:
        output_text = gene.toString()
        all_output_fh.write(output_text);
        dna_sequence = gene.sequence()
        if len(dna_sequence) < 3:
            continue
            print(gene.strain_unique_id, dna_sequence)
        writeGene(fasta_output_fh, translateSequence(dna_sequence)[:-1], gene.strain_unique_id)
    fasta_output_fh.close()
    all_output_fh.close()


for strain in strains.allGenomes():
    saveQueries(strain)
    