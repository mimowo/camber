import sys
sys.path.append("/research/wongls-group/wozniak/camber-svn/src/")
import os
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene
from soft.structs.multigene import *

#strain1_id = sys.argv[1]
pid_tr = float(sys.argv[1])
exp_type = sys.argv[2]

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
diffTime()
#readStrainsSequences([strain1_id, strain2_id])
readStrainsSequences(strains.allGenomes())
diffTime("sequences")
readExtAnnotations(strains.allGenomes())
#readExtAnnotations([strain1_id, strain2_id])
diffTime("annotations")

pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
results_dir = pangenome_dir +"results/"


def extractGeneName(gene_id):
    tokens_a = gene_id.split(".")
    if len(tokens_a) == 5:
        return tokens_a[1]+"."+tokens_a[2]+"."+tokens_a[3]
    elif len(tokens_a) == 4:
        return tokens_a[0]+"."+tokens_a[1]+"."+tokens_a[2]
    elif len(tokens_a) == 6:
        return tokens_a[2]+"."+tokens_a[3]+"."+tokens_a[4]

    else:
        return ""
    
def joinParalogs(strain_id, genes):
    global pid_tr, exp_type

    cluster_genes = set([])
    input_fh = open(results_dir+"blast-"+strain_id+"-"+strain_id+".fasta")
    lines = input_fh.readlines()
    input_fh.close()
    genes_map = {}
    
    for line in lines:
        tokens = line.split()
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        
        gene1_id_a = extractGeneName(gene1_id)
        gene2_id_a = extractGeneName(gene2_id)

        genes_map[(gene1_id_a, gene2_id_a)] = "N"

        if not gene1_id_a in genes:
            continue
        if not gene2_id_a in genes:
            continue;     
        pid = float(tokens[2])
        aln_len = int(tokens[3])
        if pid < pid_tr:
            continue
        gene1 = genes[gene1_id_a]
        gene2 = genes[gene2_id_a]
        cover = int(pid*float(aln_len)/100)
   
        if cover*3 >= 0.5*max(gene1.length(), gene2.length()):
            genes_map[(gene1_id_a, gene2_id_a)] = "T"
            
    genes_sorted = sorted(list(genes.values()), key=lambda gene:gene.left_bound)
    for gene1 in genes_sorted:
        add_new = True
        for gene_clust in cluster_genes:
            if (gene1.unique_id, gene_clust.unique_id) in genes_map:
                if genes_map[(gene1.unique_id, gene_clust.unique_id)] == "T":
                    add_new = False
        if add_new:
            cluster_genes.add(gene1)
    return cluster_genes        
    
def commonGenes(strain1_id, strain2_id):
    global pid_tr, exp_type
    ext_annotation1 = strains.getGenome(strain1_id).ext_annotation
    ext_annotation2 = strains.getGenome(strain2_id).ext_annotation
    
    annotation1 = strains.getGenome(strain1_id).annotation
    annotation2 = strains.getGenome(strain2_id).annotation
    
    core_genes = set([])
    input_fh = open(results_dir+"blast-"+strain1_id+"-"+strain2_id+".fasta")
    lines = input_fh.readlines()
    input_fh.close()
    counter = 0
    
    for line in lines:
        counter += 1
        tokens = line.split()
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        #gene1_id_a = tokens[0]
        #gene2_id_a = tokens[1]
        debug = False
        
        gene1_id_a = extractGeneName(gene1_id)
        gene2_id_a = extractGeneName(gene2_id)
        #mg1_id_a = extractMultiGeneName(gene1_id)
        #mg2_id_a = extractMultiGeneName(gene2_id)
        #print gene1_id, gene1_id_a
        if not gene1_id_a in ext_annotation1.genes:
            continue
        if not gene2_id_a in ext_annotation2.genes:
            continue;
        if debug:
            print("ok")
        if exp_type == "org":
            if not gene2_id_a in annotation2.genes:
                continue        
        pid = float(tokens[2])
        aln_len = int(tokens[3])
        if pid < pid_tr:
            continue
        gene1 = ext_annotation1.genes[gene1_id_a]
        gene2 = ext_annotation2.genes[gene2_id_a]
        cover = int(pid*float(aln_len)/100)
   
        if cover*3 >= 0.5*max(gene1.length(), gene2.length()):
            core_genes.add(gene1)
    return core_genes

    

strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
print(strains_ordered)
if parameters["SPECIES"] == "mtb":
    all_strains = list(strains.allGenomes())
    all_strains.remove("KZN_V2475")
    strains_ordered = sorted(all_strains, key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))

def multigeneIDs(genes):
    mg_ids = set([])
    for gene in genes:
        mult_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        mg_ids.add(mult_id)
    return mg_ids

for i in range(0, len(strains_ordered), 1):
    strain_id = strains_ordered[i]
    if i == 0:
        strain_first_id = strains_ordered[i]
        if exp_type == "org":
            cluster_mgenes = joinParalogs(strain_first_id, strains.getGenome(strain_first_id).annotation.genes)
            core_mgenes = multigeneIDs(cluster_mgenes)
            pan_mgenes = multigeneIDs(cluster_mgenes)
            #core_mgenes = multigeneIDs(strains.getGenome(strain_first_id).annotation.genes.values())
            #pan_mgenes = multigeneIDs(strains.getGenome(strain_first_id).annotation.genes.values())
        else:
            cluster_mgenes = joinParalogs(strain_first_id, strains.getGenome(strain_first_id).ext_annotation.genes)
            core_mgenes = multigeneIDs(cluster_mgenes)
            pan_mgenes = multigeneIDs(cluster_mgenes)            
            #core_mgenes = multigeneIDs(strains.getGenome(strain_first_id).ext_annotation.genes.values())
            #pan_mgenes = multigeneIDs(strains.getGenome(strain_first_id).ext_annotation.genes.values())
        pan_mgenes_size = len(pan_mgenes)
    else:
        strain_new_id = strains_ordered[i]
        if exp_type == "org":
            new_mgenes = multigeneIDs(list(strains.getGenome(strain_new_id).annotation.genes.values()))
        else:
            new_mgenes = multigeneIDs(list(strains.getGenome(strain_new_id).ext_annotation.genes.values()))
            
        ann_new_mgenes = multigeneIDs(list(strains.getGenome(strain_new_id).annotation.genes.values()))
        comm_genes = commonGenes(strain_first_id, strain_new_id)
        comm_mgenes_ids = multigeneIDs(comm_genes)
        
        core_mgenes_tmp = set(core_mgenes)
        for mgene_id in core_mgenes:
            if not mgene_id in comm_mgenes_ids:
                core_mgenes_tmp.remove(mgene_id)
        core_mgenes = set(core_mgenes_tmp)
        
        for j in range(0, i, 1):
            strain_curr_id = strains_ordered[j]
            comm_curr_genes = commonGenes(strain_new_id, strain_curr_id)
            
            for gene in comm_curr_genes:
                mgene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
                if mgene_id in new_mgenes:
                    new_mgenes.remove(mgene_id)

        pan_mgenes.update(new_mgenes)     
        pan_mgenes_size += len(new_mgenes)
    print(i, strain_id, len(pan_mgenes), len(core_mgenes))

