import sys
import os
sys.path.append("../../")
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

ensure_dir("/research/wongls-group/wozniak/scripts2")
ensure_dir(parameters["CLOSURE_RESULTS_BLAST"])

ensure_dir(parameters["CLOSURE_RESULTS_BLAST_PARSED"])

species = parameters["SPECIES"]
prefix = parameters["EXP_PREFIX"]
hssp = parameters["HSSP"]
params = ["HSSP","EXP_PREFIX", "SPECIES", "PID_TYPE"]

for strain1_id in strains.allGenomes():
    script_fn = "/research/wongls-group/wozniak/scripts2/b"+species+"-"+hssp+"-"+strain1_id+".sh"
    script_fh = open(script_fn, "w")

    command = "python " + "/research/wongls-group/wozniak/camber2/src/soft/repeat/do-blasts-all-gse.py " 
    command += strain1_id + createCommandParams(params, parameters)+"\n"
    script_fh.write(command)
            
    script_fh.close()
    os.system("qsub "+script_fn);

print("BLAST_END")
