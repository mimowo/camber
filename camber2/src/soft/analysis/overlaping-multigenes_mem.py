import sys
import time
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
readExtAnnotations(strains.allGenomes())

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(0)+".txt"
conn_comp_fh = open(conn_comp_fn)

old_mg_cc = {}
old_cc_mg = {}

lines = conn_comp_fh.readlines()
for line in lines:
    tokens = line.split()
    
    cc_id = tokens[0]
    mg_id = tokens[1]
    mg_tokens = mg_id.split(".")
    strain = mg_tokens[2]
    old_mg_cc[mg_id] = cc_id
    if not cc_id in old_cc_mg:
        old_cc_mg[cc_id] = {}
    if not strain in old_cc_mg[cc_id]:
        old_cc_mg[cc_id][strain] = 0
    old_cc_mg[cc_id][strain] += 1
conn_comp_fh.close();


def computeOverlap(x, y):
    x1 = x[0]
    x2 = x[1]
    y1 = y[0]
    y2 = y[1]
    return min(x2,y2)-max(x1,y1)+1

def shorterLength(x,y):
    x1 = x[0]
    x2 = x[1]
    y1 = y[0]
    y2 = y[1]
    return min(x2-x1, y2-y1)

def longerLength(x,y):
    x1 = x[0]
    x2 = x[1]
    y1 = y[0]
    y2 = y[1]
    return max(x2-x1, y2-y1)
    
def totalCoverage(x,y):
    x1 = x[0]
    x2 = x[1]
    y1 = y[0]
    y2 = y[1]
    return (min(x1,y1), max(x2,y2))
    
strains_number = strains.strainsCount()
strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    multigenes = {}
    strain = strains.getGenome(strain_id)

    for gene in list(strain.ext_annotation.genes.values()):
        multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        if not multigene_id in multigenes:
            multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, ann_id=gene.gene_id)
            multigenes[multigene_id] = multigene
        else:
            multigene = multigenes[multigene_id]
        multigene.addGene(gene)
    
    mg_list = sorted(list(multigenes.values()), key=lambda mg: mg.left_bound)
    current = (mg_list[0].left_bound, mg_list[0].right_bound)
    
    
    overlaped_pairs = 0
    ann_both = 0
    ann_one = 0
    ann_none = 0
    
    for i in range(1, len(mg_list), 1):
        for j in range(1, 3, 1):
            if i-j<0:
                continue
            next = (mg_list[i].left_bound, mg_list[i].right_bound)
            current = (mg_list[i-j].left_bound, mg_list[i-j].right_bound)
            
            cc_id1 = old_mg_cc[mg_list[i].mg_strain_unique_id]
            cc_id2 = old_mg_cc[mg_list[i-j].mg_strain_unique_id]
            
            non_anchor = False
            if len(old_cc_mg[cc_id1]) < strains_number:
                non_anchor = True
            if len(old_cc_mg[cc_id2]) < strains_number:
                non_anchor = True
            for strain_tmp_id in old_cc_mg[cc_id1]:
                if old_cc_mg[cc_id1][strain_tmp_id] > 1:
                    non_anchor = True
            for strain_tmp_id in old_cc_mg[cc_id2]:
                if old_cc_mg[cc_id2][strain_tmp_id] > 1:
                    non_anchor = True
            if non_anchor:
                continue
            overlap = computeOverlap(current, next)
            shorter_len = shorterLength(current, next)
            longer_len = longerLength(current, next)
            if overlap >= 0.5 * shorter_len:
                current = totalCoverage(current, next)
                overlaped_pairs += 1
                if(mg_list[i].ann_id != "x" and mg_list[i-1].ann_id != "x"):
                    ann_both += 1
                elif (mg_list[i].ann_id != "x" and mg_list[i-1].ann_id == "x") or (mg_list[i].ann_id == "x" and mg_list[i-1].ann_id != "x"):
                    ann_one += 1
                else:
                    ann_none += 1
       # else:
       #     current = next
    print(strain_id+" & " +str(overlaped_pairs) +" & " + str(ann_both)+" & " +str(ann_one)+" & " +str(ann_none) +"\\\\ ")
    



