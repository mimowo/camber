import sys
import time
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *

phase = sys.argv[1]

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
readExtAnnotations(strains.allGenomes())

lens_all_fh = open(parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"/lens-all-"+parameters["SPECIES"]+".txt", "w")

strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    
    multigenes = {}
    strain = strains.getGenome(strain_id)

    for gene in list(strain.ext_annotation.genes.values()):
        multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        if not multigene_id in multigenes:
            multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, ann_id=gene.gene_id)
            multigenes[multigene_id] = multigene
        else:
            multigene = multigenes[multigene_id]
        multigene.addGene(gene)

    for mg in list(multigenes.values()):
        if mg.ann_id == "x":
            lens_all_fh.write("N"+"\t"+mg.mg_strain_unique_id +"\t"+str(mg.length())+"\n")
        else:
            lens_all_fh.write("A"+"\t"+mg.mg_strain_unique_id +"\t"+str(mg.length())+"\n")
    for gene in list(strain.annotation.genes.values()):
        lens_all_fh.write("G"+"\t"+gene.gene_id +"\t"+str(gene.length())+"\n")

lens_all_fh.close()    
    