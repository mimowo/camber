import sys
import time
import os
sys.path.append("../../")
from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

if len(sys.argv) > 1:
    phase = int(sys.argv[1])
else:
    phase = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())

conn_comps = {}
conn_comp_sizes = {}
sizes_distr = {}

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

while 1:
    line = conn_comp_fh.readline()
    if not line:
        break
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    ann_id = tokens[2]
    mg_tokens = mg_id.split(".")
    genome_id = mg_tokens[2]
    strain_id = strains.getGenome(genome_id).strain_id
    if genome_id != strain_id:
        print(genome_id, strain_id)
    if not cc_id in conn_comps:
        conn_comps[cc_id] = {}
        conn_comp_sizes[cc_id] = 0
    cc = conn_comps[cc_id]
    if not strain_id in cc:
        cc[strain_id] = set([])
    cc[strain_id].add(ann_id+":"+mg_id)
    conn_comp_sizes[cc_id] += 1
conn_comp_fh.close();


strains_count = strains.strainsCount()
anchors = 0
non_anchors = 0
core_anchors = 0
core_cc = 0
orphans = 0

for cc_id in conn_comps:
    cc = conn_comps[cc_id]
    anchor = True
    core = True
    if len(cc) < strains_count:
        core = False
    if len(cc) == 1:
        orphans += 1
    for strain_id in cc:
        if len(cc[strain_id]) > 1:
            anchor = False
    if core:
        core_cc += 1
    if anchor:
        anchors += 1
        if core:
            core_anchors += 1
    else:
        non_anchors += 1        

print("# of connected components:", len(conn_comps))
print("# of anchors:", anchors)
print("# of non-anchors:", non_anchors)
print("# of core anchors:", core_anchors)
print("# of core connected components:", core_cc)
print("# of orphans:", orphans)

    