import sys
import os
sys.path.append("../../")
from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
phase = parameters["phase"]

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-tmp-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

mg_cc = {}
cc_mg = {}

lines = conn_comp_fh.readlines()
for line in lines:
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    mg_cc[mg_id] = cc_id
    if not cc_id in cc_mg:
        cc_mg[cc_id] = set([])
    cc_mg[cc_id].add(mg_id)
conn_comp_fh.close();
diffTime("read new cc")

#if not os.path.exists(graph_fn) or os.path.getsize(graph_fn)<10:

all_multigenes = set([])

it = 3
end = False 
while not end:
    text = str(it)+": "
    for strain_id in strains.allGenomes():
        text += "\t;"
        new_multigenes = set([])
        new_cc_ids = set([])
        input_fn = parameters["CLOSURE_RESULTS_ANN"]+"new-"+strain_id+"-"+str(it)+".txt"
        if os.path.exists(input_fn):
            input_fh = open(input_fn)
            new_annotation = readNewAnnotationsFromFile(input_fh, strain_id);
  #           print it, strain_id, len(new_annotation.genes)
            for gene in list(new_annotation.genes.values()):
                mg_id = str(gene.end)+"."+gene.strand+"."+gene.strain
                cc_id = mg_cc[mg_id]
                if not cc_id in new_cc_ids:
                    new_cc_ids.add(cc_id)
                if not mg_id in all_multigenes:
                    all_multigenes.add(mg_id)
                    new_multigenes.add(mg_id)
            input_fh.close()
        else:
            end = True
            break
        if len(new_multigenes) >0:
            text += "*"
        for cc_id in new_cc_ids:
            text += cc_id+","

    print(text)
    it += 1;
                 
   
