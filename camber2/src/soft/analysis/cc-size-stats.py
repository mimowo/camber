import sys
import time
import os
sys.path.append("../../")

from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

phase = int(sys.argv[1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())

conn_comps = {}
conn_comp_sizes = {}
sizes_distr = {}

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

while 1:
    line = conn_comp_fh.readline()
    if not line:
        break
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    ann_id = tokens[2]
    mg_tokens = mg_id.split(".")
    strain = mg_tokens[2]
    if not cc_id in conn_comps:
        conn_comps[cc_id] = {}
        conn_comp_sizes[cc_id] = 0
    cc = conn_comps[cc_id]
    if not strain in cc:
        cc[strain] = set([])
    cc[strain].add(ann_id+":"+mg_id)
    conn_comp_sizes[cc_id] += 1
conn_comp_fh.close();

diffTime("class");

print("cc count = ", len(conn_comps))
strains_count = len(strains.allGenomes())
anchors = 0
non_anchors = 0
core_anchors = 0
core_cc = 0
orphans = 0

conn_comp_class_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-class-"+str(phase)+".txt"
conn_comp_class_fh = open(conn_comp_class_fn, "w")

for i in range(0, len(strains.allGenomes())+1, 1):
    sizes_distr[i] = 0

ann_stats = {}
for strain_id in strains.allGenomes():
    ann_stats[strain_id] = {}
    for i in range(0, len(strains.allGenomes())+1, 1):
        ann_stats[strain_id][i] = 0
    

rem_stats = {}
for strain_id in strains.allGenomes():
    rem_stats[strain_id] = 0 
    
for cc_id in conn_comps:
    ann_strains = set([])
    cc = conn_comps[cc_id]
    anchor = True
    core = True
    if len(cc) < strains_count:
        core = False
    if len(cc) == 1:
        orphans += 1
    sizes_distr[len(cc)] += 1
    for strain_id in cc:
        if len(cc[strain_id]) > 1:
            anchor = False
        for full_mg_id in cc[strain_id]:
             tokens = full_mg_id.split(":")
             ann_id = tokens[0]
             if ann_id != "x":
                 ann_strains.add(strain_id)
    if core:
        core_cc += 1
    if anchor:
        anchors += 1
        if core:
            core_anchors += 1
    else:
        non_anchors += 1
    if anchor:
        conn_comp_class_fh.write(cc_id+"\t" + str(len(cc))+"\t"+str(len(ann_strains))+"\t"+"ANCHOR\t")
    else:
        conn_comp_class_fh.write(cc_id+"\t" + str(len(cc))+"\t"+str(len(ann_strains))+"\t"+"NON-ANCHOR\t")
    if core:
        for ann_strain_id in ann_strains:
            ann_stats[ann_strain_id][len(ann_strains)] += 1
    if  len(ann_strains) == 1:
        for strain_id in cc:
            rem_stats[strain_id] += 1
    conn_comp_class_fh.write("\n")
        
        
conn_comp_class_fh.close();  
 
print("anchors", anchors)
print("core anchors", core_anchors)
print("core", core_cc)
print("orphans", orphans)
print("non-anchors", non_anchors)

#strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
#for strain_id in strains_ordered:
#    text = strain_id +"\t"
#    for i in range(0, len(strains.allGenomes())+1, 1):
#        text += "\t" + str(ann_stats[strain_id][i])
#    print text

#for strain_id in strains_ordered:
#    print strain_id, rem_stats[strain_id]
    
for i in range(0, len(strains.allGenomes())+1, 1):
    print(i, sizes_distr[i])
    