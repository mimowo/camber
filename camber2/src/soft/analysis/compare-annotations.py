import sys
sys.path.append("../../")
from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.utils.mtb_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
import os


parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
#readAnnotations(strains.allGenomes())
iteration = 0
#readNewAnnotationsIter(strains.allGenomes(), iteration)

for strain_id in strains.allGenomes():
    print(strain_id)
    ann_old_fh = open(parameters["CLOSURE_RESULTS"]+"ann-old/"+"all-"+strain_id+"-"+str(8)+".txt")
    all_ann_old = readNewAnnotationsFromFile(ann_old_fh, strain_id)
    ann_old_fh.close()
    new_old_fh = open(parameters["CLOSURE_RESULTS"]+"ann-q/"+"all-"+strain_id+"-"+str(7)+".txt")
    all_ann_new = readNewAnnotationsFromFile(new_old_fh, strain_id)
    new_old_fh.close()
    count_add = 0
    count_rem = 0
    
    for gene_id in all_ann_old.genes:
        if not gene_id in  all_ann_new.genes:
            count_add += 1
            print("> ", strain_id, gene_id)
    for gene_id in all_ann_new.genes:
        if not gene_id in  all_ann_old.genes:
            count_rem += 1
            print("< ", strain_id, gene_id)
    print(count_add, count_rem)
    
    