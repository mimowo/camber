import sys
import time
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
readExtAnnotations(strains.allGenomes())

if len(sys.argv) > 1:
    short_cut = int(sys.argv[1])
else:
    short_cut = 0


strains_ordered = sorted(strains.allStrains(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    multigenes = {}
    long_multigenes = {}
    #print "started: ", strain_id
    strain = strains.getGenome(strain_id)
    print("strain:"+strain_id)
    for plasmid_id in strain.plasmids:
        print("-plasmid: "+plasmid_id)
