import sys
import time
import os
sys.path.append("../../")

from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

if len(sys.argv) > 1:
    phase = int(sys.argv[1])
else:
    phase = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()

strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
readExtAnnotations(strains.allGenomes())


multigenes = {}
strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    strain = strains.getGenome(strain_id)
    multigenes[strain_id] = {}
    for gene in list(strain.ext_annotation.genes.values()):
        multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        if not multigene_id in multigenes:
            multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, ann_id=gene.gene_id)
            multigenes[strain_id][multigene_id] = multigene
        else:
            multigene = multigenes[strain_id][multigene_id]
        multigene.addGene(gene)

conn_comps = {}
conn_comp_sizes = {}

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

while 1:
    line = conn_comp_fh.readline()
    if not line:
        break
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    ann_id = tokens[2]
    mg_tokens = mg_id.split(".")
    strain = mg_tokens[2]
    if not cc_id in conn_comps:
        conn_comps[cc_id] = {}
        conn_comp_sizes[cc_id] = 0
    cc = conn_comps[cc_id]
    if not strain in cc:
        cc[strain] = set([])
    cc[strain].add(ann_id+":"+mg_id)
    conn_comp_sizes[cc_id] += 1
conn_comp_fh.close();

strains_count = len(strains.allGenomes())
anchors = 0
non_anchors = 0
core_anchors = 0
core_cc = 0
orphans = 0
        
for cc_id in conn_comps:
    ann_strains = set([])
    cc = conn_comps[cc_id]
    anchor = True
    core = True
    if len(cc) < strains_count:
        core = False
    if len(cc) == 1:
        orphans += 1
    for strain_id in cc:
        if len(cc[strain_id]) > 1:
            anchor = False
        for full_mg_id in cc[strain_id]:
             tokens = full_mg_id.split(":")
             ann_id = tokens[0]
             if ann_id != "x":
                 ann_strains.add(strain_id)
    if core:
        core_cc += 1
    if anchor:
        anchors += 1
        if core:
            core_anchors += 1
    else:
        non_anchors += 1

    if len(ann_strains) < len(cc):
        longest = 0
        longest_id = "x"
        for strain_id in cc:
            for full_mg_id in cc[strain_id]:
                tokens = full_mg_id.split(":")
                mg_id = tokens[1]
                multigene = multigenes[strain_id][mg_id]
                if multigene.length() > longest:
                    longest = multigene.length()
                    longest_id = multigene.mg_strain_full_id
        if longest >= 300:
            text =  str(cc_id) +"\t" +str(longest)+"\t" +str(longest_id)+"\t"+ str(len(ann_strains))+"\t"+ str(len(cc))+"\t"+str(len(cc)/ len(ann_strains))
            for strain_id in ann_strains:
                text += "\t" + strain_id
            print(text)

        
        