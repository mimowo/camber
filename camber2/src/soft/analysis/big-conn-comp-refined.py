import sys
import time
import os
sys.path.append("../../")

from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

phase = int(sys.argv[1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()

strains_count = len(strains.allGenomes())
conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(0)+".txt"
conn_comp_fh = open(conn_comp_fn)

conn_comps = {}
old_mg_cc = {}
old_cc_mg = {}

lines = conn_comp_fh.readlines()
for line in lines:
    tokens = line.split()
    
    cc_id = tokens[0]
    mg_id = tokens[1]
    mg_tokens = mg_id.split(".")
    strain = mg_tokens[2]
    old_mg_cc[mg_id] = cc_id
    if not cc_id in old_cc_mg:
        old_cc_mg[cc_id] = set([])
    if not cc_id in conn_comps:
        conn_comps[cc_id] = {}
    if not strain in conn_comps[cc_id]:
        conn_comps[cc_id][strain] = set([])
    conn_comps[cc_id][strain].add(mg_id) 
    old_cc_mg[cc_id].add(mg_id)
conn_comp_fh.close();
diffTime("read old cc")


conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

new_conn_comps = {}
new_mg_cc = {}
new_cc_mg = {}

lines = conn_comp_fh.readlines()
for line in lines:
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    mg_tokens = mg_id.split(".")
    strain = mg_tokens[2]
    new_mg_cc[mg_id] = cc_id
    if not cc_id in new_cc_mg:
        new_cc_mg[cc_id] = set([])
    if not cc_id in new_conn_comps:
        new_conn_comps[cc_id] = {}
    if not strain in new_conn_comps[cc_id]:
        new_conn_comps[cc_id][strain] = set([])
    new_conn_comps[cc_id][strain].add(mg_id)
    new_cc_mg[cc_id].add(mg_id)
conn_comp_fh.close();
diffTime("read new cc")

subcomponents = {}
new_new_cc_ids = {}
resolved = {}

for new_cc_id in new_cc_mg:
    rep_mg_id = list(new_cc_mg[new_cc_id])[0]
    old_cc_id = old_mg_cc[rep_mg_id]
    
    if len(old_cc_mg[old_cc_id]) > len(new_cc_mg[new_cc_id]):
        if not old_cc_id in subcomponents:
            subcomponents[old_cc_id] = {}
        
        if not old_cc_id in resolved:
            resolved[old_cc_id] = True
            
        for strain in new_conn_comps[new_cc_id]:
            if len(new_conn_comps[new_cc_id][strain])>1:
                resolved[old_cc_id] = False
        
        new_new_cc_ids[new_cc_id] = old_cc_id + "-" + str(len(subcomponents[old_cc_id])+1)
        subcomponents[old_cc_id][new_new_cc_ids[new_cc_id]] = len(new_cc_mg[new_cc_id])   
    else:
        new_new_cc_ids[new_cc_id] = old_cc_id
    
        

for cc_id in conn_comps:
    cc = conn_comps[cc_id]
    anchor = True
    core = True
    if len(cc) < strains_count:
        core = False
    for strain_id in cc:
        if len(cc[strain_id]) > 1:
            anchor = False
    if not anchor:
        text = str(cc_id)+"\t"+ str(len(cc))+"\t"+ str(len(old_cc_mg[cc_id]) )+"\t"
        if cc_id in subcomponents:
            
            text += "Y"+"\t"
            if cc_id in resolved and resolved[cc_id]:
                text += "Y" +"\t"
            else:
                text += "N"+"\t"
            text += str(subcomponents[cc_id])
        else:
            text += "N"+"\t"+"N"
        print(text)
    
    
    