import sys
import time
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *

if len(sys.argv) > 1:
    phase = sys.argv[1]
else:
    phase = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
readExtAnnotations(strains.allGenomes())

tiss = {}      

strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    tiss[strain_id] = {}
    
    multigenes = {}
    strain = strains.getGenome(strain_id)

    for gene in list(strain.ext_annotation.genes.values()):
        multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        if not multigene_id in multigenes:
            multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, ann_id=gene.gene_id)
            multigenes[multigene_id] = multigene
        else:
            multigene = multigenes[multigene_id]
        multigene.addGene(gene)
    tiss[strain_id][-1] = len(multigenes)
    for mg in list(multigenes.values()):
        tis_c = len(mg.genes)
        if not tis_c in tiss[strain_id]:
            tiss[strain_id][tis_c] = 0
        tiss[strain_id][tis_c] += 1
        if tis_c >10:
            print(tis_c, mg.mg_strain_full_id)
        
        
tis_max_c = 0 
strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    for tis_c in tiss[strain_id]:
        if tis_c >tis_max_c:
            tis_max_c = tis_c
            
text = "strain_name"
for tis_c in range(tis_max_c, 0, -1):
    text += " "+str(tis_c)+"_TISs"
text += " " +  "#_of_multigenes"
      
print(text)
strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    #text = "\multicolumn{1}{|c|}{"+strain_id+"} "
    text = strain_id+" "
    for tis_c in range(tis_max_c, 0, -1):
        if tis_c in tiss[strain_id]:
            #text += " & "+ str(tiss[strain_id][tis_c])
            text += str(tiss[strain_id][tis_c]) +" "
        else:
            #text += " & " + str(0)
            text += str(0)+" "
    #text += " & "+str(tiss[strain_id][-1])+"\\\\"
    text += " "+str(tiss[strain_id][-1])
    print(text)    
 
    