import sys
import time
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
readExtAnnotations(strains.allGenomes())

if len(sys.argv) > 1:
    short_cut = int(sys.argv[1])
else:
    short_cut = 0

print("strain_name", "#_of_genes_before", "#_of_multigenes_after", "#_of_multigenes_>_"+str(short_cut) )
strains_ordered = sorted(strains.allStrains(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    multigenes = {}
    long_multigenes = {}
    #print "started: ", strain_id
    strain = strains.getGenome(strain_id)

    for gene in list(strain.ext_annotation.genes.values()):
        multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, ann_id=gene.gene_id)
        
        if not multigene_id in multigenes:
            multigenes[multigene_id] = multigene
        else:
            multigene = multigenes[multigene_id]
        multigene.addGene(gene)
        
        if gene.length() >= short_cut:
            if not multigene_id in long_multigenes:
                long_multigenes[multigene_id] = multigene
    
    print(strain_id, len(list(strain.annotation.genes.values())), len(multigenes), len(long_multigenes))            
    for plasmid_id in strain.plasmids:
        
        plasmid =  strain.plasmids[plasmid_id]
        multigenes = {}
        long_multigenes = {}
        for gene in list(plasmid.ext_annotation.genes.values()):
            multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
            multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, ann_id=gene.gene_id)
            
            if not multigene_id in multigenes:
                multigenes[multigene_id] = multigene
            else:
                multigene = multigenes[multigene_id]
            multigene.addGene(gene)
            
            if gene.length() >= short_cut:
                if not multigene_id in long_multigenes:
                    long_multigenes[multigene_id] = multigene        
        print("-", plasmid_id, len(list(plasmid.annotation.genes.values())), len(multigenes), len(long_multigenes))
        
    
    