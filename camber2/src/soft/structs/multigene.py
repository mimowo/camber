def getMultigeneStrain(multigene_id):
    return multigene_id.split(".")[2]    

class Multigene:
    def __init__(self, start, end, strand, strain, ann_id = "x", gene_name = "x"):
        if strand =="+":
            self.end = max(int(start), int(end))
            self.start = min(int(start), int(end))
        else:
            self.end = min(int(start), int(end))
            self.start = max(int(start), int(end))
        self.strand = strand;
        self.strain = strain;
        self.left_bound = min(self.start, self.end)
        self.right_bound = max(self.start, self.end)
        self.ann_id = ann_id
        self.chromosome = ""
        self.gene_name = gene_name
        self.mg_unique_id = createMultigeneId(self.end, self.strand)
        self.mg_strain_unique_id = createStrainMultigeneId(self.end, self.strand, self.strain)
        self.mg_full_id = createFullMultigeneId(self.ann_id, self.end, self.strand)
        self.mg_strain_full_id = createStrainFullMultigeneId(self.ann_id, self.end, self.strand, self.strain)
        self.genes = {}
        self.longest_gene = None;
    def setChromosome(self, local):
        self.chromosome = local
    def addGene(self, gene):
        #if gene.gene_name != "x":
        #    self.gene_name = gene.gene_name
        if gene.gene_id != "x":
            self.ann_id = gene.gene_id
            self.mg_full_id = createFullMultigeneId(self.ann_id, self.end, self.strand)
            self.mg_strain_full_id = createStrainFullMultigeneId(self.ann_id, self.end, self.strand, self.strain)
        if self.strand =="+":
            self.start = min(gene.start, self.start)
        else:
            self.start = max(gene.start, self.start)
        self.left_bound = min(self.start, self.end)
        self.right_bound = max(self.start, self.end)
        self.mg_longest_unique_id = createLongestUniqueMultigeneId(self.start, self.end, self.strand, self.strain)
        self.mg_longest_full_id = createLongestFullMultigeneId(self.ann_id, self.start, self.end, self.strand, self.strain)
        self.genes[gene.unique_id] = gene
        if self.longest_gene == None:
            self.longest_gene = gene
        else:
            if gene.length() > self.longest_gene.length():
                self.longest_gene = gene

    def length(self):
        return self.right_bound - self.left_bound + 1
    def meanlength(self):
        g_len = 0
        for gene in list(self.genes.values()):
            g_len += gene.length()
        return g_len/len(self.genes)
    def sequence(self):
        from soft.utils.mtb_utils import strains
        from soft.utils.mtb_seq_utils import complementarySequence

        strain_sequence = strains.getGenome(self.strain).sequence
        if self.strand == '+':
            gene_sequence = strain_sequence[self.left_bound-1:self.right_bound]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.left_bound-1:self.right_bound])
        return gene_sequence
    def sequence_with_promotor(self, promotor_length=100):
        from soft.utils.mtb_utils import strains
        from soft.utils.mtb_seq_utils import complementarySequence

        strain_sequence = strains.getGenome(self.strain).sequence
        if self.strand == '+':
            gene_sequence = strain_sequence[self.left_bound-1-promotor_length:self.right_bound]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.left_bound-1:self.right_bound+promotor_length])
        return gene_sequence    
    def createLongestUniqueMultigeneId(self):
        self.mg_longest_unique_id = str(self.start)+"."+str(self.end)+"."+self.strand+"."+self.strain

def createMultigeneFromGene(gene):
    multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, ann_id = gene.gene_id)
    return multigene

def createMultigeneId(end, strand):
    return str(end)+"."+strand

def createStrainMultigeneId(end, strand, strain):
    return str(end)+"."+strand+"."+strain

def createFullMultigeneId(ann_id, end, strand):
    return str(ann_id)+"."+str(end)+"."+strand

def createStrainFullMultigeneId(ann_id, end, strand, strain):
    return str(ann_id)+"."+str(end)+"."+strand+"."+strain

def createLongestUniqueMultigeneId(start, end, strand, strain):
    return str(start)+"."+str(end)+"."+strand+"."+strain

def createLongestFullMultigeneId(ann_id, start, end, strand, strain):
    return str(ann_id)+"."+str(start)+"."+str(end)+"."+strand+"."+strain

class MultigeneNode:
    def __init__(self, multigene):
        from soft.utils.mtb_utils import strains
        self.multigene = multigene
        self.targets = set([])
        self.targets_by_strain = {}
        self.next_mg_left = None
        self.next_mg_right = None
        for strain_id in strains.allGenomes():
            self.targets_by_strain[strain_id] = set([])
        self.conn_comp = None
    def addTarget(self, multigene_node):
        self.targets.add(multigene_node)
        self.targets_by_strain[multigene_node.multigene.strain].add(multigene_node)
    def setConnComp(self, conn_comp):
        self.conn_comp = conn_comp
    def hasTarget(self, multigene_node):
        return (multigene_node in self.targets)
    def getConnComp(self):
        return self.conn_comp
    def targetsByStrain(self, strain_id):
        return self.targets_by_strain[strain_id]
    def nextMgLeft(self):
        return self.next_mg_left
    def nextMgRight(self):
        return self.next_mg_right
    
class MultigeneGraph:
    nodes = {}
    nodes_by_strain = {}
    def __init__(self):
        from soft.utils.mtb_utils import strains
        self.nodes = {}
        self.nodes_by_strain = {}
        for strain in strains.allGenomes():
            self.nodes_by_strain[strain] = {}
    def predictAnchorNeighbours(self, strain1_id, strain2_id):
        nodes = list(self.nodes_by_strain[strain1_id].values())
        nodes_left = sorted(nodes, key=lambda mg_node:mg_node.multigene.left_bound)
        nodes_right = sorted(nodes, key=lambda mg_node:-mg_node.multigene.right_bound)
        for i in range(0, len(nodes_left), 1):
            mg_node = nodes_left[i]
            for j in range(i+1, len(nodes_left), 1):
                mg_node_next = nodes_left[j]
                conn_comp =  mg_node_next.getConnComp()
                if conn_comp.isAnchor():
                    if not strain2_id in conn_comp.strains:
                        continue
                    #print(mg_node_next.multigene.mg_strain_unique_id, mg_node.multigene.mg_strain_unique_id)
                    mg_node.next_mg_right = mg_node_next
                    break
        for i in range(0, len(nodes_right), 1):
            mg_node = nodes_right[i]
            for j in range(i+1, len(nodes_right), 1):
                mg_node_next = nodes_right[j]
                conn_comp =  mg_node_next.getConnComp()
                if conn_comp.isAnchor():
                    if not strain2_id in conn_comp.strains:
                        continue
                    mg_node.next_mg_left = mg_node_next
                    break       
            
    def predictNeighbours(self, strain_id):
        nodes = list(self.nodes_by_strain[strain_id].values())
        nodes_left = sorted(nodes, key=lambda mg_node:mg_node.multigene.left_bound)
        nodes_right = sorted(nodes, key=lambda mg_node:-mg_node.multigene.right_bound)
        for i in range(0, len(nodes_left), 1):
            mg_node = nodes_left[i]
            for j in range(i+1, len(nodes_left), 1):
                mg_node_next = nodes_left[j]
                mg_node.next_mg_right = mg_node_next
                break
        for i in range(0, len(nodes_right), 1):
            mg_node = nodes_right[i]
            for j in range(i+1, len(nodes_right), 1):
                mg_node_next = nodes_right[j]
                mg_node.next_mg_left = mg_node_next
                break
    def addMultigeneNode(self, multigene_node):
        multigene = multigene_node.multigene
        self.nodes_by_strain[multigene.strain][multigene.mg_strain_unique_id] = multigene_node
    def addMultigene(self, multigene):
        multigene_node = MultigeneNode(multigene)
        self.addMultigeneNode(multigene_node)
    def addEdgeByIds(self, mult1_id, mult2_id):
        strain1_id = getMultigeneStrain(mult1_id)
        strain2_id = getMultigeneStrain(mult2_id)
        mult_node1 = self.nodes_by_strain[strain1_id][mult1_id]
        mult_node2 = self.nodes_by_strain[strain2_id][mult2_id]
        self.addEdge(mult_node1, mult_node2)
    def addEdge(self, mult_node1, mult_node2):
        mult_node1.addTarget(mult_node2)
    def hasEdge(self, mult_node1, mult_node2):
        return mult_node1.hasTarget(mult_node2)
    def hasEdgeByIds(self, mult1_id, mult2_id):
        strain1_id = getMultigeneStrain(mult1_id)
        strain2_id = getMultigeneStrain(mult2_id)
        mult_node1 = self.nodes_by_strain[strain1_id][mult1_id]
        mult_node2 = self.nodes_by_strain[strain2_id][mult2_id]
        
        return self.hasEdge(mult_node1, mult_node2)
    def hasMultigeneNode(self, multigene_id, strain_id = None):
        if strain_id == None:
            strain_id = getMultigeneStrain(multigene_id)
        return (multigene_id in self.nodes_by_strain[strain_id])
    def getMultigeneNode(self, multigene_id, strain_id=None):
        if strain_id == None:
            strain_id = getMultigeneStrain(multigene_id)
        return self.nodes_by_strain[strain_id][multigene_id]
    def getMultigene(self, multigene_id, strain_id = None):
        if strain_id == None:
            strain_id = getMultigeneStrain(multigene_id)
        return self.nodes_by_strain[strain_id][multigene_id].multigene
    def hasMultigene(self, multigene_id, strain_id = None):
        if strain_id == None:
            strain_id = getMultigeneStrain(multigene_id)
        return (multigene_id in self.nodes_by_strain[strain_id])
    def getMultigeneNodeByStrain(self, multigene_id, strain_id):
        return self.nodes_by_strain[strain_id][multigene_id]
    def getMultigeneByStrain(self, multigene_id, strain_id):
        return self.nodes_by_strain[strain_id][multigene_id].multigene
    def getMultigenesByStrain(self, strain_id):
        return self.nodes_by_strain[strain_id]
    def getMultigeneNodes(self):
        return self.nodes
    def nodes(self):
        return self.nodes;
    def applyConnComps(self, conn_comps):
        for conn_comp in list(conn_comps.conn_comps.values()):
            for mult_node in list(conn_comp.nodes.values()):
                mult_node.setConnComp(conn_comp.name)
