class Genome(object):
    sequence = ""
    rev_sequence = ""
    name = ""
    strain_id = ""
    short_name = ""
    annotation = None
    new_annotation = None
    ext_annotation = None
    def __init__(self, genome_id, strain_id, short_name = ""):
        self.name = genome_id;
        if short_name == "":
            self.short_name = genome_id
        else:
            self.short_name = short_name 
        self.strain_id = strain_id
        self.annotation = None
        self.new_annotation = None
        self.ext_annotation = None
    def calcRevSequence(self):
        from soft.utils.mtb_seq_utils import complementarySequence
        self.rev_sequence = complementarySequence(self.sequence)
    def setSequence(self, sequence):
        self.sequence = sequence;
    def getSequence(self):
        return self.sequence;
    def setAnnotation(self, annotation):
        self.annotation = annotation;
    def annotation(self):
        return self.annotation;
    def setNewAnnotation(self, new_annotation):
        self.new_annotation = new_annotation;
    def setExtAnnotation(self, ext_annotation):
        self.ext_annotation = ext_annotation;
    def newAnnotation(self):
        return self.new_annotation;
    #def name(self):
    #    return self.name;

class Plasmid(Genome):

    def __init__(self, plasmid_id, strain_id, short_name= ""):
        super(Plasmid, self).__init__(plasmid_id, strain_id, short_name)

class Strain(Genome):
    def __init__(self, strain_id, short_name= ""):
        super(Strain, self).__init__(strain_id, strain_id, short_name)
        self.plasmids = {}
    def addPlasmid(self, plasmid):
        self.plasmids[plasmid.name] = plasmid 
    def plasmids(self):
        return self.plasmids
        
class Strains:
    strains = {}
    genomes_by_short_names = {}
    genomes = {}
    ref_strain = ""
    def __init_(self):
        self.strains = {}
        self.genomes = {}
        self.ref_strain = ""
    def setReferenceStrain(self, strain_name):
        self.ref_strain = strain_name;
    def refStrain(self):
        if self.ref_strain == "":
            return list(self.strains.keys())[0]
        return self.ref_strain
    def allGenomes(self):
        return list(self.genomes);
    def allStrains(self):
        return list(self.strains);
    def strain(self, strain_name):
        return self.strains[strain_name]
    def getGenome(self, genome_id):
        return self.genomes[genome_id]
    def addStrain(self, strain_id, short_name = ""):
        if short_name == "":
            short_name = strain_id        
        strain = Strain(strain_id, short_name)
        self.strains[strain_id] = strain
        self.genomes_by_short_names[strain.short_name] = strain#.getGenome()
        self.genomes[strain_id] = strain#.getGenome()
            
    def addPlasmid(self, strain_id, plasmid_id, short_name = ""):
        if short_name == "":
            short_name = plasmid_id
        
        strain = self.strains[strain_id]
        plasmid = Plasmid(plasmid_id, strain_id, short_name)
        strain.addPlasmid(plasmid)
        self.genomes_by_short_names[strain.short_name] = plasmid#.getGenome()
        self.genomes[plasmid_id] = plasmid#.getGenome()
    def strainsCount(self):
        return len(self.strains)
    def genomesCount(self):
        return len(self.genomes)


