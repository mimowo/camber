class ConnectedComponent:
    name = ""
    nodes = {}
    nodes_by_strain = {}
    is_anchor = False
    strains = set([])
    def __init__(self, name):
        from soft.utils.mtb_utils import strains
        self.name = name
        self.is_anchor = False
        self.nodes = {}
        self.strains = set([])
        self.nodes_by_strain = {}
        for strain in strains.allGenomes():
            self.nodes_by_strain[strain] = {}
    def isAnchor(self):
        if self.is_anchor:
            return True
        else:
            return False
    def nodes(self):
        return self.nodes
     
    def nodesByStrain(self, strain_id):
        return self.nodes_by_strain[strain_id]
                
    def addNode(self, mg_node):
        self.nodes[mg_node.multigene.mg_strain_unique_id] = mg_node
        self.nodes_by_strain[mg_node.multigene.strain][mg_node.multigene.mg_strain_unique_id] = mg_node
        if not mg_node.multigene.strain in self.strains:
            self.strains.add(mg_node.multigene.strain)
    def hasNode(self, mg_node):
        return mg_node.multigene.mg_strain_unique_id in self.nodes
    def addMutation(self, mutation):
        self.mutations.add(mutation)

class ConnectedComponents:
    def __init__(self):
        self.conn_comps = {}
        self.anchors = {}
        self.am_anchors = {}
        self.non_anchors = {}
        self.orphans = {}
        self.anchors_in_all_strain = {}
        self.anchors_not_in_all_strain = {}
        self.conn_comp_in_all_strain = {}
        self.am_anns_anchors = {}
    def addConnComp(self, conn_comp):
        self.conn_comps[conn_comp.name] = conn_comp
    def getConnComp(self, conn_id):
        return self.conn_comps[conn_id]
    def classifyConnComps(self):
        for conn_comp in list(self.conn_comps.values()):
            self.classifyConnComp(conn_comp)

    def classifyConnComp(self, conn_comp):
        from soft.utils.mtb_utils import strains
        is_non_anchor = False
        strains_count = 0
        am = False
        am_ann = False
        for strain_id in strains.allGenomes():
            if hasattr(strains,"plasmids") and strain_id in strains.plasmids:
                continue
            mg_count = len(conn_comp.nodesByStrain(strain_id))
            if hasattr(strains, "plasmids_by_strain"):
                for plasmid_id in strains.plasmids_by_strain[strain_id]:
                    mg_count += len(conn_comp.nodesByStrain(plasmid_id))
            
            if mg_count > 1:
                is_non_anchor = True
            if mg_count>0:
                strains_count += 1
            for mg_node in list(conn_comp.nodesByStrain(strain_id).values()):
                if len(mg_node.multigene.genes)>1:
                    am = True
                if mg_node.multigene.ann_id == "x":
                    am_ann = True
        if is_non_anchor == True:
            conn_comp.is_anchor = False
            self.non_anchors[conn_comp.name] = conn_comp
        else:
            conn_comp.is_anchor = True
            self.anchors[conn_comp.name] = conn_comp
            if am_ann:
                self.am_anns_anchors[conn_comp.name] = conn_comp
            if am:
                self.am_anchors[conn_comp.name] = conn_comp
            if strains_count == 1:
                self.orphans[conn_comp.name] = conn_comp

            if hasattr(strains, "plasmids") and strains_count == len(strains.allGenomes())-len(strains.plasmids):
                self.anchors_in_all_strain[conn_comp.name] = conn_comp
            else:
                self.anchors_not_in_all_strain[conn_comp.name] = conn_comp
        if strains_count == len(strains.allGenomes()):
            self.conn_comp_in_all_strain[conn_comp.name] = conn_comp

