class StrainsTreeNode:
    strain_name = ""
    parent = None
    children = []
    def __init__(self, strain_name, parent):
        self.strain_name = strain_name
        self.parent = parent
        self.children = []
    

class StrainTree:
    strain_nodes = {}
    strain_leafs = {}
    def __init__(self):
        self.strain_nodes = {}
        self.strain_leafs = {}
    root = None
    
    def other_neighbors(self, node_name):
        node = self.strain_nodes[node_name]
        parent = node.parent
        others = []
        for strain_node in parent.children:
            if strain_node.strain_name != node_name:
                others.append(strain_node)

        return others
    
    def neighbors(self, node_name):
        node = self.strain_nodes[node_name]
        parent = node.parent
        return parent.children
    def non_neighbors(self, node_name):
        neighbors = self.neighbors(node_name)
        others = []
        for strain_node in list(self.strain_leafs.values()):
            if not strain_node in neighbors:
                others.append(strain_node)
            
        return others
    
    
