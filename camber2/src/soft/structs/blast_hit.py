class BlastHit:
    def __init__(self, query_gene, hit_gene, evalue_b, aln_len=0, residues=0, identity_b="x", blast_score="x", query_start=0, query_end=0, evalue_a="x",  identities="x"):
        self.query_gene = query_gene
        self.hit_gene = hit_gene
        self.evalue_b = evalue_b
        self.evalue_a = evalue_a
        self.blast_score = blast_score
        self.identity_b = identity_b
        self.identities = identities
        self.residues = residues
        self.aln_len = aln_len
        self.query_start = int(query_start)
        self.query_end = int(query_end)
    def toString(self):
        out = self.hit_gene.unique_id;
        out += "\t" + str(self.hit_gene.start)
        out += "\t" + str(self.hit_gene.end)
        out += "\t" + self.hit_gene.strand
        out += "\t" + self.hit_gene.strain
        out += "\t" + self.query_gene.unique_id
        out += "\t" + str(self.query_gene.start)
        out += "\t" + str(self.query_gene.end)
        out += "\t" + self.query_gene.strand
        out += "\t" + self.query_gene.strain
        out += "\n"
        return out;
    def extToString(self):
        out = self.hit_gene_ext.unique_id
        out += "\t" + str(self.hit_gene_ext.start)
        out += "\t" + str(self.hit_gene_ext.end)
        out += "\t" + self.hit_gene_ext.strand
        out += "\t" + self.hit_gene_ext.strain
        out += "\t" + self.query_gene.unique_id
        out += "\t" + str(self.query_gene.start)
        out += "\t" + str(self.query_gene.end)
        out += "\t" + self.query_gene.strand
        out += "\t" + self.query_gene.strain
        out += "\n"
        return out;
    def addBlastHitExtended(self, hit_gene_ext):
        self.hit_gene_ext = hit_gene_ext
