class Gene:
    def __init__(self, start, end, strand, strain, gene_id="x", gene_name="x"):
        if strand == "+":
            self.plus_strand = True
            self.start = min(int(start), int(end))
            self.end = max(int(start), int(end))
            self.left_bound = self.start
            self.right_bound = self.end
        else:
            self.plus_strand = False
            self.start = max(int(start), int(end))
            self.end = min(int(start), int(end))
            self.left_bound = self.end
            self.right_bound = self.start
        self.strand = strand
        self.strain = strain
        
        self.unique_id = self.createGeneId(gene_id, self.left_bound, self.right_bound, strand)
        self.strain_unique_id = self.createStrainGeneId(gene_id, self.left_bound, self.right_bound, strand, strain)
        self.full_id = self.createFullGeneId(gene_id, self.left_bound, self.right_bound, strand)
        self.full_strain_id = self.createFullStrainGeneId(gene_id, self.left_bound, self.right_bound, strand, self.strain)
        self.gene_id = gene_id
        
    def createGeneId(self, gene_id, start, end, strand):
        return str(start)+"."+str(end)+"."+strand
    def createStrainGeneId(self, gene_id, start, end, strand, strain):
        return str(start)+"."+str(end)+"."+strand+"."+strain
    def createFullGeneId(self, gene_id, start, end, strand):
        return str(gene_id)+"."+str(start)+"."+str(end)+"."+strand
    def createFullStrainGeneId(self, gene_id, start, end, strand, strain):
        return str(gene_id)+"."+str(start)+"."+str(end)+"."+strand+"."+strain
    def startCodon(self):
        from soft.utils.mtb_utils import strains
        from soft.utils.mtb_seq_utils import complementarySequence
        strain_sequence = strains.getGenome(self.strain).sequence
        if self.strand == '+':
            gene_sequence = strain_sequence[self.left_bound-1:self.left_bound+2]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.right_bound-3:self.right_bound])
        return gene_sequence


    def sequence(self):
        from soft.utils.mtb_utils import strains
        from soft.utils.mtb_seq_utils import complementarySequence

        strain_sequence = strains.getGenome(self.strain).sequence
        if self.strand == '+':
            gene_sequence = strain_sequence[self.left_bound-1:self.right_bound]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.left_bound-1:self.right_bound])
        return gene_sequence

    def proteinSequence(self):
        from soft.utils.mtb_seq_utils import translateSequence
        return translateSequence(self.sequence())

    def length(self):
        return self.right_bound - self.left_bound + 1

    def toString(self):
        out = "";
        out += self.gene_id
        out += "\t" + str(self.start)
        out += "\t" + str(self.end)
        out += "\t" + self.strand
        out += "\n"
        return out;

def createGeneFromFullId(gene_id):
    tokens = gene_id.split(".")
    count = len(tokens)
    strain = tokens[count-1]
    strand = tokens[count-2]
    end = tokens[count-3]
    start = tokens[count-4]
    if count == 5:
        gene_id = tokens[count-5]
    else:
        gene_id = "x"
    gene = Gene(start, end, strand, strain, gene_id)
    return gene

def createGeneFromShortId(gene_id, strain_id):
    tokens = gene_id.split(".")
    count = len(tokens)
    strand = tokens[count-1]
    end = tokens[count-2]
    start = tokens[count-3]
    gene_id = "x"
    gene = Gene(start, end, strand, strain_id, gene_id)
    return gene

