import sys
import time
import os
sys.path.append("../../")

from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.structs.multigene import *
from soft.structs.conn_comp import *

phase = sys.argv[1]
#if len(sys.argv)>1:
#    phase = int(sys.argv[len(sys.argv)-1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime("params")
strains = readStrainsInfo()
diffTime("info")

ensure_dir(parameters["CLUSTER_SCRIPTS"])
res_dir = parameters["CLOSURE_RESULTS_REFINEMENT"]
ensure_dir(parameters["CLOSURE_RESULTS_REFINEMENT"])
ensure_dir(res_dir)  

species = parameters["SPECIES"]
exp_prefix = parameters["EXP_PREFIX"]
hssp = parameters["HSSP"]

cur_dir = os.path.abspath(os.curdir) +"/"

for strain1_id in strains.allGenomes(): 
#for strain1_id in strains.allGenomes():
    script_fn = parameters["CLUSTER_SCRIPTS"] + "ref"+strain1_id+".sh"
    script_fh = open(script_fn, "w")

    for strain2_id in strains.allGenomes(): 
        if strain1_id >= strain2_id:
            continue
        command = "python " + cur_dir + "remove-edges-strains-pair.py " 
        command += strain1_id + " " + strain2_id +" phase="+str(phase)+" SPECIES="+species+" EXP_PREFIX="+exp_prefix+" HSSP="+hssp
        command += "\n"
        script_fh.write(command)
            
    script_fh.close()
    os.system("qsub -cwd "+script_fn);
    
    
