import sys
import time
import os

sys.path.append("../../")


from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.structs.multigene import *
from soft.structs.conn_comp import *


strain1_id = sys.argv[1]
strain2_id = sys.argv[2]
#if len(sys.argv)>1:
#    phase = int(sys.argv[len(sys.argv)-1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
phase = int(parameters["phase"])
diffTime("params")
strains = readStrainsInfo()
diffTime("info")

multigene_graph = MultigeneGraph()
diffTime()
input_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_details.txt")
multigene_graph = readMultigenesGraphEff([strain1_id, strain2_id], multigene_graph, input_fh)
#multigene_graph = readMultigenesGraphEff(strains.allGenomes(), multigene_graph, input_fh)
input_fh.close()
print(len(multigene_graph.nodes))
print(len(multigene_graph.nodes_by_strain[strain1_id]))
print(len(multigene_graph.nodes_by_strain[strain2_id]))

diffTime("graph")
input_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_graph-"+str(phase)+".txt")
multigene_graph = readMultigeneGraphEdges(multigene_graph, phase, input_fh=input_fh, strain_set=[strain1_id, strain2_id])
input_fh.close()
diffTime("edges")
input_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt")
conn_comps = readConnComps(phase, multigene_graph, input_fh=input_fh, strain_set=[strain1_id, strain2_id])
diffTime("cc")
input_fh.close()
conn_comps.classifyConnComps()
diffTime("class")
multigene_graph.predictAnchorNeighbours(strain1_id, subset = [strain1_id, strain2_id])
diffTime("neighbours1")
multigene_graph.predictAnchorNeighbours(strain2_id, subset = [strain1_id, strain2_id])
diffTime("neighbours2")

print("all conn_comp", len(conn_comps.conn_comps))
print("anchors: ", len(conn_comps.anchors))
print("non-anchors: ", len(conn_comps.non_anchors))
print("orphans: ", len(conn_comps.orphans))
print("anchors in all strains: ", len(conn_comps.anchors_in_all_strain))
 

def supportedEdge(mult_node1, mult_node2):
    fl1 = mult_node1.nextMgLeft()
    fr1 = mult_node1.nextMgRight()
    fl2 = mult_node2.nextMgLeft()
    fr2 = mult_node2.nextMgRight()
    debug = False

    if fl1 == None or fl2 == None or fr1==None or fr2 == None:        
        return False
    if fl2 in fl1.targets and fr1 in fr2.targets:
        if debug == True:
            print("1t")
        return True
    if fl1 in fr2.targets and fl2 in fr1.targets:
        if debug == True:
            print("2t")
        return True
    if debug == True:
        print("false")
    return False
        
def doRefinement(output_fh):
    global components_by_strain, multigenes_by_strain, first_left, first_right

    for non_anchor in list(conn_comps.non_anchors.values()):
        for mult_node1 in list(non_anchor.nodes.values()):
            #if len(mult_node1.targets) == 1:
            #    continue
            for mult_node2 in mult_node1.targets:
                is_supported = supportedEdge(mult_node1, mult_node2)
                
                if not is_supported:
                    print("no support", mult_node1.multigene.mg_strain_unique_id, mult_node2.multigene.mg_strain_unique_id)
                    output_fh.write(mult_node1.multigene.mg_strain_unique_id+"\t"+mult_node2.multigene.mg_strain_unique_id+"\n")
                else:
                    print("supported", mult_node1.multigene.mg_strain_unique_id, mult_node2.multigene.mg_strain_unique_id)
                
 
res_dir = parameters["CLOSURE_RESULTS_REFINEMENT"]
output_fn = res_dir +"removed-" +strain1_id+"-"+strain2_id +"-"+str(phase) + ".txt"
if not os.path.exists(output_fn):
    output_fh = open(output_fn, "w")
    doRefinement(output_fh)
    output_fh.close()
