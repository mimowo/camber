import sys

sys.path.append("../../")
import os
from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import ensure_dir

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

ensure_dir(parameters["ANNOTATIONS"])

def convertGenBank(strain_id, input_fh, ann_fh):
    lines = input_fh.readlines()
    gene_name = "-"
    gene_id = "-"
    locus_id = "-"
    note_id = "-"
    orfid_id = "-"
    start = 0
    end = 0
    strand = "+"
    defined = False
    for i in range(0, len(lines), 1):
        line = lines[i]
        tokens = line.split()
        if (len(tokens) > 0):
            if (tokens[0] in ["gene", "CDS"] and len(tokens)==2 and tokens[1].count("..")>0) or (i==len(lines)-2):
                if start != 0 and end != 0 and defined:
                    if gene_id == "-":
                        gene_id = gene_name
                    if gene_name == "-":
                        gene_name = gene_id                       
                    if gene_name.count("RNA")==0 and gene_name.count("PP")==0 and gene_name.count("ncR")==0 and gene_name.count("IS")==0 and gene_name.count("fnm")==0 and gene_name.count("ssrA")==0:
#                        ann_fh.write(gene_name+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+str(length)+"\t-\t-\t"+gene_id+"\t"+gene_name+"\t-\n")
                        ann_fh.write(gene_id+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+gene_name+"\n")
                    defined = False
                    start = 0
                    end = 0
                    
            if tokens[0] == "CDS" and len(tokens)==2:    
                gene_name = "-"
                gene_id = "-"
                locus_id = "-"
                note_id = "-"
                orfid_id = "-"                
                defined = True
                
                if len(tokens) < 2:
                    continue
                if tokens[1].count("complement")>0:
                    strand = "-"
                    cords = tokens[1].split("(")[1].split(")")[0].split("..")
                    if len(cords) != 2:
                        continue
                    start = cords[0]
                    end = cords[1]
                else:
                    strand = "+"
                    cords = tokens[1].split("..")
                    if len(cords) != 2:
                        continue
                    start = cords[0]
                    end = cords[1]
                if start[0] in [">", "<"]:
                    start = start[1:] 
                if end[0] in [">", "<"] :
                    end = end[1:]
            if tokens[0].count("/gene")>0:
                gene_name = tokens[0].split('"')[1]
            if tokens[0].count("ORFID:")>0 and len(tokens) == 1:
                orfid_id = tokens[0].split('ORFID:')[1]
            if tokens[0].count('/note="')>0 and len(tokens) == 1:
                note_id = tokens[0].split('/note="')[1]
                if note_id.count("ORFID:")>0:
                    note_id = note_id.split('ORFID:')[1]   
            if tokens[0].count("/locus_tag")>0:
                locus_id = tokens[0].split('"')[1]
            if tokens[0].count("/pseudo")>0:
                defined = False
            if locus_id != "-":
                gene_id = locus_id
            elif orfid_id != "-":
                gene_id = orfid_id
            elif note_id != "-":
                gene_id = note_id
                                
            if gene_id[-1:] in ['.', ';', '"', ' ']:
                gene_id = gene_id[:-1]
            if gene_id[-1:] in ['.', ';', '"', ' ']:
                gene_id = gene_id[:-1]
                
progress = CAMBerProgress("Formatting annotations:")
n = len(strains.allGenomes())
progress.setJobsCount(n)

for strain in strains.allGenomes():
    
    input_fn = parameters["GENBANKS"]+"gb-"+strain+".gb"
    ann_fn = parameters["ANNOTATIONS"]+"ann-"+strain+".txt"

    input_fh = open(input_fn)
    ann_fh = open(ann_fn, "w")

    convertGenBank(strain, input_fh, ann_fh)

    input_fh.close()
    ann_fh.close()
    progress.update()