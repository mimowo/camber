import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.utils.mtb_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *


def saveBlastHits(blast_hits, output_fh):
    for blast_hit in blast_hits:
        output_fh.write(blast_hit.toString())

def saveBlastResults(blast_results):
    text = ""
    for blast_hit in blast_results:
        text_line = blast_hit.extToString();
        text += text_line
    return text

def parseBlastResultsStrains(params):
    strain1_id, strain2_id = params

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    iteration = computeIteration(parameters["CLOSURE_ITERATION"])

    readStrainsSequences([strain1_id, strain2_id], verbose=False)
    strains.getGenome(strain1_id).calcRevSequence()
    strains.getGenome(strain2_id).calcRevSequence()

    readExtAnnotations([strain1_id, strain2_id], iteration, verbose=False)
    readNewAnnotationsIter([strain1_id, strain2_id], iteration, verbose=False)

    output_fn = parameters["CLOSURE_RESULTS_BLAST"]+"blast-"+strain1_id+"-"+strain2_id+"-"+str(iteration)+".txt"
                  
    blast_results = parseBlastResults(strain1_id, strain2_id)

    output_fh = open(output_fn, "w")
    text = saveBlastResults(blast_results)
    output_fh.write(text)
    output_fh.close();

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    iteration = computeIteration(parameters["CLOSURE_ITERATION"])
    print("Parses computed BLASTs to transfer annotations between strains.")
    print("Current iteration: " + str(iteration))

    strains = readStrainsInfo()

    ensure_dir(parameters["CLOSURE_RESULTS_BLAST"])

    TASKS = []        
    for strain1_id in strains.allGenomes(): 
        for strain2_id in strains.allGenomes(): 
            if strain1_id == strain2_id:
                continue
            TASKS.append((strain1_id, strain2_id))   
        
    WORKERS = int(parameters["WORKERS"])

    progress = CAMBerProgress("BLAST parse:")
    progress.setJobsCount(len(TASKS))

    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(parseBlastResultsStrains, TASKS):
            progress.update()
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            parseBlastResultsStrains(TASK)
            progress.update()
