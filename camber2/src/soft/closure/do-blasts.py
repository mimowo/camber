import sys
import os
import time
import multiprocessing

sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir
from soft.utils.mtb_progress import CAMBerProgress
from soft.utils.mtb_utils import standardDnaBlastList
from soft.utils.mtb_io_utils import writeGene

def saveQueriesDna(strain_id):
    strain = strains.getGenome(strain_id)
    genes = strain.newAnnotation().genes
    queries_dir = parameters["BLAST_QUERIES_FOLDER"] + strain_id
    for gene in list(genes.values()):
        query_fn = os.path.normpath(queries_dir+'/'+gene.unique_id+'.fasta')
        if not os.path.exists(query_fn) or os.path.getsize(query_fn)<20:
            query_fh = open(query_fn, "w+");
            writeGene(query_fh, gene.sequence(), gene.strain_unique_id)
            query_fh.close();

def blastDna(params):
    strain1_id, strain2_id = params

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    iteration = computeIteration(parameters["CLOSURE_ITERATION"])
    
    queries_dir = parameters["BLAST_QUERIES_FOLDER"] + strain1_id
    results_dir = parameters["BLAST_RESULTS_FOLDER"] + strain1_id+'-'+strain2_id

    readStrainsSequences([strain1_id], verbose=False)
    readNewAnnotationsIter([strain1_id], iteration, verbose=False)

    saveQueriesDna(strain1_id)

    tr_evalue = parameters["BLAST_EVALUE"]
    genome_fn = parameters["GENOMES"]+'seq-' + strain2_id + ".fasta";
    strain1 = strains.getGenome(strain1_id)
    genes = strain1.newAnnotation().genes
    
    any_new = False
    for gene in list(genes.values()):
        #print( gene.strain_unique_id)
        query_fn = queries_dir + "/" + gene.unique_id + ".fasta";
        src_output_fn = results_dir + "/tmp-" + gene.strain_unique_id + ".txt";
        dst_output_fn = results_dir + "/blast-" + gene.strain_unique_id + ".txt";

        if not os.path.exists(dst_output_fn):
            output_fn = results_dir + "/blast-" + gene.strain_unique_id + ".xml";
            blast_exe = parameters["BLAST_PATH"]
            
            standardDnaBlastList(blast_exe, query_fn, genome_fn, src_output_fn, tr_evalue);
            if os.path.exists(dst_output_fn):
                os.remove(dst_output_fn)
            os.rename(src_output_fn, dst_output_fn)
            
    #progress.update()


if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    iteration = computeIteration(parameters["CLOSURE_ITERATION"])
    print("Computes BLASTs to transfer annotations between strains.")
    print("Current iteration: " + str(iteration))

    ensure_dir(parameters["BLAST_FOLDER"])
    ensure_dir(parameters["BLAST_RESULTS_FOLDER"])
    ensure_dir(parameters["BLAST_QUERIES_FOLDER"])
    print(parameters["BLAST_FOLDER"])
    print(parameters["BLAST_RESULTS_FOLDER"])

    if not os.path.exists(parameters["BLAST_PATH"]):
        print("Cannot find BLAST at the specified location: "+parameters["BLAST_PATH"])
        print("Customize the file: parameters-paths-default.txt")
        exit()
    
    strains = readStrainsInfo()
    
    for strain1_id in strains.allGenomes():
        queries_dir = parameters["BLAST_QUERIES_FOLDER"] + strain1_id
        ensure_dir(queries_dir)

        for strain2_id in strains.allGenomes():        
            results_dir = parameters["BLAST_RESULTS_FOLDER"] + strain1_id+'-'+strain2_id  
            ensure_dir(results_dir)
      
    progress = CAMBerProgress("BLASTs:")
    
    WORKERS = int(parameters["WORKERS"])

    TASKS = []
    for strain1_id in strains.allGenomes(): 
        for strain2_id in strains.allGenomes():
            if strain1_id == strain2_id:
                continue
            TASKS.append((strain1_id, strain2_id))   

    progress.setJobsCount(len(TASKS))    

    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(blastDna, TASKS):
            progress.update()
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            blastDna(TASK)
            progress.update()
