import sys
import os
import time

sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

params = ["HSSP", "SPECIES", "EXP_PREFIX", "WORKERS"]

python_exe = sys.executable

prepare_dbs_command = python_exe + " prepare-databases.py" + createCommandParams(params, parameters)
prepare_command = python_exe + " prepare-anns.py" + createCommandParams(params, parameters)
do_blast_command = python_exe +" do-blasts.py" + createCommandParams(params, parameters)
parse_blast_command = python_exe +" parse-blasts.py" + createCommandParams(params, parameters)
concat_command = python_exe +" merge-anns.py" + createCommandParams(params, parameters)

s_time = time.time()

os.system(prepare_dbs_command)
os.system(prepare_command)
for i in range(0, 100, 1):
    tb0 = time.time()
    os.system(do_blast_command)
    tb1 = time.time()
    print(str(i) + " blasts " + str(tb1-tb0))
    os.system(parse_blast_command)
    os.system(concat_command)
    iteration = computeIteration(parameters["CLOSURE_ITERATION"])
    end = True
    for strain_id in strains.allGenomes():
        strain_fn = parameters["CLOSURE_RESULTS_ANN"]+"new-"+strain_id+"-"+str(iteration)+".txt"
        if os.path.getsize(strain_fn) > 0:
            end = False
    if end:
        break;

t_time = time.time()

print("TIME", t_time-s_time)

