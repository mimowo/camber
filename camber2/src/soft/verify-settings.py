import sys
import os
sys.path.append("../")

from soft.utils.mtb_utils import overwriteParameters, readParameters

parameters = overwriteParameters(sys.argv)
parameters = readParameters()

print("Parameters: ")

for param in parameters:
    text = param + ": "+parameters[param]
    print(text)

print()
print("Paths:")

print("CAMBer location: "+parameters["CAMBER_PATH"])
if not os.path.exists(parameters["BLAST_FORMATDB_PATH"]):
    print("Cannot find BLAST at the specified location: "+parameters["BLAST_FORMATDB_PATH"])
    print("Customize the file: parameters-paths-default.txt")
    exit()
else:
    print("OK: FORMATDB is at the specified location: "+parameters["BLAST_FORMATDB_PATH"])
    
if not os.path.exists(parameters["BLAST_PATH"]):
    print("Cannot find BLAST at the specified location: "+parameters["BLAST_PATH"])
    print("Customize the file: parameters-paths-default.txt")
    exit()
else:
    print("OK: BLAST is at the specified location: "+parameters["BLAST_PATH"])   

if not os.path.exists(parameters["CAMBER_RESOURCE_PATH"]):
    print("Cannot find the experiment folder at the specified location: "+parameters["CAMBER_RESOURCE_PATH"])
    print("Customize the file: parameters-resources.txt")
    exit()
else:
    print("OK: The experiment folder at the specified location "+parameters["CAMBER_RESOURCE_PATH"])   


