import sys
import time
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

if len(sys.argv)>1:
    phase = int(sys.argv[1]);
else:
    phase = 0

res_dir = parameters["CLOSURE_RESULTS_REFINEMENT"]
removed_graph = set([])

nr_removed = 0

for strain1_id in strains.allGenomes():
    for strain2_id in strains.allGenomes():
        if strain1_id == strain2_id:
            continue
        input_fn = res_dir + "removed-" +strain1_id+"-"+strain2_id +"-"+str(phase) + ".txt"
        if os.path.exists(input_fn):
            input_fh = open(input_fn)
            
            lines = input_fh.readlines()
            for line in lines:
                nr_removed += 1
                removed_graph.add(line)

if nr_removed == 0:
    print("The refinement procedure was already computed after: ", phase, " phases.")
else:
    print("The refinement procedure: removal of ", nr_removed ," not supported edges.")
    multigene_old_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_graph-"+str(phase)+".txt"
    multigene_old_fh = open(multigene_old_fn)
    multigene_new_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_graph-"+str(phase+1)+".txt"
    multigene_new_fh = open(multigene_new_fn, "w")
                              
    while 1:
        line = multigene_old_fh.readline()
        if not line:
            break
        if not line in removed_graph:
            multigene_new_fh.write(line)
    
    multigene_old_fh.close();
    multigene_new_fh.close();
    print("Finished.")
