import sys
import time
import os
import multiprocessing
    
sys.path.append("../../")

from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.structs.multigene import *
from soft.structs.conn_comp import *


def supportedEdge(mult_node1, mult_node2):
    fl1 = mult_node1.nextMgLeft()
    fr1 = mult_node1.nextMgRight()
    fl2 = mult_node2.nextMgLeft()
    fr2 = mult_node2.nextMgRight()
    debug = False
    
    if fl1 == None or fl2 == None or fr1==None or fr2 == None:        
        return False
    if fl2 in fl1.targets and fr1 in fr2.targets:
        #if debug == True:
        #    print("1t")
        return True
    if fl1 in fr2.targets and fl2 in fr1.targets:
        #if debug == True:
        #    print("2t")
        return True
    #if debug == True:
    #    print("false")
    return False

def doRefinement(output_fh, conn_comps):
    for non_anchor in list(conn_comps.non_anchors.values()):
        for mult_node1 in list(non_anchor.nodes.values()):

            for mult_node2 in mult_node1.targets:
                is_supported = supportedEdge(mult_node1, mult_node2)
                
                if not is_supported:
                    output_fh.write(mult_node1.multigene.mg_strain_unique_id+"\t"+mult_node2.multigene.mg_strain_unique_id+"\n")
                else:
                    pass
 
def doRefinementStrains(params):
    strain1_id, strain2_id, phase = params

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    strains = readStrainsInfo()

    multigene_graph = MultigeneGraph()
    #print("mult0")
    input_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_details.txt")
    multigene_graph = readMultigenesGraphEff([strain1_id, strain2_id], multigene_graph, input_fh)
    input_fh.close()
    #print("mult1")
    input_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_graph-"+str(phase)+".txt")
    multigene_graph = readMultigeneGraphEdges(multigene_graph, input_fh=input_fh, strain_set=[strain1_id, strain2_id])
    input_fh.close()
    
    #print("mult2")
    input_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt")
    conn_comps = readConnComps(multigene_graph, input_fh=input_fh, strain_set = [strain1_id, strain2_id])
    input_fh.close()
    conn_comps.classifyConnComps()
    #print("mult3")
    multigene_graph.predictAnchorNeighbours(strain1_id, strain2_id)
    #print("mult4") 
    multigene_graph.predictAnchorNeighbours(strain2_id, strain1_id)
    #print("mult5") 
    res_dir = parameters["CLOSURE_RESULTS_REFINEMENT"]
    output_fn = res_dir +"removed-" +strain1_id+"-"+strain2_id +"-"+str(phase) + ".txt"
    if not os.path.exists(output_fn):
        output_fh = open(output_fn, "w")
        doRefinement(output_fh, conn_comps)
        output_fh.close()
    #print("mult5")

def doRefinementStrain(strain1_id):
    for strain2_id in strains.allGenomes(): 
        if strain1_id >= strain2_id:
            continue
        doRefinementStrains(strain1_id, strain2_id)
        

if __name__ == '__main__':
    if len(sys.argv)>1:
        phase = int(sys.argv[1])
    else:
        phase = 0

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    strains = readStrainsInfo()

    ensure_dir(parameters["CLOSURE_RESULTS_REFINEMENT"])


    progress = CAMBerProgress("Refinement:")
    n = len(strains.allGenomes())
    progress.setJobsCount(n*(n-1)/2)        

    WORKERS = int(parameters["WORKERS"])

    TASKS = []
    for strain1_id in strains.allGenomes(): 
        for strain2_id in strains.allGenomes(): 
            if strain1_id >= strain2_id:
                continue
            TASKS.append((strain1_id, strain2_id, phase))

    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(doRefinementStrains, TASKS):
            progress.update(desc=str(r))
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            doRefinementStrains(TASK)
            progress.update()

