import sys
import os
import time

sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *

if len(sys.argv)>1:
    phase = int(sys.argv[1])
else:
    phase = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

params = ["HSSP", "SPECIES", "EXP_PREFIX"]

python_exe = sys.executable

remove_command = python_exe + " remove-edges-all.py "+str(phase)+" " + createCommandParams(params, parameters)
mggraph_command = python_exe + " create-multigene-graph-edges-removed.py "+str(phase)+" " + createCommandParams(params, parameters)
cc_command = python_exe + " compute-components.py "+str(phase+1)+" " + createCommandParams(params, parameters)
ref_command = python_exe + " compute-components-refined.py "+str(phase+1)+" " + createCommandParams(params, parameters)
det_command = python_exe + " save-components-details.py "+str(phase+1)+" " + createCommandParams(params, parameters)

s_time = time.clock()

os.system(remove_command)
os.system(mggraph_command)
os.system(cc_command)
os.system(ref_command)
os.system(det_command)

t_time = time.clock()

print("TIME", t_time-s_time)

