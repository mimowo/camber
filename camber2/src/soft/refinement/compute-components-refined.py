import sys
import time
import os

sys.path.append("../../")

from soft.utils.mtb_utils import *
from soft.structs.multigene import *
from soft.utils.mtb_cc_utils import *

if len(sys.argv)>1:
    phase = int(sys.argv[1]);
else:
    phase = 1 
    
parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase-1)+".txt"
conn_comp_fh = open(conn_comp_fn)

old_mg_cc = {}
old_cc_mg = {}

lines = conn_comp_fh.readlines()
for line in lines:
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    old_mg_cc[mg_id] = cc_id
    if not cc_id in old_cc_mg:
        old_cc_mg[cc_id] = set([])
    old_cc_mg[cc_id].add(mg_id)
conn_comp_fh.close();


conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

new_mg_cc = {}
new_cc_mg = {}

lines = conn_comp_fh.readlines()
for line in lines:
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    new_mg_cc[mg_id] = cc_id
    if not cc_id in new_cc_mg:
        new_cc_mg[cc_id] = set([])
    new_cc_mg[cc_id].add(mg_id)
conn_comp_fh.close();

subcomponents = {}
new_new_cc_ids = {}

print("Re-naming of sub-connected components.")
for new_cc_id in new_cc_mg:
    rep_mg_id = list(new_cc_mg[new_cc_id])[0]
    old_cc_id = old_mg_cc[rep_mg_id]
    
    if len(old_cc_mg[old_cc_id]) > len(new_cc_mg[new_cc_id]):
        if not old_cc_id in subcomponents:
            subcomponents[old_cc_id] = 0
        subcomponents[old_cc_id] += 1
        new_new_cc_ids[new_cc_id] = old_cc_id + "-" + str(subcomponents[old_cc_id])   
    else:
        new_new_cc_ids[new_cc_id] = old_cc_id

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-tmp-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn, "w")

for mg_id in new_mg_cc:
    new_cc_id = new_mg_cc[mg_id]
    new_new_cc_id = new_new_cc_ids[new_cc_id]
    conn_comp_fh.write(new_new_cc_id +"\t" +mg_id+"\n")
    
conn_comp_fh.close();
print("Finished.")
