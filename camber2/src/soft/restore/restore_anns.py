import sys
import os


sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene
from soft.structs.multigene import *
from soft.structs.annotation import Annotation
from soft.structs.gene import Gene

if __name__ == '__main__':

    
    parameters = overwriteParameters(sys.argv)
    parameters["EXP_PREFIX"] = "res"
    parameters = readParameters()
    diffTime()
    strains = readStrainsInfo()
    readStrainsSequences(strains.allGenomes())
    diffTime("sequences")

    ensure_dir(parameters["BLAST_FOLDER"])
    ensure_dir(parameters["RESULTS_FOLDER"]);
    ensure_dir(parameters["CAMBER_PARAM_RESULTS_FOLDER"]);
    ensure_dir(parameters["CLOSURE_RESULTS"])
    ensure_dir(parameters["CLOSURE_RESULTS_ANN"])
    ensure_dir(parameters["CLOSURE_RESULTS_PSEUDO"])

    iteration_fh = open(parameters["CLOSURE_ITERATION"],"w")
    iteration_fh.write("1\n")
    iteration_fh.close()

    input_fh = open(parameters["CAMBER_RESOURCE_PATH"] + "supp.txt")
    lines = input_fh.readlines()
    input_fh.close()

    annotations_new_map = {}
    annotations_all_map = {}
    annotations_old_map = {}

    line0 = lines[0]
    tokens0 = line0.strip().split("\t")
    strains0 = tokens0[5:]

    for strain_id in strains.allGenomes():
        annotation_new = Annotation()
        annotation_all = Annotation()
        annotation_old = Annotation()
        annotations_new_map[strain_id] = annotation_new
        annotations_all_map[strain_id] = annotation_all
        annotations_old_map[strain_id] = annotation_old

    for line in lines[1:]:
        tokens = line.split("\t")
        if len(tokens) < 5: continue
        cc_id = tokens[0]
        strain_tokens = tokens[5:]
        for i in range(len(strain_tokens)):
            mg_tokens = strain_tokens[i].strip().strip('"').strip(";").strip(",").split(";")
            strain_id = strains0[i]

            for mg_token in mg_tokens:
                if len(mg_token) == 0:
                    continue
                try:
                    mg_info, mg_lengths = mg_token.split(":")
                
                    if mg_info.count(".") == 2:
                        mg_ann_id, mg_end_str, strand = mg_info.split(".")
                    else:
                        mg_ann1_id, mg_ann2_id,  mg_end_str, strand = mg_info.split(".")
                        mg_ann_id = mg_ann1_id + "." + mg_ann2_id
                except:
                    print("error", mg_token, mg_tokens)
                gene_end = int(mg_end_str)
            #    print(strain_id, mg_ann_id, mg_end, mg_strand, mg_lengths)
                len_tokens = mg_lengths.strip(",").split(",")
                for len_token in len_tokens:
                    if len_token.count("(") > 0:
                        len_str = len_token.split("(")[0].strip("*")
                    else:
                        len_str = len_token.strip("*")
             #       if len(len_str) == 0:
             #           print(len_token, len_tokens, line)
                    gene_len = int(len_str)
                    if len_token.count("*") > 0:
                        is_ann = True
                    else:
                        is_ann = False

            #        print(mg_ann_id, gene_end, strand, len_token, is_ann, gene_len)

                    if strand == "+":
                        gene_start = gene_end - gene_len + 1
                    else:
                        gene_start = gene_end + gene_len - 1

                    if is_ann:
                        gene = Gene(gene_start, gene_end, strand, strain_id, mg_ann_id)
                        annotations_old_map[strain_id].addGene(gene)
                        annotations_all_map[strain_id].addGene(gene)
                    else:
                        gene = Gene(gene_start, gene_end, strand, strain_id, "x")
                        annotations_new_map[strain_id].addGene(gene)
                        annotations_all_map[strain_id].addGene(gene)
                    


    for strain_id in strains.allGenomes():
        #print(len(annotations_old_map[strain_id].genes))
        annotation = annotations_old_map[strain_id]
        output_fh = open(parameters["CLOSURE_RESULTS_ANN"]+"all-"+strain_id+"-"+str(0)+".txt", "w")
        sorted_list = sorted(list(annotation.genes.values()), key=lambda gene:gene.left_bound)
        for gene in sorted_list:
            output_text = gene.toString()
            output_fh.write(output_text);
        output_fh.close()

    for strain_id in strains.allGenomes():
        annotation = annotations_new_map[strain_id]
        output_fh = open(parameters["CLOSURE_RESULTS_ANN"]+"new-"+strain_id+"-"+str(0)+".txt", "w")
        sorted_list = sorted(list(annotation.genes.values()), key=lambda gene:gene.left_bound)
        for gene in sorted_list:
            output_text = gene.toString()
            output_fh.write(output_text);
        output_fh.close()

    for strain_id in strains.allGenomes():
        annotation = annotations_all_map[strain_id]
        output_fh = open(parameters["CLOSURE_RESULTS_ANN"]+"all-"+strain_id+"-"+str(1)+".txt", "w")
        sorted_list = sorted(list(annotation.genes.values()), key=lambda gene:gene.left_bound)
        for gene in sorted_list:
            output_text = gene.toString()
            output_fh.write(output_text);
        output_fh.close()

