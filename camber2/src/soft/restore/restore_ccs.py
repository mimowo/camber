import sys
import os


sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir, writeGene
from soft.structs.multigene import *
from soft.structs.annotation import Annotation
from soft.structs.gene import Gene

if __name__ == '__main__':

    
    parameters = overwriteParameters(sys.argv)
    parameters["EXP_PREFIX"] = "res"
    parameters = readParameters()
    diffTime()
    strains = readStrainsInfo()
    readStrainsSequences(strains.allGenomes())
    diffTime("sequences")

    ensure_dir(parameters["BLAST_FOLDER"])
    ensure_dir(parameters["RESULTS_FOLDER"]);
    ensure_dir(parameters["CAMBER_PARAM_RESULTS_FOLDER"]);
    ensure_dir(parameters["CLOSURE_RESULTS"])
    ensure_dir(parameters["CLOSURE_RESULTS_GRAPHS"])

    input_fh = open(parameters["CAMBER_RESOURCE_PATH"] + "supp.txt")
    lines = input_fh.readlines()
    input_fh.close()

    tls0 = []
    tls1 = []

    line0 = lines[0]
    tokens0 = line0.strip().split("\t")
    strains0 = tokens0[5:]

    print("x", strains0)

    for line in lines[1:]:
        tokens = line.split("\t")
        if len(tokens) < 5: continue
        cc1_id = tokens[0]
        cc0_id = cc1_id.split("-")[0]

        strain_tokens = tokens[5:]
        for i in range(len(strain_tokens)):
            mg_tokens = strain_tokens[i].strip().strip('"').strip(";").strip(",").split(";")
            strain_id = strains0[i]

            for mg_token in mg_tokens:
                if len(mg_token) == 0:
                    continue
                try:
                    mg_info, mg_lengths = mg_token.split(":")
                
                    if mg_info.count(".") == 2:
                        mg_ann_id, mg_end_str, strand = mg_info.split(".")
                    else:
                        mg_ann1_id, mg_ann2_id,  mg_end_str, strand = mg_info.split(".")
                        mg_ann_id = mg_ann1_id + "." + mg_ann2_id
                except:
                    print("error", mg_token, mg_tokens)


                gene_end = int(mg_end_str)
                len_tokens = mg_lengths.strip(",").split(",")

                tl0 = cc0_id + "\t"
                tl0+= str(gene_end) + "." + strand + "." + strain_id + "\t"
                tl0+= mg_ann_id
                tl1 = cc1_id + "\t"
                tl1+= str(gene_end) + "." + strand + "." + strain_id + "\t"
                tl1+= mg_ann_id

                lens_tmp = ""

                for len_token in len_tokens:
                    if len_token.count("(") > 0:
                        len_str = len_token.split("(")[0].strip("*")
                    else:
                        len_str = len_token.strip("*")
                    gene_len = int(len_str)
                    if len_token.count("*") > 0:
                        is_ann = True
                        tl0 += "\t" + "*" + str(gene_len)
                        tl1 += "\t" + "*" + str(gene_len)
                    else:
                        is_ann = False
                        tl0 += "\t" + str(gene_len)
                        tl1 += "\t" + str(gene_len)
                tls0.append(tl0+"\n")
                tls1.append(tl1+"\n")                



    output_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(0)+".txt", "w")
    for tl in tls0:
        output_fh.write(tl)
    output_fh.close()

    output_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(1)+".txt", "w")
    for tl in tls1:
        output_fh.write(tl)
    output_fh.close()



