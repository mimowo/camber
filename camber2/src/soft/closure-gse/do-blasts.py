import sys
import os
sys.path.append("../../")

from threading import Thread
import time
import popen2
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir
from soft.utils.mtb_progress import CAMBerProgress
script_nr = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
iteration = computeIteration(parameters["CLOSURE_ITERATION"])
readNewAnnotationsIter(strains.allGenomes(), iteration)

ensure_dir(parameters["BLAST_FOLDER"])
ensure_dir(parameters["BLAST_RESULTS_FOLDER"])
ensure_dir(parameters["BLAST_QUERIES_FOLDER"])
ensure_dir(parameters["CLUSTER_SCRIPTS"])

MAX_PROCESSES = 12


saved_queries = set([])


def saveQueriesDna(strain_id):
    from soft.utils.mtb_io_utils import writeGene
    strain = strains.getGenome(strain_id)
    genes = strain.newAnnotation().genes
    queries_dir = parameters["BLAST_QUERIES_FOLDER"] + strain_id
    ensure_dir(queries_dir)
    for gene in list(genes.values()):
        query_fn = queries_dir+'/'+gene.unique_id+'.fasta'
        if not os.path.exists(query_fn) or os.path.getsize(query_fn)<20:
            query_fh = open(query_fn, "w");
            writeGene(query_fh, gene.sequence(), gene.strain_unique_id)
            query_fh.close();


def blastDna(strain1_id, strain2_id):
    global script_nr, saved_queries
    queries_dir = parameters["BLAST_QUERIES_FOLDER"] + strain1_id
    xml_results_dir = os.path.abspath(parameters["BLAST_RESULTS_FOLDER"] + strain1_id+'-'+strain2_id)  
    ensure_dir(xml_results_dir)  
    tr_evalue = parameters["BLAST_EVALUE"]
    
    genome_fn = parameters["GENOMES"]+'seq-' + strain2_id + ".fasta";
    strain1 = strains.getGenome(strain1_id)
    genes = strain1.newAnnotation().genes
    
    ensure_dir(parameters["CLUSTER_SCRIPTS"]+strain1_id+"-"+strain2_id+"/")
    script_fn = parameters["CLUSTER_SCRIPTS"]+strain1_id+"-"+strain2_id+"/b"+parameters["SPECIES"]+"-"+str(script_nr)+".sh"
    script_fh = open(script_fn, "w")
    
    any_new = False 
    for gene in list(genes.values()):
        query_fn = queries_dir + "/" + gene.unique_id + ".fasta";
        
        src_output_fn = xml_results_dir + "/tmp-" + gene.strain_unique_id + ".txt";
        dst_output_fn = xml_results_dir + "/blast-" + gene.strain_unique_id + ".txt";
        #print query_file_name
        if not os.path.exists(dst_output_fn):
            any_new = True
            blast_exe = parameters["BLAST_PATH"]
            if platform.system().count("Windows") > 0 :
                blast_exe = os.path.abspath(blast_exe)    
            
            command = blast_exe + " -p blastn -e "+str(tr_evalue)+" -F F ";
            command = command + " -d " + genome_fn + " -i " + query_fn + " -o " + src_output_fn + " -m 8\n";
            command = command + "mv "+src_output_fn+" "+dst_output_fn +"\n"
            script_fh.write(command)
            
    script_fh.close()
    if any_new:
        if not strain1_id in saved_queries:
            saved_queries.add(strain1_id)
            saveQueriesDna(strain1_id)
        script_nr += 1
        
        while True:
            time.sleep(1)
            r,w,e = popen2.popen3("qstat -u "+parameters["CLUSTER_USERNAME"]+" | wc -l")
            
            processes = int(r.readlines()[0])-2
            print(processes)
            r.close()
            w.close()
            e.close()
            time.sleep(1)
            if processes >= MAX_PROCESSES:
                time.sleep(5)
            else:
                break
            
        os.system("qsub -cwd "+script_fn);


progress = CAMBerProgress("BLASTs:")
n = len(strains.allGenomes())
progress.setJobsCount(n*(n-1))

def blast(strain1_id):

    for strain2_id in strains.allGenomes():
        if strain1_id != strain2_id:   

            blastDna(strain1_id, strain2_id)
            progress.update()
            
for strain1_id in strains.allGenomes():
    blast(strain1_id)
   #Thread(target=blast, args=[strain1_id]).start()


print("BLAST_END")


