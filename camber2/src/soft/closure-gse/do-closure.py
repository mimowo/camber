import sys
import os
sys.path.append("../..")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
import time
import popen2

def computed():
    r,w,e = popen2.popen3("qstat | grep "+parameters["CLUSTER_USERNAME"]+" | grep "+parameters["SPECIES"]+" | wc -l")
    lines = r.readlines()
    print("out: " + str(lines))
    processes = lines[0].split()[0]
    print("processes: " + str(processes))
    r.close()
    w.close()
    e.close()
    if processes == "0":
        return True
    else:
        return False

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

params = ["HSSP", "SPECIES", "EXP_PREFIX", "PID_TYPE"]

prepare_command = "python prepare-databases.py" + createCommandParams(params, parameters)
prepare_command = "python prepare-anns.py" + createCommandParams(params, parameters)
do_blast_command = "python run-blast-gse.py" + createCommandParams(params, parameters)
parse_blast_command = "python run-parse-gse.py" + createCommandParams(params, parameters)
concat_command = "python run-concat-gse.py" + createCommandParams(params, parameters)

#os.system(prepare_command)
for i in range(0, 100, 1):
    os.system(do_blast_command)
    print(do_blast_command)
    time.sleep(60)
    while not computed():
        time.sleep(10)
    os.system(parse_blast_command)
    print(parse_blast_command)
    time.sleep(60)
    while not computed():
        time.sleep(10)
    os.system(concat_command)
    print(concat_command)
    time.sleep(60)
    while not computed():
        time.sleep(10)
    time.sleep(60)
    iteration = computeIteration(parameters["CLOSURE_ITERATION"])
    end = True
    for strain_id in strains.allGenomes():
        strain_fh = open(parameters["CLOSURE_RESULTS_ANN"]+"new-"+strain_id+"-"+str(iteration)+".txt")
        new_annotation = readNewAnnotationsFromFile(strain_fh, strain_id)
        strain_fh.close()
        if len(new_annotation.genes) > 0:
            end = False
    if end:
        break;

