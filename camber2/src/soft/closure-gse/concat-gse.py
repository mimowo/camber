import sys
import os
sys.path.append("../..")
from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.utils.mtb_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.structs.annotation import *

strain2_id = sys.argv[1]
print(sys.argv)


parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
#readAnnotations(strains.allGenomes())
print(parameters["ITERATION"])
iteration = int(parameters["ITERATION"])
new_iteration = iteration + 1
#readNewAnnotationsIter(strains.allGenomes(), iteration)

def joinAnnotations(blast_orfs, new_annotation, all_annotation):
    for blast_gene in list(blast_orfs.genes.values()):
        blast_gene_id = blast_gene.unique_id
        if not blast_gene_id in all_annotation.genes:
            all_annotation.genes[blast_gene_id] = blast_gene
            new_annotation.genes[blast_gene_id] = blast_gene

new_annotation = Annotation();
strain_fh = open(parameters["CLOSURE_RESULTS_ANN"]+"all-"+strain2_id+"-"+str(iteration)+".txt")
all_annotation = readNewAnnotationsFromFile(strain_fh, strain2_id)
strain_fh.close()
for strain1_id in strains.allGenomes():
    if strain1_id != strain2_id:
        print(strain1_id, strain2_id)
        blast_orfs_fh = open(parameters["CLOSURE_RESULTS_BLAST"]+"blast-"+strain1_id+"-"+strain2_id+"-"+str(iteration)+".txt")
        blast_orfs = readBlastHitsAnnFromFile(blast_orfs_fh)
        blast_orfs_fh.close();
        joinAnnotations(blast_orfs, new_annotation, all_annotation)
new_orfs_fh = open(parameters["CLOSURE_RESULTS_ANN"]+"new-"+strain2_id+"-"+str(new_iteration)+".txt", "w")
sorted_list = sorted(list(new_annotation.genes.values()), key=lambda gene:gene.left_bound)
for gene in sorted_list:
    output_text = gene.toString()
    new_orfs_fh.write(output_text);
new_orfs_fh.close();
all_orfs_fh = open(parameters["CLOSURE_RESULTS_ANN"]+"all-"+strain2_id+"-"+str(new_iteration)+".txt", "w")
sorted_list = sorted(list(all_annotation.genes.values()), key=lambda gene:gene.left_bound)
for gene in sorted_list:
    output_text = gene.toString()
    all_orfs_fh.write(output_text);
all_orfs_fh.close();
