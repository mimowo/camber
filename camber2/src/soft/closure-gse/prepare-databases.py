import sys
sys.path.append("../../")
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import translateSequence
from soft.utils.mtb_io_utils import ensure_dir
from soft.utils.mtb_closure_utils import *
import os

diffTime("")
parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime("parameters")
strains = readStrainsInfo()

for strain_id in strains.allGenomes():
    genome_fn = parameters["GENOMES"]+'seq-' + strain_id + ".fasta";
    genome_fn = os.path.abspath(genome_fn)
    formatdb_exe = os.path.abspath(parameters["BLAST_FORMATDB_PATH"])
    print(genome_fn)
    if parameters["CLOSURE_QUERIES"].lower() == "dna":
        print("dna")
        os.system(formatdb_exe+" -p F -i "+genome_fn)
    if parameters["CLOSURE_QUERIES"].lower() == "prot":
        print("prot")
        os.system(formatdb_exe+" -p T -i "+genome_fn)
