import sys
sys.path.append("../..")
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import translateSequence
from soft.utils.mtb_io_utils import ensure_dir
from soft.utils.mtb_closure_utils import *
import os

print("Initialization of the closure procedure.")
print("Creates folders to store results")
print("Creates file iteration.txt to info which iteration of the closure procedure")
print("Creates files with list of genes used as BLAST queries")

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
readAnnotations(strains.allGenomes(), accept_alt_starts = True)

ensure_dir(parameters["BLAST_FOLDER"])
ensure_dir(parameters["RESULTS_FOLDER"]);
ensure_dir(parameters["CAMBER_PARAM_RESULTS_FOLDER"]);
ensure_dir(parameters["CLOSURE_RESULTS"])
ensure_dir(parameters["CLOSURE_RESULTS_ANN"])
ensure_dir(parameters["CLOSURE_RESULTS_PSEUDO"])

iteration_fh = open(parameters["CLOSURE_ITERATION"],"w")
iteration_fh.write("0\n")
iteration_fh.close();

iteration = 0;

progress = CAMBerProgress("Prepare:")
n = len(strains.allGenomes())
progress.setJobsCount(n)

strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    
    wrong_start = 0
    wrong_stop = 0
    wrong_length = 0
    stop_inside = 0
  
    all_output_fn = parameters["CLOSURE_RESULTS_ANN"]+"all-"  + strain_id+"-"+ str(iteration)+".txt"
    new_output_fn = parameters["CLOSURE_RESULTS_ANN"]+"new-" + strain_id+"-"+ str(iteration)+".txt"
    
    all_output_fh = open(all_output_fn, "w");
    new_output_fh = open(new_output_fn, "w");
    annotation = strains.getGenome(strain_id).annotation

    sorted_list = sorted(list(annotation.genes.values()), key=lambda gene:gene.left_bound)
    for gene in sorted_list:
        output_text = gene.toString()
        all_output_fh.write(output_text);
        if gene.startCodon() in start_codons:
            new_output_fh.write(output_text);
    new_output_fh.close()
    all_output_fh.close()        
    
    pseudo_output_fn = parameters["CLOSURE_RESULTS_PSEUDO"]+"pseudo-" + strain_id+".txt"
    
    pseudo_output_fh = open(pseudo_output_fn, "w");
    for pseudo_gene in list(annotation.pseudo_genes.values()):
        res_code =  correctGene(pseudo_gene)
        if res_code == -1:
            wrong_length += 1
        elif res_code == -2:
            wrong_start += 1
        elif res_code == -3:
            wrong_stop += 1
        elif res_code == -4:
            stop_inside += 1
        pseudo_output_fh.write(str(res_code)+" "+pseudo_gene.full_id+"\n")
        dna_sequence = pseudo_gene.sequence()
        pseudo_output_fh.write(dna_sequence+"\n")
        pseudo_output_fh.write(translateSequence(dna_sequence)+"\n")
        pseudo_output_fh.write("\n")
    pseudo_output_fh.close()
    progress.update()

