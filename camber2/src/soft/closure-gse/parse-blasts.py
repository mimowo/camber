import sys
import os
sys.path.append("../../")
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

ensure_dir(parameters["CLUSTER_SCRIPTS"])
ensure_dir(parameters["CLOSURE_RESULTS_BLAST"])

ensure_dir(parameters["CLOSURE_RESULTS_BLAST_PARSED"])

species = parameters["SPECIES"]
prefix = parameters["EXP_PREFIX"]
hssp = parameters["HSSP"]
params = ["HSSP","EXP_PREFIX", "SPECIES", "PID_TYPE"]

cur_dir = os.path.abspath(os.curdir) +"/"

for strain1_id in strains.allGenomes():
    script_fn = parameters["CLUSTER_SCRIPTS"] + "p"+species+"-"+hssp+"-"+strain1_id+".sh"
    script_fh = open(script_fn, "w")

    command = "python " + cur_dir + "rate-blast-gse.py " 
    command += strain1_id + createCommandParams(params, parameters)+"\n"
    print(command)
    script_fh.write(command)
            
    script_fh.close()
    os.system("qsub -cwd "+script_fn);

print("BLAST_END")
