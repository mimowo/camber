import sys
import os
sys.path.append("../../")
from threading import Thread
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import *
from soft.utils.mtb_io_utils import ensure_dir

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
iteration = computeIteration(parameters["CLOSURE_ITERATION"])
new_iteration = iteration + 1

ensure_dir(parameters["CLUSTER_SCRIPTS"])

species = parameters["SPECIES"]
prefix = parameters["EXP_PREFIX"]
hssp = parameters["HSSP"]
params = ["HSSP","EXP_PREFIX", "SPECIES", "PID_TYPE"]

cur_dir = os.path.abspath(os.curdir) +"/"

for strain1_id in strains.allGenomes():
    script_fn = parameters["CLUSTER_SCRIPTS"] + "c"+species+"-"+hssp+"-"+strain1_id+".sh"
    script_fh = open(script_fn, "w")

    command = "python " + cur_dir + "concat-gse.py "
    command += strain1_id +" ITERATION="+str(iteration) + createCommandParams(params, parameters)+"\n"
    print(command)
    
    script_fh.write(command)
            
    script_fh.close()
    os.system("qsub -cwd "+script_fn);

print("BLAST_END")

iteration_fh = open(parameters["CLOSURE_ITERATION"],"w")
iteration_fh.write(str(new_iteration)+"\n")
iteration_fh.close();
