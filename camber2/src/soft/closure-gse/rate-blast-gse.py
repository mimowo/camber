import sys
import os
sys.path.append("../../")
from soft.utils.mtb_utils import *
from soft.utils.mtb_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.utils.mtb_seq_utils import *

from threading import Thread

strain2_id = sys.argv[1]
print(sys.argv)


parameters = overwriteParameters(sys.argv)
parameters = readParameters()

print(strain2_id)

strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
strains.getGenome(strain2_id).calcRevSequence();
#readAnnotations(strains.allGenomes())
iteration = computeIteration(parameters["CLOSURE_ITERATION"])
print(iteration)
readNewAnnotationsIter(strains.allGenomes(), iteration)
camber_type = parameters["CLOSURE_QUERIES"].lower()

def saveBlastHits(blast_hits, output_fh):
    for blast_hit in blast_hits:
        output_fh.write(blast_hit.toString())


def saveBlastResults(blast_results):
    text = ""
    for blast_hit in blast_results:
        text_line = blast_hit.extToString();
        text += text_line
    return text

def parseBlastResultsStrain(strain2_id):
    #for strain2_id in ["KZN_1435"]:
    for strain1_id in strains.allGenomes():
      #  if strain2_id != "APECO1":
      #      continue    
        if strain1_id != strain2_id:
            output_fn = parameters["CLOSURE_RESULTS_BLAST"]+"blast-"+strain1_id+"-"+strain2_id+"-"+str(iteration)+".txt"
            print(strain1_id, strain2_id)
            print("output_fn: ", output_fn)
            if not os.path.exists(output_fn):# or os.path.getsize(output_fn) < 10000:              
                diffTime("praparing"+strain1_id+" "+strain2_id)
                
                good_blast_results = parseBlastResults(strain1_id, strain2_id)
                
                diffTime("parsing")
    
                output_fh = open(output_fn, "w")
                text = saveBlastResults(good_blast_results)
                output_fh.write(text)
                output_fh.close();
                
                diffTime("complete save")
        

parseBlastResultsStrain(strain2_id)

print("PARSE_BLAST_END")
