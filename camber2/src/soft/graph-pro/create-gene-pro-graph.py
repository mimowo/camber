import sys
import os
sys.path.append("../../")
from soft.utils.mtb_utils import *
from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import overwriteParameters
from soft.utils.mtb_io_utils import ensure_dir
from soft.utils.mtb_io_utils import readBlastEdges

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

graph = set([])

def calcGeneLenFromID(gene_id):
    tokens = gene_id.split(".")
    start = int(tokens[0])
    end = int(tokens[1])
    return (abs(end-start)+1)/3

def readRateBlastProEdges(input_fh):
    edges = set([])
    
    lines = input_fh.readlines()
    for line in lines:
        tokens = line.split()
        if len(line) < 3:
            continue
        gene1_id = tokens[0]
        gene2_id = tokens[1]
        gene1_len = calcGeneLenFromID(gene1_id)
        gene2_len = calcGeneLenFromID(gene2_id)
        #print line
        identity_b = float(tokens[2])
        aln_len = int(tokens[3])
        identities = int(round(identity_b*aln_len/100))
        mismatches = int(tokens[4])
        residues = identities+mismatches
        identity = int(round(identities*100/max(gene1_len, gene2_len)))
        if identity >= hssp(residues, 30.5):
            edges.add((gene1_id, gene2_id)) 
            
    return edges

def extendGraph(edges):
    global graph
    for edge in edges:
        start_node_id = edge[0]
        end_node_id = edge[1]
        if not (start_node_id, end_node_id) in graph and not (end_node_id, start_node_id) in graph:  
            graph.add(edge)

def saveGraph(output_fh):
    global graph;
    for edge in graph:
        start_node_id = edge[0]
        end_node_id = edge[1]
        output_fh.write(start_node_id + "\t"+end_node_id + "\n")

ensure_dir(parameters["CLOSURE_RESULTS_GRAPHS"])
graph_fn = parameters["CLOSURE_RESULTS_GRAPHS"]+"graph-pro.txt"

#if not os.path.exists(graph_fn) or os.path.getsize(graph_fn)<10:
graph_fh = open(graph_fn, "w")
for strain1 in strains.allGenomes():
    for strain2 in strains.allGenomes():
        if strain1 != strain2: #and strain1=="KZN_4207" and strain2=="F11":
            graph = set([])
            print(strain1, strain2)
            
            input_fn = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/results/"+"blast-"+strain1+"-"+strain2+".txt"
            if os.path.exists(input_fn):
                input_fh = open(input_fn)
                edges = readRateBlastProEdges(input_fh)
          #      print edges
                extendGraph(edges)
                input_fh.close()
            else:
                print(input_fn)
            print(len(graph))
            saveGraph(graph_fh)
graph_fh.close()
