import sys
import time
import os
sys.path.append("../../")

from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

phase = int(sys.argv[1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()

nodes = {}
multigene_nodes_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_nodes.txt"
multigene_nodes_fh = open(multigene_nodes_fn)
while 1:
    line =  multigene_nodes_fh.readline()
    if not line:
        break;
    node_id = line[:-1]
    nodes[node_id] = set([])
multigene_nodes_fh.close()
diffTime("read nodes")

multigene_graph_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_graph-pro-"+str(phase)+".txt")
while 1:
    line =  multigene_graph_fh.readline()
    if not line:
        break;
    tokens = line.split()
    
    node1_id = tokens[0]
    node2_id = tokens[1]
    nodes[node1_id].add(node2_id)
    nodes[node2_id].add(node1_id)
    
ccs = connected_components(nodes)    

multigene_graph_fh.close();

diffTime("read graph")

if phase == 0:
    conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-tmp-"+str(0)+".txt"
else:
    conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn, "w")

for cc_id in ccs:
    for mg_node_id in ccs[cc_id]:
        conn_comp_fh.write(str(cc_id)+"\t"+mg_node_id+"\n")

conn_comp_fh.close();

diffTime("saved cc")



