import sys
import time
import os

sys.path.append("../../")

from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
diffTime("sequences")
phase = sys.argv[1]

multigenes = {}
multigene_nodes_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_details.txt"
multigene_nodes_fh = open(multigene_nodes_fn)

while 1:
    line = multigene_nodes_fh.readline()
    if not line:
        break;
    tokens = line.split();
    multigenes[tokens[0]] = list()
    for token in tokens:
        multigenes[tokens[0]].append(token)
    
multigene_nodes_fh.close()

cc_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-tmp-"+str(phase)+".txt"
cc_fh = open(cc_fn)

cc_det_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-det-"+str(phase)+".txt"
cc_det_fh = open(cc_det_fn, "w")

while 1:
    line = cc_fh.readline()
    if not line:
        break
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    mult_dets = multigenes[mg_id]
    cc_det_fh.write(cc_id)
    for mult_det in mult_dets:
        cc_det_fh.write("\t"+mult_det)
    cc_det_fh.write("\n")
    
cc_fh.close()
cc_det_fh.close()


