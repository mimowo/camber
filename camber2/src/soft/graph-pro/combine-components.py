import sys
import time
import os

sys.path.append("../../")

from soft.utils.mtb_utils import *
from soft.structs.multigene import *
from soft.utils.mtb_cc_utils import *

phase = int(sys.argv[1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()

def readCCs(input_fh):
    cc = {}
    lines = input_fh.readlines()
    for line in lines:
        tokens = line.split()
        cc_id = tokens[0]
        mg_id = tokens[1]
        if not cc_id in cc:
            cc[cc_id] = set([])
        cc[cc_id].add(mg_id)
    return cc;

def saveCCs(output_fh, cc):
    for cc_id in cc:
        for mg_id in cc[cc_id]:
            output_fh.write(cc_id+"\t"+mg_id+"\n")

def combineCCs(cc1, cc2):
    cc_ret = {}
    used = set([])
    for cc1_id in cc1:
        print(cc1_id)
        sum = 0
        cc1_mgs = cc1[cc1_id]
        different = True
        overlaps = set([])
        cc_id_ret = ""
        for cc2_id in cc2:
            cc2_mgs = cc2[cc2_id]
            if cc1_mgs.issubset(cc2_mgs) and cc2_mgs.issubset(cc1_mgs):
                different = False
                cc_id_ret = cc2_id
            elif cc1_mgs.issubset(cc2_mgs) or cc2_mgs.issubset(cc1_mgs):
                overlaps.add(cc2_id)
                sum += len(cc2_mgs)
        if different:
            if len(cc1[cc1_id]) != 24 or sum != 24:
                continue
            count = 0
            for over_cc in overlaps:
                used.add(over_cc)
                count += 1
                if count == len(overlaps):
                    cc_id_ret += over_cc
                else:
                    cc_id_ret += over_cc+":"
            cc_ret[cc_id_ret] = set([])
            for over_cc in overlaps:
                cc_ret[cc_id_ret].update(cc2[over_cc])
    for cc2_id in cc2:
        if not cc2_id in used:
            cc_ret[cc2_id] = cc2[cc2_id].copy()

    return cc_ret



conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)
cc1 = readCCs(conn_comp_fh)
conn_comp_fh.close();

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)
cc2 = readCCs(conn_comp_fh)
conn_comp_fh.close();

#printCCstats(cc1)
#printCCstats(cc2)

ccc = combineCCs(cc1, cc2)

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-comb-tmp-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn, "w")
saveCCs(conn_comp_fh, ccc)
conn_comp_fh.close();
 