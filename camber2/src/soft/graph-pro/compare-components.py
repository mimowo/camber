import sys
import time
import os

sys.path.append("../../")

from soft.utils.mtb_utils import *
from soft.structs.multigene import *
from soft.utils.mtb_cc_utils import *

phase = int(sys.argv[1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()

def readCCs(input_fh):
    cc = {}
    lines = input_fh.readlines()
    for line in lines:
        tokens = line.split()
        cc_id = tokens[0]
        mg_id = tokens[1]
        if not cc_id in cc:
            cc[cc_id] = set([])
        cc[cc_id].add(mg_id)
    return cc;

def compareCCs(cc1, cc2):
    ident = set([])
    diff = {}
    for cc1_id in cc1:
 #       print cc1_id
        sum = 0
        cc1_mgs = cc1[cc1_id]
        different = True
        overlaps = set([])
        for cc2_id in cc2:
            cc2_mgs = cc2[cc2_id]
            
            if cc1_mgs.issubset(cc2_mgs) and cc2_mgs.issubset(cc1_mgs):
                ident.add((cc1_id,cc2_id))
                different = False
            elif cc1_mgs.issubset(cc2_mgs) or cc2_mgs.issubset(cc1_mgs):
                overlaps.add(cc2_id)
                sum += len(cc2_mgs)
        if different:
            if len(cc1[cc1_id]) != sum:
                continue
            if sum != 24:
                continue
            diff[cc1_id] = overlaps.copy()
            print("different: ", len(cc1[cc1_id]), cc1_id)
            for over_cc in overlaps:
                print("overlap: ", len(cc2[over_cc]), over_cc, cc2[over_cc])
        else:
            pass
       #     print "ident!"

    return diff, ident

#def printCCstats(ccs):
#    conn_comps_ann = {}
#    anchors = 0
#    core_anchors = 0
#    core_cc = 0
#    orphans = 0
#    non_anchors = 0
#    for cc_id in ccs:
#       conn_comps_ann[cc_id] = set([])
#       cc = ccs[cc_id]
#       anchor = True
#       core = True
#       if len(cc) < 24:
#           core = False
#       if len(cc) == 1:
#           orphans += 1
#       for strain_id in cc:
#           if len(cc[strain_id]) > 1:
#               anchor = False
#           for full_mg_id in cc[strain_id]:
#                tokens = full_mg_id.split(":")
#                ann_id = tokens[0]
#                if ann_id != "x":
#                    conn_comps_ann[cc_id].add(strain_id)
#       if core:
#           core_cc += 1
#       if anchor:
#           conn_comps_types[cc_id] = "ANCHOR"
#           anchors += 1
#           if core:
#               core_anchors += 1
#       else:
#           conn_comps_types[cc_id] = "NON_ANCHOR"
#           non_anchors += 1        
#    
#    print "anchors", anchors
#    print "core anchors", core_anchors
#    print "core", core_cc
#    print "orphans", orphans
#    print "non-anchors", non_anchors

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)
cc1 = readCCs(conn_comp_fh)
conn_comp_fh.close();

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)
cc2 = readCCs(conn_comp_fh)
conn_comp_fh.close();

#printCCstats(cc1)
#printCCstats(cc2)

diff, ident = compareCCs(cc1, cc2)

print("lens:", len(diff), len(ident))



 