import sys
import time
import os
from soft.structs.gene import Gene
from soft.structs.multigene import *
from soft.utils.mtb_closure_utils import *

dir_path = os.path.abspath(sys.argv[0])
folder_name = "camber-svn"
camber_dir = dir_path[0:dir_path.find(folder_name)]+folder_name+"/"
camber_src_dir = camber_dir+"src/"
sys.path.append("../../")
print camber_src_dir

from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.structs.multigene import *
from soft.utils.mtb_cc_utils import *

#phase = int(sys.argv[1])
phase = int(0)

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
diffTime("sequences")

mg_cc = {}
mg_ann = {}
mg_tiss = {}
mg_ann_tiss = {}

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

while 1:
    line = conn_comp_fh.readline()
    if not line:
        break
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    ann_id = tokens[2]
    mg_cc[mg_id] = cc_id
    mg_ann[mg_id] = ann_id
    mg_tiss[mg_id] = set([])
    mg_ann_tiss[mg_id] = set([])
    for i in range(3, len(tokens), 1):
        tis = tokens[i]
        if tis[0] == "*":
            mg_tiss[mg_id].add(int(tis[1:]))
            mg_ann_tiss[mg_id].add(int(tis[1:]))
        else:
            mg_tiss[mg_id].add(int(tis))
            
conn_comp_fh.close();

count = 0
count_up = 0 

for strain_id in strains.allGenomes():
    ann_fn = parameters["ANNOTATIONS"] + "ann-"+strain_id+".txt"
    ann_fh = open(ann_fn)
    
    print strain_id
    lines = ann_fh.readlines()
    for line in lines:
        tokens = line.split("\t")
        if len(tokens) == 3 or len(tokens) == 4:
            ann_id = tokens[0]
            start = int(tokens[1])
            end = int(tokens[2])
            strand = tokens[3]
        elif len(tokens) >= 8:
            ann_id = tokens[8]
            start = int(tokens[1])
            end = int(tokens[2])
            strand = tokens[3]
        else:
            continue
            
        gene = Gene(start, end, strand, strain_id, ann_id, ann_id) 
        if correctGene(gene)<0 and correctAltGene(gene)==0:
            multigene = createMultigeneFromGene(gene)
            mg_id = multigene.mg_strain_unique_id
            if not mg_id in mg_tiss:
                count += 1 
                print "not", mg_id
                print gene.sequence()
                print count
                continue
            count_up += 1 
            print "add", mg_id
            print ann_id
            print gene.sequence()
            print count
            
            tiss = mg_tiss[mg_id]
            tis = int(gene.length())
            ann_tiss = mg_ann_tiss[mg_id]
            if mg_ann[mg_id] == "x":
                mg_ann[mg_id] = ann_id
            if not tis in tiss:
                mg_tiss[mg_id].add(tis)
            if not tis in ann_tiss:
                mg_ann_tiss[mg_id].add(tis)                    
        
conn_comp_int_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-int-"+str(phase)+".txt"
conn_comp_int_fh = open(conn_comp_int_fn, "w")
print conn_comp_int_fn

for mg_id in mg_cc:
    cc_id = mg_cc[mg_id]
    text = cc_id + "\t" + mg_id +"\t"
    text += mg_ann[mg_id] + "\t"
    for tis in mg_tiss[mg_id]:
        ann_tiss = mg_ann_tiss[mg_id]
        if tis in ann_tiss:
            text += "*" + str(tis) +"\t"
        else:
            text += str(tis) +"\t"
    text += "\n"
    conn_comp_int_fh.write(text)
conn_comp_int_fh.close()

      