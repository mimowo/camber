import sys
import time
import os
dir_path = os.path.abspath(sys.argv[0])
folder_name = "camber-svn"
camber_dir = dir_path[0:dir_path.find(folder_name)]+folder_name+"/"
camber_src_dir = camber_dir+"src/"
sys.path.append("../../")
print(camber_src_dir)

from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.structs.multigene import *
from soft.utils.mtb_cc_utils import *

phase = int(sys.argv[1])

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime()
strains = readStrainsInfo()
readStrainsSequences(strains.allGenomes())
diffTime("sequences")
readExtAnnotations(strains.allGenomes())
diffTime("annotations")


conn_comps = {}
conn_comps_mg_ids = {}
conn_comps_types = {}
conn_comp_sizes = {}
conn_comps_ann = {}
conn_comps_line = {}
sizes_distr = {}

conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-det-"+str(phase)+".txt"
conn_comp_fh = open(conn_comp_fn)

while 1:
    line = conn_comp_fh.readline()
    if not line:
        break
    tokens = line.split()
    cc_id = tokens[0]
    mg_id = tokens[1]
    ann_id = tokens[2]
    mg_tokens = mg_id.split(".")
    strain = mg_tokens[2]
    if not cc_id in conn_comps:
        conn_comps[cc_id] = {}
        conn_comps_mg_ids[cc_id] = {}
        for strain_id in strains.allGenomes():
            conn_comps_mg_ids[cc_id][strain_id] = set([])
        conn_comp_sizes[cc_id] = 0
    cc = conn_comps[cc_id]
    cc_mg_ids = conn_comps_mg_ids[cc_id]
    if not strain in cc:
        cc[strain] = set([])
    cc[strain].add(ann_id+":"+mg_id)
    cc_mg_ids[strain].add(mg_id)
    conn_comp_sizes[cc_id] += 1
conn_comp_fh.close();

diffTime("class");

print("cc count = ", len(conn_comps))
strains_count = len(strains.allGenomes())
anchors = 0
non_anchors = 0
core_anchors = 0
core_cc = 0
orphans = 0


ann_stats = {}
for strain_id in strains.allGenomes():
    ann_stats[strain_id] = {}
    for i in range(0, len(strains.allGenomes())+1, 1):
        ann_stats[strain_id][i] = 0
        
for cc_id in conn_comps:
    conn_comps_ann[cc_id] = set([])
    cc = conn_comps[cc_id]
    anchor = True
    core = True
    if len(cc) < strains_count:
        core = False
    if len(cc) == 1:
        orphans += 1
    for strain_id in cc:
        if len(cc[strain_id]) > 1:
            anchor = False
        for full_mg_id in cc[strain_id]:
             tokens = full_mg_id.split(":")
             ann_id = tokens[0]
             if ann_id != "x":
                 conn_comps_ann[cc_id].add(strain_id)
    if core:
        core_cc += 1
    if anchor:
        conn_comps_types[cc_id] = "ANCHOR"
        anchors += 1
        if core:
            core_anchors += 1
    else:
        conn_comps_types[cc_id] = "NON_ANCHOR"
        non_anchors += 1        
 
print("anchors", anchors)
print("core anchors", core_anchors)
print("core", core_cc)
print("orphans", orphans)
print("non-anchors", non_anchors)


strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))

for cc_id in conn_comps:
    text = cc_id +"\t"
    text += conn_comps_types[cc_id] +"\t"
    text += str(conn_comp_sizes[cc_id]) +"\t"
    text += str(len(conn_comps[cc_id])) +"\t"
    text += str(len(conn_comps_ann[cc_id])) +"\t"
    conn_comps_line[cc_id] = text


for strain_id in strains_ordered:
    multigene_graph = MultigeneGraph()
    multigene_graph = readMultigenesGraph([strain_id], multigene_graph)
    diffTime("multigene graph")
    print("xxx", strain_id)
    
    for cc_id in conn_comps:
        text = conn_comps_line[cc_id]
        for mg_id in conn_comps_mg_ids[cc_id][strain_id]:
            #print "ask", mg_id, strain_id, len(multigene_graph.nodes_by_strain[strain_id])
            multigene = multigene_graph.getMultigene(mg_id, strain_id=strain_id)
            text += ";"+multigene.mg_full_id+":"
            genes_list = sorted(list(multigene.genes.values()), key = lambda gene: gene.length())
            for gene in genes_list:
                text += str(gene.length())
                text += "("+gene.sequence()[:3]+")"
                if gene.gene_id == "x":
                    text += ","
                else:
                    text +="*,"
        text += "\t"
        conn_comps_line[cc_id] = text
        
conn_comp_table_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-pro-table-"+str(phase)+".txt"
conn_comp_table_fh = open(conn_comp_table_fn, "w")
print(conn_comp_table_fn)
text = ""
text += "cc_ID\t"
text += "cc_type\t"
text += "cc_mg_count\t"
text += "cc_strains_count\t"
text += "cc_ann_strains_count\t"

for strain_id in strains_ordered:
    text += strain_id +"\t"

conn_comp_table_fh.write(text+"\n")  

for cc_id in conn_comps:
    text = conn_comps_line[cc_id]
    conn_comp_table_fh.write(text+"\n")  
conn_comp_table_fh.close()

      