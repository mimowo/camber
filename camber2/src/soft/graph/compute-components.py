import sys
import time
import os
sys.path.append("../../")

from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

if len(sys.argv)>1:
    phase = int(sys.argv[1])
else:
    phase = 0

print("Computes connected components of the consolidation graph.")

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

nodes = {}
multigene_nodes_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_details.txt"
multigene_nodes_fh = open(multigene_nodes_fn)
while 1:
    line =  multigene_nodes_fh.readline()
    if not line:
        break;
    tokens = line.split()
    if len(tokens) > 0:
        node_id = tokens[0]
        nodes[node_id] = set([])
multigene_nodes_fh.close()

multigene_graph_fh = open(parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_graph-"+str(phase)+".txt")
while 1:
    line =  multigene_graph_fh.readline()
    if not line:
        break;
    tokens = line.split()
    
    node1_id = tokens[0]
    node2_id = tokens[1]
    nodes[node1_id].add(node2_id)
    nodes[node2_id].add(node1_id)
multigene_graph_fh.close();
    
ccs = connected_components(nodes)

if phase == 0:
    conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-tmp-"+str(0)+".txt"
else:
    conn_comp_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "conn_comp-"+str(phase)+".txt"

conn_comp_fh = open(conn_comp_fn, "w")    
for cc_id in ccs:
    for mg_node_id in ccs[cc_id]:
        conn_comp_fh.write(str(cc_id)+"\t"+mg_node_id+"\n")

conn_comp_fh.close();

print("Finished.")
