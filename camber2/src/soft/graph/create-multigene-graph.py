import sys
import time
sys.path.append("../../")
import os
from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

print("Computes the consolidation graph from the graph of BLAST hits.")


ensure_dir(parameters["CLOSURE_RESULTS_GRAPHS"])
graph_fn = parameters["CLOSURE_RESULTS_GRAPHS"]+"graph.txt"
multigene_graph_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_graph-"+str(0)+".txt"

graph = set([])

def saveMultigeneGraph(output_fh):
    global graph;
    for edge in graph:
        start_node_id = edge[0]
        end_node_id = edge[1]
        output_fh.write(start_node_id + "\t"+end_node_id + "\n")

multigene_graph_fh = open(multigene_graph_fn, "w")

def createMgFromGeneId(gene_id):
    tokens = gene_id.split(".")
    count = len(tokens)
    strain = tokens[count-1]
    strand = tokens[count-2]
    end_tmp = int(tokens[count-3])
    start_tmp = int(tokens[count-4])
    if strand == "+":
        end = max(end_tmp, start_tmp)
    else:
        end = min(end_tmp, start_tmp)        
    return str(end)+"."+strand+"."+strain



graph_fh = open(graph_fn)
lines = graph_fh.readlines()

progress = CAMBerProgress("Multigene graph:")
progress.setJobsCount(len(lines))

for line in lines:
    tokens = line.split();
    gene_a_id = tokens[0]
    gene_b_id = tokens[1]
    mult_a_id = createMgFromGeneId(gene_a_id)
    mult_b_id = createMgFromGeneId(gene_b_id)    

    if not (mult_a_id, mult_b_id) in graph and not (mult_b_id, mult_a_id) in graph:
        graph.add((mult_a_id, mult_b_id))
        multigene_graph_fh.write(mult_a_id + "\t"+mult_b_id + "\n")
    progress.update()
graph_fh.close()

multigene_graph_fh.close()
