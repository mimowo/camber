import sys
import os
import time

sys.path.append("../../")

from soft.utils.mtb_closure_utils import *
from soft.utils.mtb_utils import *


parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

params = ["HSSP", "SPECIES", "EXP_PREFIX"]

python_exe = sys.executable

graph_command = python_exe + " create-gene-graph.py 0 " + createCommandParams(params, parameters)
mggraph_command = python_exe + " create-multigene-graph.py 0 " + createCommandParams(params, parameters)
mgdetails_command = python_exe + " save-multigene-details.py 0 " + createCommandParams(params, parameters)
cc_command = python_exe + " compute-components.py 0 " + createCommandParams(params, parameters)
ccdetails_command = python_exe + " save-components-details.py 0 " + createCommandParams(params, parameters)

s_time = time.clock()

os.system(graph_command)
os.system(mggraph_command)
os.system(mgdetails_command)
os.system(cc_command)
os.system(ccdetails_command)

t_time = time.clock()

print("TIME", t_time-s_time)

