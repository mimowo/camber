import sys
import time
import os
sys.path.append("../../")
from soft.structs.multigene import *
from soft.utils.mtb_utils import *
from soft.utils.mtb_cc_utils import *

print("Saves info about all multigenes.")

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()
readExtAnnotations(strains.allGenomes())

multigene_nodes_fn = parameters["CLOSURE_RESULTS_GRAPHS"] + "mult_details.txt"
multigene_nodes_fh = open(multigene_nodes_fn, "w")


progress = CAMBerProgress("Multigenes:")
n = len(strains.allGenomes())
progress.setJobsCount(n)

for strain_id in strains.allGenomes():
    multigenes = {}
    strain = strains.getGenome(strain_id)
    for gene in list(strain.ext_annotation.genes.values()):
        multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
        if not multigene_id in multigenes:
            multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain)
            multigenes[multigene_id] = multigene
        else:
            multigene = multigenes[multigene_id]
        multigene.addGene(gene)
    
    for multigene in list(multigenes.values()):
        multigene_nodes_fh.write(multigene.mg_strain_unique_id+"\t"+multigene.ann_id)
        for gene in list(multigene.genes.values()):
            if gene.gene_id == "x":
                multigene_nodes_fh.write("\t"+str(gene.length()))
            else:
                multigene_nodes_fh.write("\t*"+str(gene.length()))
        multigene_nodes_fh.write("\n")
    progress.update()
multigene_nodes_fh.close()
