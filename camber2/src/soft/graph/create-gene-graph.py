import sys
import os
sys.path.append("../../")
from soft.utils.mtb_utils import *
from soft.utils.mtb_utils import overwriteParameters
from soft.utils.mtb_io_utils import ensure_dir
from soft.utils.mtb_io_utils import readBlastEdges

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()

iterations = computeLastIteration()

print("Computes the graph of BLAST hits.")
print("Current iteration: " + str(iterations))

graph = set([])

def extendGraph(edges):
    global graph
    for edge in edges:
        start_node_id = edge[0]
        end_node_id = edge[1]
        if not (start_node_id, end_node_id) in graph and not (end_node_id, start_node_id) in graph:  
            graph.add(edge)

def saveGraph(output_fh):
    global graph;
    for edge in graph:
        start_node_id = edge[0]
        end_node_id = edge[1]
        output_fh.write(start_node_id + "\t"+end_node_id + "\n")

ensure_dir(parameters["CLOSURE_RESULTS_GRAPHS"])
graph_fn = parameters["CLOSURE_RESULTS_GRAPHS"]+"graph.txt"

#if not os.path.exists(graph_fn) or os.path.getsize(graph_fn)<10:

progress = CAMBerProgress("Graph:")
n = len(strains.allGenomes())
progress.setJobsCount(n*(n-1)*(iterations))

graph_fh = open(graph_fn, "w")
for strain1 in strains.allGenomes():
    for strain2 in strains.allGenomes():
        if strain1 != strain2: #and strain1=="KZN_4207" and strain2=="F11":
            graph = set([])            
            for it in range(0, iterations, 1):
         #       print(strain1, strain2, it)
                input_fn = parameters["CLOSURE_RESULTS_BLAST"]+"blast-"+strain1+"-"+strain2+"-"+str(it)+".txt"
                if os.path.exists(input_fn):
                    input_fh = open(input_fn)
                    edges = readBlastEdges(input_fh)
              #      print edges
                    extendGraph(edges)
                    input_fh.close()
                progress.update()
            
            saveGraph(graph_fh)
graph_fh.close()
