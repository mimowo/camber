import os

from soft.structs.strains import *



def readParametersFromFile(input_fn, parameters_map):
    input_fh = open(input_fn);

    while 1:
        wline = input_fh.readline()
        if not wline:
            break;
        wline = wline.strip() ##
        line = wline.split("#")[0]
        tokens = line.split(":")
        if len(tokens)>1:
            key = tokens[0]
            if key == "CAMBER_PATH":
                pass;#parameters_map[key] = os.path.abspath(tokens[1])+"/"
            else:
                value = ""
                for i in range(1, len(tokens), 1):
                    if tokens[i] in parameters_map:
                        value += parameters_map[tokens[i]]
                    else:
                        value += tokens[i]
                if not key in parameters_map:
                    if key in ["CAMBER_SRC_PATH", "CAMBER_RESOURCE_PATH"]:
                        value = os.path.abspath(value)+"/"
                    parameters_map[key] = value
    input_fh.close()
    return parameters_map;

def readStrainsFromFile(input_fn, subset=None):
    input_fh = open(input_fn)
    strains = Strains()

    lines = input_fh.readlines();
    for line in lines:
        if len(line)>0:
            if line[0] == "#":
                continue
        tokens = line.split();
        if len(tokens) == 1 or (len(tokens)>=2 and tokens[1]=="-"):
            strain_name = tokens[0];
            if subset != None and not strain_name in subset:
                continue
            #included = tokens[1];
            short_name = strain_name
            if len(tokens) >= 3:
                short_name = tokens[2]
            #if included == "i":
                #strain = Strain(strain_name, short_name)
            strains.addStrain(strain_name, short_name)
            #elif included == "ref":
            #    strains.addStrain(strain_name, short_name)
            #    strains.setReferenceStrain(strain_name)
        elif len(tokens)>=2 and tokens[1]!="-":
            
            strain_name = tokens[1];
            if subset != None and not strain_name in subset:
                continue
            #included = tokens[1]
            plasmid_name = tokens[0];

            #short_name = plasmid_name
            if len(tokens) >= 3:
                short_name = tokens[2]
                
            #if included == "i":
            strains.addPlasmid(strain_name, plasmid_name)
                #print("add", strain_name, plasmid_name)
            #elif included == "ref":
            #    strains.addPlasmid(strain_name, plasmid_name)
                #print("add", strain_name, plasmid_name)
                
    input_fh.close();
    return strains

#def readStrainsFromFile(input_fn, subset=None):
#    input_fh = open(input_fn)
#    strains = Strains()
#
#    lines = input_fh.readlines();
#    for line in lines:
#        if len(line)>0:
#            if line[0] == "#":
#                continue
#        tokens = line.split();
#        if len(tokens) >= 2:
#            strain_name = tokens[0];
#            if subset != None and not strain_name in subset:
#                continue
#            included = tokens[1];
#            short_name = strain_name
#            if len(tokens) >= 4:
#                short_name = tokens[3]            
#            if(included == "i" or included == "ref"):
#                #strain = Strain(strain_name, short_name)
#                strains.addStrain(strain_name, short_name)
#            if included == "ref":
#                strains.addStrain(strain_name, short_name)
#                strains.setReferenceStrain(strain_name)
#    input_fh.close();
#    return strains

def splitTreeTokens(line):
    tokens = []
    token = ""
    br = 0;
    for c in line:
        if c == '(':
            br += 1
        elif c== ')':
            br -= 1
        if br == 0 and c == ',':
            tokens.append(token)
            token = ""
        else:
            token += c
    if token != "":
        tokens.append(token)
    return tokens

def readStrainsTreeRec(strains_tree, line, parent, id):
    from soft.structs.strains_tree import StrainsTreeNode
    if line.count(",")>0:
        pos = line.rfind(")")
        left = line[1:pos]
        right = line[pos+1:]

        if right == "":
            id += 1
            right = str(id)
        new_parent_node = StrainsTreeNode(right, parent)
        if parent != None:
            parent.children.append(new_parent_node)
        else:
            strains_tree.root = new_parent_node
        strains_tree.strain_nodes[right] = new_parent_node

        left_tokens = splitTreeTokens(left)

        for left_token in left_tokens:
            readStrainsTreeRec(strains_tree, left_token, new_parent_node, id)
    else:
        node = StrainsTreeNode(line, parent)
        parent.children.append(node)
        strains_tree.strain_nodes[line] = node
        strains_tree.strain_leafs[line] = node

def readStrainsTreeFromFile(input_fn):
    from soft.structs.strains_tree import StrainTree
    strains_tree = StrainTree()
    input_fh = open(input_fn)
    line = input_fh.readline().strip()

    readStrainsTreeRec(strains_tree, line, None, 0)
    input_fh.close()

    return strains_tree;

def readNewAnnotationsFromFile(ann_fh, strain):
    from soft.structs.annotation import Annotation
    from soft.structs.gene import Gene
    annotations = Annotation()
    lines = ann_fh.readlines()
    for line in lines:
        if not line:
            break;
        line = line.strip()
        tokens = line.split("\t")
        if(len(tokens) == 4):
            gene_id = tokens[0]
            strand = tokens[3]
            start = int(tokens[1]);
            end = int(tokens[2]);
            gene = Gene(start, end, strand, strain, gene_id)
            # if correctGene(gene) == 0:
            annotations.addGene(gene)
    return annotations;

def readBlastHitsAnnFromFile(ann_fh):
    from soft.structs.annotation import Annotation
    from soft.structs.gene import Gene
    annotations = Annotation()
    lines = ann_fh.readlines()
    for line in lines:
        if line[-1:] == '\n':
            line = line[:-1]
        tokens = line.split("\t")
        gene_id = "x"
        start = int(tokens[1]);
        end = int(tokens[2]);
        strand = tokens[3]
        strain = tokens[4]

        gene = Gene(start, end, strand, strain, gene_id)
        annotations.addGene(gene)

    return annotations;

def readAnnotationsFromFile(ann_fh, strain_id, accept_alt_starts = False):
    from soft.utils.mtb_closure_utils import correctGene
    from soft.structs.annotation import Annotation
    from soft.structs.gene import Gene
    annotations = Annotation()
    lines = ann_fh.readlines()
    for line in lines:
        tokens = line.strip().split("\t")
        if(len(tokens) == 4 or len(tokens)==5 or len(tokens) == 6):
            gene_id = tokens[0];
            start = int(tokens[1]);
            end = int(tokens[2]);
            strand = tokens[3]
            
            
            gene = Gene(start, end, strand, strain_id, gene_id)
        elif len(tokens)>=8:
            try:
                start = int(tokens[1]);
            except ValueError:
                continue;
            strand = tokens[3];
            if strand == "+":
                start = min(int(tokens[1]),int(tokens[2]));
                end = max(int(tokens[1]),int(tokens[2]));
            else:
                start = max(int(tokens[1]),int(tokens[2]));
                end = min(int(tokens[1]),int(tokens[2]));
            gene_name = tokens[7];
            gene_id = tokens[8];

            gene = Gene(start, end, strand, strain_id, gene_id, gene_name)
        elif len(tokens) < 4:
            continue
        try:
            if correctGene(gene, accept_alt_starts) == 0:
                annotations.addGene(gene)
            else:
                annotations.addPseudoGene(gene)
        except:
            print(strain_id, line)
         
    return annotations;


def readGenomeSequenceFromFile(genome_fh):
    seq = ""
    genome_lines = genome_fh.readlines();

    for line in genome_lines[1:]:
        parts = line.split();
        if(len(parts) == 1):
            seq += parts[0];

    return seq;

def ensure_dir(path):
    if not os.path.exists(path):
        os.mkdir(path)

def writeGene(fh, seq, annotation):
    curr = 0;
    fh.write(">" + annotation + "\n");
    while(curr < len(seq)):
        fh.write(seq[curr:min(curr + 70, len(seq))] + "\n")
        curr = curr + 70;
    fh.write("\n");

def readBlastEdges(input_fh):
    edges = set([])
    lines = input_fh.readlines()

    for line in lines:
        try:
            words = line.split()
            hit_id = words[0]
            hit_strain = words[4]
            query_id = words[5]
            query_strain = words[9]
            edges.add((hit_id+"."+hit_strain, query_id+"."+query_strain))
        except:
            print("error: ", line)

    return edges;

def readConnCompsFromFile(input_fh, multigene_graph, strains=None, strain_set=None):
    from soft.structs.conn_comp import ConnectedComponents
    from soft.structs.conn_comp import ConnectedComponent
    conn_comps = ConnectedComponents()

    lines = input_fh.readlines()
    for line in lines:
        tokens = line.split()
        conn_id = tokens[0]
        full_mg_id = tokens[1]
        #extractUniqueMgID(full_mg_id)
        mg_id = full_mg_id#extractUniqueMgID(full_mg_id)

        mg_tokens = mg_id.split(".")
        strain_id = mg_tokens[len(mg_tokens)-1]
        if strain_set != None:
            if not strain_id in strain_set:
                continue
        elif strains != None:
            if not strain_id in strains.strains:
                if strain_id in strains.plasmids:
                    strain_id = strains.plasmids[strain_id].name 
                else:
                    continue

        if not conn_id in conn_comps.conn_comps:
            conn_comp = ConnectedComponent(conn_id)
            conn_comps.addConnComp(conn_comp)
        else:
            conn_comp = conn_comps.getConnComp(conn_id)

        mg_node = multigene_graph.getMultigeneNode(mg_id, strain_id)
        mg_node.setConnComp(conn_comp)

        conn_comp.addNode(mg_node)

        
    return conn_comps

def saveDetailedMultigenesTableToFile(conn_comps_list, multigenes_graph, output_fh):

    from soft.utils.mtb_utils import strains
    text = "CONN_COMP_ID"
    for strain_id in strains.allGenomes():
        text += "\t"+strain_id
    output_fh.write(text+"\n");

    for conn_comp in conn_comps_list:
        text = str(conn_comp.name)+ "\t"
        gene_id = "x"
        for mg_node in list(conn_comp.nodes.values()):
            if mg_node.multigene.gene_name != "x":
                if gene_id != "x" and mg_node.multigene.gene_name.upper() != gene_id.upper():
                    gene_id = "ERR"
                    break
                else:
                    gene_id = mg_node.multigene.gene_name
        text += gene_id + "\t"

        text += str(conn_comp.resSupport()) + "\t"
        text += str(conn_comp.scSupport()) + "\t"

        for strain_id in strains.allGenomes():
            for mg_node in list(conn_comp.nodesByStrain(strain_id).values()):
                multigene = mg_node.multigene
                text += ";("+multigene.gene_name+")"+multigene.mg_full_id+":"
                genes_list = sorted(list(multigene.genes.values()), key = lambda gene: gene.length())
                for gene in genes_list:
                    text += str(gene.length())
                    text += "("+gene.sequence()[:3]+")"
                    if gene.gene_id == "x":
                        text += ","
                    else:
                        text +="*,"

            text += "\t"
        output_fh.write(text+"\n");

def saveMultigeneGraphEdgesToFile(multigene_graph, output_fh):
    for mult_node1 in list(multigene_graph.nodes.values()):
        for mult_node2 in mult_node1.targets:
            output_fh.write(mult_node1.multigene.mg_strain_unique_id+"\t"+mult_node2.multigene.mg_strain_unique_id+"\n")

def readMultigeneGraphEdgesFromFile(multigene_graph, input_fh, strain_set=None):
    lines = input_fh.readlines()
    if strain_set == None:
        for line in lines:
            tokens = line.split()
            mult1_id = tokens[0]
            mult2_id = tokens[1]
            multigene_graph.addEdgeByIds(mult1_id, mult2_id)
            multigene_graph.addEdgeByIds(mult2_id, mult1_id)
    else:
        for line in lines:
            tokens = line.split()
            mult1_id = tokens[0]
            mult2_id = tokens[1]
            mg1_tokens = mult1_id.split(".")
            mg2_tokens = mult2_id.split(".")
            strain1_id = mg1_tokens[len(mg1_tokens)-1]
            strain2_id = mg2_tokens[len(mg2_tokens)-1]
             
            if strain1_id in strain_set and strain2_id in strain_set:
                multigene_graph.addEdgeByIds(mult1_id, mult2_id)
                multigene_graph.addEdgeByIds(mult2_id, mult1_id)

    return multigene_graph

#def readMultigeneGraphNodesFromFile(multigene_graph, input_fh, strain_set=None):
#    lines = input_fh.readlines()
#    
#    
#    for line in lines:
#        tokens = line.split()
#        mult1_id = tokens[0]
#        mult2_id = tokens[1]
#            multigene_graph.addEdgeByIds(mult1_id, mult2_id)
#            multigene_graph.addEdgeByIds(mult2_id, mult1_id)
#    else:
#        for line in lines:
#            tokens = line.split()
#            mult1_id = tokens[0]
#            mult2_id = tokens[1]
#            mg1_tokens = mult1_id.split(".")
#            mg2_tokens = mult2_id.split(".")
#            strain1_id = mg1_tokens[len(mg1_tokens)-1]
#            strain2_id = mg2_tokens[len(mg2_tokens)-1]
#             
#            if strain1_id in strain_set and strain2_id in strain_set:
#                multigene_graph.addEdgeByIds(mult1_id, mult2_id)
#                multigene_graph.addEdgeByIds(mult2_id, mult1_id)
#
#    return multigene_graph

def parseParsedBlastResultsFile(gene, input_fh, strain_dst):
    
    from soft.structs.gene import Gene
    from soft.structs.blast_hit import BlastHit
    
    lines = input_fh.readlines()
    blast_hits = []
    for line in lines:
        tokens = line.split()
        gene_start = int(tokens[8])
        gene_end = int(tokens[9])

        if gene_start<gene_end:
            gene_strand = "+"
            query_start = int(tokens[6])
            query_end = int(tokens[7])
        else:
            gene_strand = "-"
            query_start = int(tokens[6])
            query_end = int(tokens[7])

        hit_gene = Gene(gene_start, gene_end, gene_strand, strain_dst)

        evalue = tokens[10]
        aln_len = int(tokens[3])
        identity_b = float(tokens[2])
        identities = int(round(identity_b*aln_len/100))
        mismatches = int(tokens[4])
        residues = identities+mismatches
        #identity = query_len - int(tokens[4])

        blast_hit = BlastHit(gene, hit_gene, evalue_b=evalue, identities=identities, identity_b=identity_b, aln_len=aln_len, residues=residues, query_start=query_start, query_end=query_end)
        blast_hits.append(blast_hit)
    return blast_hits
