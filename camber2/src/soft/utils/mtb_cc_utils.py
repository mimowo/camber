def connected_components(nodes):
    visited = set([])
    list_of_curr = []
    cc = {}
    sets = {}
    cc_id = 0
    for node_id in nodes:
        
        if not node_id in visited:
            cc_id += 1
            cc[cc_id] = set([])
            list_of_curr.append(node_id)
            
        while len(list_of_curr) > 0:
            curr_node = list_of_curr.pop(0)
            cc[cc_id].add(curr_node)
            sets[curr_node] = cc_id
            neighbours = nodes[curr_node]
            for neigh in neighbours:
                if not neigh in visited:
                    visited.add(neigh)
                    list_of_curr.append(neigh)
                 
    return cc