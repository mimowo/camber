import math

def correctGene(gene, alt_starts = False):
    from soft.utils.mtb_utils import start_codons
    from soft.utils.mtb_utils import stop_codons
    from soft.utils.mtb_seq_utils import findProteinSequenceLength
    
    if not gene.length() % 3 == 0:
        return -1;
    gene_seq = gene.sequence()
    if not alt_starts:
        if not gene_seq[:3] in start_codons:
            return -2;
    if not gene_seq[-3:] in stop_codons:
        return -3;
    length = findProteinSequenceLength(gene_seq, alt_starts = alt_starts);
    if not length == len(gene_seq):
        return -4
    return 0;

def computeIteration(input_fn):
    input_fh = open(input_fn)
    line = input_fh.readline();
    word = line.split()[0]
    iteration = int(word)
    input_fh.close()
    return iteration;

def hssp(l, n):
    if l <= 11:
        return 100.0
    elif l <= 450:
        return min(100.0, n + 480*l**(-0.32*(1+math.exp(-float(l)/1000.0))))
    else:
        return 19.5 + n

