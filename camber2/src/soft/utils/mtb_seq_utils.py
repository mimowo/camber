def translateCodon(codon):
	if(codon in ["GCT", "GCC", "GCA", "GCG"]):
		return "A";
	elif(codon in ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"]):
		return "L";
	elif(codon in ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"]):
		return "R";
	elif(codon in ["AAA", "AAG"]):
		return "K";
	elif(codon in ["AAT", "AAC"]):
		return "N";
	elif(codon in ["ATG"]):
		return "M";
	elif(codon in ["GAT", "GAC"]):
		return "D";
	elif(codon in ["TTT", "TTC"]):
		return "F";
	elif(codon in ["TGT", "TGC"]):
		return "C";
	elif(codon in ["CCT", "CCC", "CCA", "CCG"]):
		return "P";
	elif(codon in ["CAA", "CAG"]):
		return "Q";
	elif(codon in ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"]):
		return "S";
	elif(codon in ["GAA", "GAG"]):
		return "E";
	elif(codon in ["ACT", "ACC", "ACA", "ACG"]):
		return "T";
	elif(codon in ["GGT", "GGC", "GGA", "GGG"]):
		return "G";
	elif(codon in ["TGG"]):
		return "W";
	elif(codon in ["CAT", "CAC"]):
		return "H";
	elif(codon in ["TAT", "TAC"]):
		return "Y";
	elif(codon in ["ATT", "ATC", "ATA"]):
		return "I";
	elif(codon in ["GTT", "GTC", "GTA", "GTG"]):
		return "V";
	elif(codon in ["TAA", "TGA", "TAG"]):
		return "*";
	else:
		return "#";

def translateSequence(dna_seq):
	prot_seq = "";
	codon = "";
	for i in range(0, len(dna_seq), 1):
		if dna_seq[i] != '-':
			codon = codon + dna_seq[i];
			if len(codon) == 3:
				amino = translateCodon(codon);
				prot_seq = prot_seq + amino;
				if amino == "*":
					return prot_seq;
				codon = "";
	return prot_seq;


def reverseString(s):
	return s[::-1]

def replaceLetter(s):
	if(s == "A"):
		ret = "T";
	elif(s == "T"):
		ret = "A";
	elif(s == "G"):
		ret = "C";
	elif(s == "C"):
		ret = "G";
	else:
		ret = s;
	return ret;

def replaceLetters(s):
	ret = ""
	for i in range(len(s)):
		if(s[i] == "A"):
			ret += "T";
		elif(s[i] == "T"):
			ret += "A";
		elif(s[i] == "G"):
			ret += "C";
		elif(s[i] == "C"):
			ret += "G";
		else:
			ret += s[i];
	return ret;

def complementarySequence(seq):
	return replaceLetters(reverseString(seq));

def findProteinSequenceLength(seq, start=0, alt_starts=False, debug=False):
	n = len(seq)
	i = start
	if not alt_starts:
		if not seq[i:i+3] in ["ATG", "GTG", "TTG"]:
			return 0
	while i + 3 <= n:
		i += 3
		codon = seq[i-3:i]
		if codon in ["TAG", "TAA", "TGA"]:
			return i-start;
	return 0
