from threading import Semaphore
import sys

class CAMBerProgress:
    def __init__(self, text):
        self.number= 0
        self.sem = Semaphore()
        self.done = 0
        self.last_done = 0
        self.dots_put = 0
        sys.stdout.write(text)
        sys.stdout.flush()
    def setJobsCount(self, number):
        self.number = number
    def update(self):
        self.sem.acquire()
        self.done += 1
        dots_number = int(round(float(self.done*100)/float(self.number)))
        new_dots_number = dots_number - self.dots_put
        #print(self.done, self.number, dots_number, new_dots_number)
        if new_dots_number > 0:
            for i in range(0, new_dots_number, 1):
                if (self.dots_put + i) % 10 == 0:
                    sys.stdout.write("|")
                sys.stdout.write(".")
            sys.stdout.flush()
            self.last_done = self.done
            self.dots_put = dots_number
        if self.done >= self.number:
            print("100%")
        self.sem.release()
        
        