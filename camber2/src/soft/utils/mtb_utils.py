import sys
from soft.utils.mtb_progress import CAMBerProgress
sys.path.append("../../")

from soft.structs.strains import Strains

import time
import platform
import os

start_codons = ["ATG", "GTG", "TTG"];
stop_codons = ["TAG", "TAA", "TGA"];

parameters_map = {}
strains = Strains()

start_time = time.clock()


def getCamberPath():
    prog_name = sys.argv[0]
    dir_path = os.path.abspath(prog_name)

    folder_name = "camber2"
    if dir_path.count(folder_name) > 0:
        camber_path = dir_path[:dir_path.rfind(folder_name)]+folder_name+"/"
        return camber_path;
    else:
        camber_path = os.path.abspath("../../../../camber2/")+"/"
        return camber_path;

def computeLocation(gene1, gene2):
    if gene1.left_bound < gene2.left_bound:
        overlap = max(gene1.right_bound - gene2.left_bound + 1, 0)
        dist = max(gene2.left_bound - gene1.right_bound - 1, 0)
    else:
        overlap = max(gene2.right_bound - gene1.left_bound + 1, 0)
        dist = max(gene1.left_bound - gene2.right_bound - 1, 0)
        
    return overlap, dist 

def shift(id, exp_len):
    ret = id
    diff = exp_len - len(id)
    i = 0
    while i < diff:
        ret += " "
        i+=1
    return ret;

def diffTime(comment=""):
    global start_time
    end_time = time.time()
    print((end_time - start_time), comment)
    start_time = end_time

def createCommandParams(attrs, parameters):
    new_command = ""
    for key in attrs:
        new_command += " "+key+"="+parameters[key]
    return new_command

def overwriteParameters(sys_argv):
    global parameters_map;
    
    parameters_map["CAMBER_PATH"] = getCamberPath()
    parameters_map["CAMBER_SRC_PATH"] = parameters_map["CAMBER_PATH"]+"src/"
    
    for arg in sys_argv:
        tokens = arg.split("=")
        if len(tokens) == 2:
            key = tokens[0]
            value=tokens[1]
            parameters_map[key] = value
    return parameters_map

def readParameters():
    from soft.utils.mtb_io_utils import readParametersFromFile
    global parameters_map;
    params_dir = parameters_map["CAMBER_SRC_PATH"]+"/soft/"; 
    
    parameters_map = readParametersFromFile(params_dir+"parameters-resources.txt", parameters_map)
    parameters_map = readParametersFromFile(params_dir+"parameters-paths-default.txt", parameters_map)
    parameters_map = readParametersFromFile(params_dir+"parameters-params.txt", parameters_map)
    parameters_map = readParametersFromFile(params_dir+"parameters-results.txt", parameters_map)
    return parameters_map

def readStrainsTree():
    global strains_tree
    from soft.utils.mtb_io_utils import readStrainsTreeFromFile
    params_dir = parameters_map["CAMBER_SRC_PATH"]+"/soft/";
    
    strains_tree = readStrainsTreeFromFile(params_dir+parameters_map["STRAINS_TREE_FILE"]+".txt");
    return strains_tree

def readStrainsInfo():
    global strains
    from soft.utils.mtb_io_utils import readStrainsFromFile
    strains_fn = parameters_map["STRAINS_INFO_FILE"]
    strains = readStrainsFromFile(strains_fn);
    return strains

def readStrainsSequences(strains_set, verbose=True):
    from soft.utils.mtb_io_utils import readGenomeSequenceFromFile

    if verbose:
        progress = CAMBerProgress("Reading sequences:")
        progress.setJobsCount(len(strains_set))
    for strain_id in strains_set:
        strain_fn = parameters_map["GENOMES"]+"seq-" + strain_id + ".fasta"
        strain_fn = os.path.abspath(strain_fn)
        strain_fh = open(strain_fn)
        strain_seq = readGenomeSequenceFromFile(strain_fh)
        strain_fh.close()
        strains.getGenome(strain_id).setSequence(strain_seq)
        if verbose:
            progress.update()       

def readAnnotations(strains_set, accept_alt_starts = False, verbose=True):
    from soft.utils.mtb_io_utils import readAnnotationsFromFile
    if verbose:
        progress = CAMBerProgress("Reading annotations:")
        progress.setJobsCount(len(strains_set))
    for strain in strains_set:
        strain_fh = open(parameters_map["ANNOTATIONS"]+"ann-" + strain + ".txt")
        annotation = readAnnotationsFromFile(strain_fh, strain, accept_alt_starts)
        strain_fh.close()
        strains.getGenome(strain).setAnnotation(annotation)
        if verbose:
            progress.update()
        

def readNewAnnotationsIter(strains_set, iteration, verbose=True):
    from soft.utils.mtb_io_utils import readNewAnnotationsFromFile
    if verbose:
        progress = CAMBerProgress("Reading new annotations:")
        progress.setJobsCount(len(strains_set))
    for strain in strains_set:
        strains_fn = parameters_map["CLOSURE_RESULTS_ANN"]+"new-"+strain+"-"+str(iteration)+".txt"
        strain_fh = open(strains_fn)
        #print(strains_fn)
        annotation = readNewAnnotationsFromFile(strain_fh, strain)
        strain_fh.close()
        strains.getGenome(strain).setNewAnnotation(annotation)
        if verbose:
            progress.update()

def computeLastIteration():
    it = 0
    while it < 100:
        strain = strains.refStrain()
        input_fn = parameters_map["CLOSURE_RESULTS_ANN"]+"all-"+strain+"-"+str(it)+".txt"
        if not os.path.exists(input_fn):
            break;
        it += 1
    return it-1;


def readExtAnnotations(strains_set, iteration=None, verbose=True):
    from soft.utils.mtb_io_utils import readNewAnnotationsFromFile
    if iteration == None:
        iteration = computeLastIteration()

    if verbose:
        progress = CAMBerProgress("Reading all annotations:")
        progress.setJobsCount(len(strains_set))
    for strain in strains_set:
        strain_fh = open(parameters_map["CLOSURE_RESULTS_ANN"]+"all-"+strain+"-"+str(iteration)+".txt")
        annotation = readNewAnnotationsFromFile(strain_fh, strain)
        strain_fh.close()
        strains.getGenome(strain).setExtAnnotation(annotation)
        strain_fh = open(parameters_map["CLOSURE_RESULTS_ANN"]+"all-"+strain+"-"+str(0)+".txt")
        annotation = readNewAnnotationsFromFile(strain_fh, strain)
        strains.getGenome(strain).setAnnotation(annotation)
        strain_fh.close()
        if verbose:
            progress.update()
    
    
def standardDnaBlastList(blast_exe, query_file_name, genome_file_name, output_file_name, tr_evalue):
    if platform.system().count("Windows") > 0 :
        blast_exe = os.path.abspath(blast_exe)    
    command = blast_exe + " -p blastn -e "+str(tr_evalue)+" -F F ";
    command = command + " -d " + genome_file_name + " -i " + query_file_name + " -o " + output_file_name + " -m 8";
    os.system(command);

def standardAminoBlastList(blast_exe, query_file_name, genome_file_name, output_file_name, tr_evalue):
    if platform.system().count("Windows") > 0 :
        blast_exe = os.path.abspath(blast_exe) 
    command = blast_exe + " -p tblastn -e "+str(tr_evalue)+" -F F -C 0 ";
    command = command + " -d " + genome_file_name + " -i " + query_file_name + " -o " + output_file_name + " -m 8";
    os.system(command)
    
def rateBlastHitsProt(blast_hits):    
    return [],[]


def rateBlastHitsDna(blast_hits, strain_id, len_p, id_p, evalue_tr, debug=False):
    from soft.utils.mtb_seq_utils import findProteinSequenceLength
    from soft.utils.mtb_closure_utils import hssp
    from soft.structs.gene import Gene
    
    good_hits = []
    strain = strains.getGenome(strain_id)
    strain_seq = strain.sequence
    rev_strain_seq = strain.rev_sequence
    
    n = len(strain_seq)
    if parameters_map["HSSP"] == "NO":
        hssp_b = False
    else:
        hssp_b = True
    
    pid_method = 0
    if parameters_map["PID_TYPE"] == "Q":
        pid_method = 0
    elif parameters_map["PID_TYPE"] == "B":
        pid_method = 1
        
    for blast_hit in blast_hits:
        if not int(blast_hit.query_start) in [1,2]:
            continue  
  
        if not float(blast_hit.evalue_b) <= float(evalue_tr):
            continue
        
        query_len = blast_hit.query_gene.length()
        if pid_method == 0:
            pid = float(blast_hit.identities)/float(query_len)
        elif pid_method == 1:
            pid = float(blast_hit.identity_b)
        
        if hssp_b:
            res_c = int(blast_hit.residues / 3)
            if not pid*100.0 >= hssp(res_c, id_p*100.0 - 19.5):
                continue

        else:
            if not pid >= id_p:
                continue                
        
        hit_gene = blast_hit.hit_gene
        
        if(hit_gene.strand == "+"):
            prot_start = int(hit_gene.start) - int(blast_hit.query_start) + 1
            length = findProteinSequenceLength(strain_seq, start=prot_start-1);
            start = prot_start
            end = prot_start + length - 1
        else:
            prot_start = int(hit_gene.start) + int(blast_hit.query_start) - 1
            length = findProteinSequenceLength(rev_strain_seq, start=n-prot_start, debug=True);
            start = prot_start - length + 1
            end = prot_start
            #print(start, end, length, n)
            #print(rev_strain_seq[n-prot_start:100])

        if (length >= (1-len_p) * query_len and length <= (1+len_p)* query_len):
            ext_hit_gene = Gene(start, end, hit_gene.strand, hit_gene.strain)
            blast_hit.addBlastHitExtended(ext_hit_gene)
            good_hits.append(blast_hit)

    return good_hits

def parseBlastResults(strain1_id, strain2_id):
    from soft.utils.mtb_io_utils import parseParsedBlastResultsFile
    from soft.utils.mtb_io_utils import ensure_dir
    len_p = float(parameters_map["FRACTION_LENGTH_CHANGE"])/float(100.0)
    id_p = float(parameters_map["IDENTITY_BEFORE_EXTENTION"])/float(100.0)
    evalue_tr = float(parameters_map["THRESHOLD_EVALUE"])
    strain1 = strains.getGenome(strain1_id)
    
    parsed_input_dir = parameters_map["BLAST_RESULTS_FOLDER"]+strain1_id+"-"+strain2_id+"/"    
    ensure_dir(parsed_input_dir)

    good_hit_map = []

    for gene in list(strain1.newAnnotation().genes.values()):
        parsed_input_fn = parsed_input_dir+"blast-"+gene.strain_unique_id+".txt"
        parsed_input_fh = open(parsed_input_fn, 'r')
        blast_hits = parseParsedBlastResultsFile(gene, parsed_input_fh, strain2_id);
        parsed_input_fh.close()
        
        good_hits = rateBlastHitsDna(blast_hits, strain2_id, len_p, id_p, evalue_tr)
        good_hit_map += good_hits
        #rated_hits["good"]
        #bad_hit_map += bad_hits#rated_hits["bad"]
        

    return good_hit_map;    
    
def readMultigenesGraphFast(strains, multigene_graph, input_fh):
    from soft.structs.multigene import Multigene
    count = 0
    while 1:
        count += 1;
        if (count % 10000 == 0):
            pass
        line = input_fh.readline()
        if not line:
            break;

        tokens = line.split()
        #cc_id = tokens[0]
        mg_tokens = tokens[1].split(".")
        ann_id = tokens[2]
        end = mg_tokens[0]
        strand = mg_tokens[1]
        strain_plas_id = mg_tokens[2]
        

        if strain_plas_id in strains.allGenomes(): 
            strain_id = strain_plas_id     
            max_length = 0
            
            for i in range(3, len(tokens), 1):
                length = int(tokens[i].strip("*"))
                if length > max_length:
                    max_length = length
            
            if strand == "+":
                start = int(end) - max_length + 1
            else:
                start = int(end) + max_length - 1            
            
            multigene = Multigene(start, end, strand, strain_id, ann_id=ann_id, gene_name=ann_id)     
            multigene.createLongestUniqueMultigeneId()
            multigene.setChromosome("main")
            multigene_graph.addMultigene(multigene)
        elif strain_plas_id in strains.plasmids:
            plasmid_id = strain_plas_id
            strain_id = strains.plasmids[plasmid_id].name
            max_length = 0
            
            for i in range(3, len(tokens), 1):
                length = int(tokens[i].strip("*"))
                if length > max_length:
                    max_length = length
            
            if strand == "+":
                start = int(end) - max_length + 1
            else:
                start = int(end) + max_length - 1            
            
            multigene = Multigene(start, end, strand, strain_id, ann_id=ann_id, gene_name=ann_id)     
            multigene.createLongestUniqueMultigeneId()
            multigene.setChromosome("plasmid")
            multigene_graph.addMultigene(multigene)            

    return multigene_graph
 
    
def readMultigenesGraphEff(strains_set, multigene_graph, input_fh):
    from soft.structs.multigene import Multigene
    while 1:
        line = input_fh.readline()
        if not line:
            break;

        tokens = line.split()
        mg_tokens = tokens[0].split(".")
        strand = mg_tokens[1]
        strain = mg_tokens[2]

        if strain in strains_set:        
            max_length = 0
            
            for i in range(2, len(tokens), 1):
                length = int(tokens[i].strip("*"))
                if length > max_length:
                    max_length = length
    
            end = mg_tokens[0]

            
            if strand == "+":
                start = int(end) - max_length + 1
                multigene = Multigene(start, end, strand, strain)
                
            else:
                start = int(end) + max_length - 1            
                multigene = Multigene(start, end, strand, strain)
                
            multigene_graph.addMultigene(multigene)

    return multigene_graph

def readMultigenesGraph(multigene_graph, strains_set):
    from soft.structs.multigene import createStrainMultigeneId
    from soft.structs.multigene import Multigene
     
    progress = CAMBerProgress("Reading graph:")
    progress.setJobsCount(len(strains_set))
    for strain_id in strains_set:
        strain = strains.getGenome(strain_id)
        for gene in list(strain.ext_annotation.genes.values()):
            multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain)
            if not multigene_graph.hasMultigeneNode(multigene_id):
                multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain)
                multigene_graph.addMultigene(multigene)
            else:
                multigene = multigene_graph.getMultigene(multigene_id)
            multigene.addGene(gene)
        progress.update()    
    return multigene_graph

def readConnComps(multigene_graph, input_fh = None, strains=None, strain_set=None):
    from soft.utils.mtb_io_utils import readConnCompsFromFile
    #if input_fh == None:
    #    input_fh = open(parameters_map["CLOSURE_RESULTS_GRAPHS"]+"conn_comp-det-"+str(phase)+".txt")
        
    conn_comps = readConnCompsFromFile(input_fh, multigene_graph, strains=strains, strain_set=strain_set)
    #input_fh.close()
    return conn_comps

def saveMultigeneGraphEdges(multigene_graph, phase):
    from soft.utils.mtb_io_utils import saveMultigeneGraphEdgesToFile
    output_fh = open(parameters_map["CLOSURE_RESULTS_GRAPHS"]+"mult_graph-"+str(phase)+".txt", "w")
    saveMultigeneGraphEdgesToFile(multigene_graph, output_fh)
    output_fh.close()

def readMultigeneGraphEdges(multigene_graph, input_fh, strain_set=None):
    from soft.utils.mtb_io_utils import readMultigeneGraphEdgesFromFile
    #if input_fh == None:
        #input_fh = open(parameters_map["CLOSURE_RESULTS_GRAPHS"]+"mult_graph-"+str(phase)+".txt")
        
    multigene_graph = readMultigeneGraphEdgesFromFile(multigene_graph, input_fh, strain_set=strain_set)
    input_fh.close()
    return multigene_graph

#def readMultigeneGraphNodes(multigene_graph, input_fh, strain_set=None):
#    from soft.utils.mtb_io_utils import readMultigeneGraphNodesFromFile
#    #if input_fh == None:
#    #    input_fh = open(parameters_map["CLOSURE_RESULTS_GRAPHS"]+"mult_graph-"+str(phase)+".txt")
#        
#    multigene_graph = readMultigeneGraphNodesFromFile(multigene_graph, input_fh, strain_set=strain_set)
#    input_fh.close()
#    return multigene_graph
