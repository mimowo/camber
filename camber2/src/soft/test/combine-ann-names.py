import sys
sys.path.append("../..")
from soft.utils.mtb_utils import *
from soft.utils.mtb_seq_utils import translateSequence
from soft.utils.mtb_io_utils import ensure_dir
from soft.utils.mtb_closure_utils import *
import os

diffTime("")
parameters = overwriteParameters(sys.argv)
parameters = readParameters()
diffTime("parameters")
strains = readStrainsInfo()

ensure_dir(parameters["BLAST_FOLDER"])
ensure_dir(parameters["RESULTS_FOLDER"]);
ensure_dir(parameters["CAMBER_PARAM_RESULTS_FOLDER"]);
ensure_dir(parameters["CLOSURE_RESULTS"])
ensure_dir(parameters["CLOSURE_RESULTS_ANN"])
ensure_dir(parameters["CLOSURE_RESULTS_PSEUDO"])

iteration_fh = open(parameters["CLOSURE_ITERATION"],"w")
iteration_fh.write("0\n")
iteration_fh.close();

iteration = 0;

print((parameters["CAMBER_PARAM_RESULTS_FOLDER"]))


all_output_fn = parameters["CLOSURE_RESULTS"]+"names_anns.txt"

all_output_fh = open(all_output_fn, "w")
 

strains_ordered = sorted(strains.allGenomes(), key=lambda strain_id:-len(strains.getGenome(strain_id).sequence))
for strain_id in strains_ordered:
    all_input_fn = parameters["ANNOTATIONS"]+"ann-"  + strain_id+".txt"
    all_input_fh = open(all_input_fn)
    lines = all_input_fh.readlines()
    for line in lines:
        tokens = line.split()
        if len(tokens)>=4:
            all_output_fh.write(tokens[0]+"\t"+tokens[4]+"\n")
        
    all_input_fh.close()
all_output_fh.close()

