import sys
import time
import os
from threading import Thread
sys.path.append("../../")

from soft.utils.mtb_utils import *
from soft.utils.mtb_io_utils import *
from soft.structs.multigene import *
from soft.structs.conn_comp import *


if len(sys.argv)>1:
    phase = int(sys.argv[1])
else:
    phase = 0

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
strains = readStrainsInfo()


ensure_dir(parameters["CLUSTER_SCRIPTS"])
ensure_dir(parameters["CLOSURE_RESULTS_REFINEMENT"])

species = parameters["SPECIES"]
exp_prefix = parameters["EXP_PREFIX"]
hssp = parameters["HSSP"]

cur_dir = os.path.abspath(os.curdir) +"/"

threads_list = []

def doRefinementStrain(strain1_id):
    for strain2_id in strains.allGenomes(): 
        if strain1_id >= strain2_id:
            continue
        command = "python -v " + cur_dir + "remove-edges-strains-pair.py " 
        command += strain1_id + " " + strain2_id +" phase="+str(phase)+" SPECIES="+species+" EXP_PREFIX="+exp_prefix+" HSSP="+hssp
        print(command)
        os.system(command)
        progress.update()
        
progress = CAMBerProgress("Refinement:")
n = len(strains.allGenomes())
progress.setJobsCount(n*(n-1)/2)        
        
for strain1_id in strains.allGenomes(): 
    doRefinementStrain(strain1_id)
    #for strain1_id in strains.allGenomes():
    #    thread = Thread(target=doRefinementStrain, args=[strain1_id])
    
    #threads_list.append(thread)
    #thread.start()
    
#for thread in threads_list:
#    thread.join()

    

            
    
