import sys
import os
import time
sys.path.append("../../")
import threading
from threading import Thread

tab = {1:2,4:5}

print(tab.keys())

MAX = 3

sem = threading.BoundedSemaphore(value=MAX)

def work(nr):
    sem.acquire()
    time.sleep(1)
    print(nr)
    sem.release()
    


threads_list = []

for i in range(0, 100, 1):
    thread = Thread(target=work, args=[i])

    threads_list.append(thread)
    thread.start()

for thread in threads_list:
    thread.join()


