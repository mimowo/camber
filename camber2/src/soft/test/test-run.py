import sys
import os

script_fn = "/research/wongls-group/wozniak/scripts2/test.sh"
script_fh = open(script_fn, "w")

os.putenv("SOME_VARIABLE", "my value")
os.environ["SOME_VARIABLE"]="/path/to/program"
print(os.environ['PATH'])

print(os.getenv("SOME_VARIABLE"))
print(os.environ['SOME_VARIABLE'])

command = "python " + "/research/wongls-group/wozniak/camber-svn/src/soft/test/test.py\n" 
script_fh.write(command)            
script_fh.close()
os.system("qsub -e error.log -o out.txt -cwd "+script_fn);
